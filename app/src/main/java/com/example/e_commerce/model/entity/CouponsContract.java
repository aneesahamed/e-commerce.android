package com.example.e_commerce.model.entity;

import android.content.ContentUris;
import android.net.Uri;

import static com.example.e_commerce.model.DBConstant.COUPONS_TABLE;

public class CouponsContract extends BaseContract {
    public static class CouponsEntry {
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(COUPONS_TABLE).build();

        public static Uri getAddressUri(String id) {
            return Uri.parse("content://" + CONTENT_AUTHORITY+"/"+COUPONS_TABLE+"/"+id);
        }
    }
}
