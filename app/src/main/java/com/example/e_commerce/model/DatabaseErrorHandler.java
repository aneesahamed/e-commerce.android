package com.example.e_commerce.model;

import android.util.Log;

public class DatabaseErrorHandler {
    public static void handleError(String tag, String errorMessage) {
        Log.e(tag, errorMessage);
    }
}
