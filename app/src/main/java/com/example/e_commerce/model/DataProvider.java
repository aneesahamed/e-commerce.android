package com.example.e_commerce.model;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import com.example.e_commerce.model.entity.AddressContract;
import com.example.e_commerce.model.entity.BaseContract;
import com.example.e_commerce.model.entity.CouponsContract;
import com.example.e_commerce.model.entity.UserContract;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import static com.example.e_commerce.model.DBConstant.ADDRESS_TABLE;
import static com.example.e_commerce.model.DBConstant.COUPONS_TABLE;
import static com.example.e_commerce.model.DBConstant.PRODUCT_TABLE;
import static com.example.e_commerce.model.DBConstant.UID;
import static com.example.e_commerce.model.DBConstant.USER_TABLE;

public class DataProvider extends ContentProvider {

    private static final int ADDRESS_ENTRY = 100;
    private static final int ADDRESS_WITH_ID = 101;
    private static final int ADDRESS_QUERY = 102;
    private static final int USER_ADDRESS_UPDATE_ID = 200;
    private static final int COUPON_WITH_ID = 300;
    private static final int PRODUCT_WITH_ID = 400;
    private static UriMatcher urimatcher;
    public static DataBaseHelper dataBaseHelper;
    static {
        urimatcher = new UriMatcher(UriMatcher.NO_MATCH);
        urimatcher.addURI(AddressContract.CONTENT_AUTHORITY,ADDRESS_TABLE ,ADDRESS_ENTRY);
        urimatcher.addURI(AddressContract.CONTENT_AUTHORITY,ADDRESS_TABLE+"/#" ,ADDRESS_WITH_ID);
        urimatcher.addURI(UserContract.CONTENT_AUTHORITY ,USER_TABLE , USER_ADDRESS_UPDATE_ID);
        urimatcher.addURI(CouponsContract.CONTENT_AUTHORITY ,COUPONS_TABLE+"/*" , COUPON_WITH_ID);
        urimatcher.addURI(BaseContract.CONTENT_AUTHORITY ,PRODUCT_TABLE+"/*" , PRODUCT_WITH_ID);

    }

    @Override
    public boolean onCreate() {
        dataBaseHelper =  new DataBaseHelper(getContext());
        return dataBaseHelper != null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        switch (urimatcher.match(uri)){
            case ADDRESS_ENTRY:
                long id = dataBaseHelper.insertAddress(values);
                if(id>0){
                    getContext().getContentResolver().notifyChange(uri, null);
                    return AddressContract.AddressEntry.getAddressUri(id);

                }
                else {
                    throw new android.database.SQLException("Failed to insert row into: " + uri);
                }

        }

        return null;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        Cursor cursor = null;
        switch (urimatcher.match(uri)){
            case ADDRESS_WITH_ID:
                cursor = dataBaseHelper.getAddressById(projection ,selection,selectionArgs);
                break;
            case COUPON_WITH_ID:
                cursor = dataBaseHelper.getCouponList(null,UID+" =?",new String[]{uri.getLastPathSegment()});
                break;
            case PRODUCT_WITH_ID:
               return dataBaseHelper.getMatchedData(uri.getLastPathSegment());
        }
        return cursor;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        int numRows = 0;
        switch (urimatcher.match(uri)){
            case USER_ADDRESS_UPDATE_ID:
                numRows = dataBaseHelper.updateUserAddress(values ,selection ,selectionArgs);
                break;

            case ADDRESS_WITH_ID:
                numRows = dataBaseHelper.updateAddress(values ,selection ,selectionArgs);
                break;

        }

        return numRows;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }
}
