package com.example.e_commerce.model.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.e_commerce.common.AppConstant;

import java.util.ArrayList;
import java.util.List;

public class User implements Parcelable {
    private String uid;
    private String mobileNo;
    private String name;
    private String email;
    private String profileUri;
    private String addressId = AppConstant.EMPTY;
    private List<String> selecedIds = new ArrayList<>();

    public void setSelecedIds(List<String> selecedIds) {
        this.selecedIds.clear();
        this.selecedIds.addAll(selecedIds);
    }

    public List<String> getSelecedIds() {
        return selecedIds;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUid() {
        return uid;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public String getName() {
        return name;
    }

    public void setProfileUri(String profileUri) {
        this.profileUri = profileUri;
    }

    public String getProfileUri() {
        return profileUri;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    public String getAddressId() {
        return addressId;
    }

    public User() {
    }

    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public User(Parcel in) {
        this.uid = in.readString();
        this.name = in.readString();
        this.email = in.readString();
        this.mobileNo = in.readString();
        this.profileUri = in.readString();
        this.addressId = in.readString();
        in.readStringList(selecedIds);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.uid);
        dest.writeString(this.name);
        dest.writeString(this.email);
        dest.writeString(this.mobileNo);
        dest.writeString(this.profileUri);
        dest.writeStringList(this.selecedIds);
        dest.writeString(this.addressId);
    }
}
