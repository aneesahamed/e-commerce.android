package com.example.e_commerce.model;

import android.app.SearchManager;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.provider.BaseColumns;
import android.text.TextUtils;
import android.util.Log;

import com.example.e_commerce.R;
import com.example.e_commerce.common.AppConstant;
import com.example.e_commerce.common.PreferenceManager;
import com.example.e_commerce.common.Utils;
import com.example.e_commerce.model.entity.Address;
import com.example.e_commerce.model.entity.Category;
import com.example.e_commerce.model.entity.Coupon;
import com.example.e_commerce.model.entity.GeneralCategory;
import com.example.e_commerce.model.entity.HomeFeed;
import com.example.e_commerce.model.entity.Order;
import com.example.e_commerce.model.entity.Price;
import com.example.e_commerce.model.entity.Product;
import com.example.e_commerce.model.entity.Review;
import com.example.e_commerce.model.entity.Specification;
import com.example.e_commerce.model.entity.TrendsDeal;
import com.example.e_commerce.model.entity.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import androidx.annotation.Nullable;

import static com.example.e_commerce.model.DBConstant.ADDRESS_ID;
import static com.example.e_commerce.model.DBConstant.ADDRESS_TABLE;
import static com.example.e_commerce.model.DBConstant.ADDRESS_TYPE;
import static com.example.e_commerce.model.DBConstant.ALT_MOBILE_NO;
import static com.example.e_commerce.model.DBConstant.AMOUNT;
import static com.example.e_commerce.model.DBConstant.AREA;
import static com.example.e_commerce.model.DBConstant.CART_TABLE;
import static com.example.e_commerce.model.DBConstant.CATEGORY_TABLE;
import static com.example.e_commerce.model.DBConstant.CID;
import static com.example.e_commerce.model.DBConstant.CITY;
import static com.example.e_commerce.model.DBConstant.COLOR;
import static com.example.e_commerce.model.DBConstant.COUPONS_TABLE;
import static com.example.e_commerce.model.DBConstant.COUPON_DESC;
import static com.example.e_commerce.model.DBConstant.COUPON_ID;
import static com.example.e_commerce.model.DBConstant.COUPON_TITLE;
import static com.example.e_commerce.model.DBConstant.DATABASE_NAME;
import static com.example.e_commerce.model.DBConstant.DATABASE_VERSION;
import static com.example.e_commerce.model.DBConstant.DELIVED;
import static com.example.e_commerce.model.DBConstant.DELIVERED_TIME;
import static com.example.e_commerce.model.DBConstant.DELIVERY_FEE;
import static com.example.e_commerce.model.DBConstant.DISCOUNT;
import static com.example.e_commerce.model.DBConstant.DOOR_NO;
import static com.example.e_commerce.model.DBConstant.EMAIL;
import static com.example.e_commerce.model.DBConstant.EXPECTED_DELIVERY_TIME;
import static com.example.e_commerce.model.DBConstant.FINAL_AMOUNT;
import static com.example.e_commerce.model.DBConstant.FIRST_NAME;
import static com.example.e_commerce.model.DBConstant.GCID;
import static com.example.e_commerce.model.DBConstant.GENERAL_CATEGORY_TABLE;
import static com.example.e_commerce.model.DBConstant.HERTZ;
import static com.example.e_commerce.model.DBConstant.ID;
import static com.example.e_commerce.model.DBConstant.IMG_ONE;
import static com.example.e_commerce.model.DBConstant.IMG_THREE;
import static com.example.e_commerce.model.DBConstant.IMG_TWO;
import static com.example.e_commerce.model.DBConstant.KEYWORDS;
import static com.example.e_commerce.model.DBConstant.LAST_NAME;
import static com.example.e_commerce.model.DBConstant.MEM;
import static com.example.e_commerce.model.DBConstant.MOBILE_NO;
import static com.example.e_commerce.model.DBConstant.MODEL_NAME;
import static com.example.e_commerce.model.DBConstant.MODEL_NUMBER;
import static com.example.e_commerce.model.DBConstant.NAME;
import static com.example.e_commerce.model.DBConstant.OFFER_PRODUCT;
import static com.example.e_commerce.model.DBConstant.OFFER_TREND;
import static com.example.e_commerce.model.DBConstant.OFFSET;
import static com.example.e_commerce.model.DBConstant.ORDER_APPROVE;
import static com.example.e_commerce.model.DBConstant.ORDER_APPROVE_TIME;
import static com.example.e_commerce.model.DBConstant.ORDER_ID;
import static com.example.e_commerce.model.DBConstant.ORDER_TABLE;
import static com.example.e_commerce.model.DBConstant.PACKED;
import static com.example.e_commerce.model.DBConstant.PACKED_TIME;
import static com.example.e_commerce.model.DBConstant.PAYMENT_MODE;
import static com.example.e_commerce.model.DBConstant.PID;
import static com.example.e_commerce.model.DBConstant.PINCODE;
import static com.example.e_commerce.model.DBConstant.PRODUCT_BRAND;
import static com.example.e_commerce.model.DBConstant.PRODUCT_DESC;
import static com.example.e_commerce.model.DBConstant.PRODUCT_IMAGE_TABLE;
import static com.example.e_commerce.model.DBConstant.PRODUCT_TABLE;
import static com.example.e_commerce.model.DBConstant.PROFILE;
import static com.example.e_commerce.model.DBConstant.QTY;
import static com.example.e_commerce.model.DBConstant.RAM;
import static com.example.e_commerce.model.DBConstant.RATING;
import static com.example.e_commerce.model.DBConstant.REDEEMED_STATE;
import static com.example.e_commerce.model.DBConstant.REVIEW_CONTENT;
import static com.example.e_commerce.model.DBConstant.REVIEW_TABLE;
import static com.example.e_commerce.model.DBConstant.ROW_ID;
import static com.example.e_commerce.model.DBConstant.SEARCH_HISTORY;
import static com.example.e_commerce.model.DBConstant.SHIPPED;
import static com.example.e_commerce.model.DBConstant.SHIPPED_TIME;
import static com.example.e_commerce.model.DBConstant.SIM_TYPE;
import static com.example.e_commerce.model.DBConstant.STATE;
import static com.example.e_commerce.model.DBConstant.STOCK_COUNT;
import static com.example.e_commerce.model.DBConstant.SUGGESTION_TABLE;
import static com.example.e_commerce.model.DBConstant.TAG;
import static com.example.e_commerce.model.DBConstant.UID;
import static com.example.e_commerce.model.DBConstant.URL;
import static com.example.e_commerce.model.DBConstant.USER_TABLE;
import static com.example.e_commerce.model.DBConstant.WARRENTY;
import static com.example.e_commerce.model.DBConstant.WEIGHT;
import static com.example.e_commerce.model.DBConstant.WISH_LIST_TABLE;

public class DataBaseHelper extends SQLiteOpenHelper {
    private Context context;
    public static final String KEY_WORD = SearchManager.SUGGEST_COLUMN_TEXT_1;
    public static final String KEY_DEFINITION = SearchManager.SUGGEST_COLUMN_TEXT_2;
    private static final HashMap<String, String> mColumnMap = buildColumnMap();
    private static final String CREATE_USER_TABLE = "CREATE TABLE " + USER_TABLE + "(" +
            UID + " TEXT NOT NULL PRIMARY KEY ," +
            NAME + " TEXT," +
            EMAIL + " TEXT," +
            MOBILE_NO + " TEXT," +
            PROFILE + " TEXT," +
            ADDRESS_ID + " TEXT)";
    private static final String CREATE_GENERAL_CATEGORY_TABLE = "CREATE TABLE " + GENERAL_CATEGORY_TABLE + "(" +
            GCID + " TEXT NOT NULL PRIMARY KEY ," +
            NAME + " TEXT," +
            URL + " TEXT)";
    private static final String CREATE_CATEGORY_TABLE = "CREATE TABLE " + CATEGORY_TABLE + "(" +
            CID + " TEXT NOT NULL PRIMARY KEY," +
            GCID + " TEXT," +
            NAME + " TEXT," +
            URL + " TEXT)";
    private static String CREATE_PRODUCT_TABLE = "CREATE TABLE " + PRODUCT_TABLE + "(" +
            PID + " TEXT NOT NULL PRIMARY KEY," +
            GCID + " TEXT," +
            CID + " TEXT," +
            NAME + " TEXT," +
            AMOUNT + " INTEGER," +
            DISCOUNT + " INTEGER," +
            STOCK_COUNT + " TEXT," +
            PRODUCT_DESC + " TEXT," +
            WARRENTY + " TEXT," +
            PRODUCT_BRAND + " TEXT," +
            RAM + " TEXT," +
            COLOR + " TEXT," +
            MEM + " TEXT," +
            HERTZ + " TEXT," +
            WEIGHT + " TEXT," +
            MODEL_NUMBER + " TEXT," +
            MODEL_NAME + " TEXT," +
            SIM_TYPE + " TEXT)";

    private static final String CREATE_CART_TABLE = "CREATE TABLE " + CART_TABLE + "(" +
            UID + " TEXT," +
            PID + " TEXT," +
            QTY + " INTEGER)";
    private static final String CREATE_WISHLIST_TABLE = "CREATE TABLE " + WISH_LIST_TABLE + "(" +
            UID + " TEXT," +
            PID + " TEXT," +
            QTY + " INTEGER)";

    private static final String CREATE_ADDRESS_TABLE = "CREATE TABLE " + ADDRESS_TABLE + "(" +
            UID + " TEXT," +
            ADDRESS_ID + " TEXT NOT NULL PRIMARY KEY," +
            FIRST_NAME + " TEXT," +
            LAST_NAME + " TEXT," +
            DOOR_NO + " TEXT," +
            AREA + " TEXT," +
            CITY + " TEXT," +
            STATE + " TEXT," +
            PINCODE + " TEXT," +
            ALT_MOBILE_NO + " TEXT," +
            ADDRESS_TYPE + " TEXT)";

    private static final String CREATE_COUPONS_TABLE = "CREATE TABLE " + COUPONS_TABLE + "(" +
            UID + " TEXT," +
            COUPON_ID + " TEXT," +
            COUPON_TITLE + " TEXT," +
            DISCOUNT + " INTEGER," +
            URL + " TEXT," +
            REDEEMED_STATE + " TEXT)";
    private static final String CREATE_REVIEW_TABLE = "CREATE TABLE " + REVIEW_TABLE + "(" +
            PID + " TEXT," +
            UID + " TEXT," +
            NAME + " TEXT," +
            REVIEW_CONTENT + " TEXT," +
            RATING + " TEXT," +
            IMG_ONE + " TEXT," +
            IMG_TWO + " TEXT," +
            IMG_THREE + " TEXT)";

    private static final String CREATE_PRODUCT_IMAGE_TABLE = "CREATE TABLE " + PRODUCT_IMAGE_TABLE + "(" +
            PID + " TEXT," +
            URL + " TEXT)";

    private static final String CREATE_SEARCH_HISTORY_TABLE = "CREATE TABLE " + SEARCH_HISTORY + "(" +
            ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            KEYWORDS + " TEXT)";

    private static final String CREATE_ORDER_TABLE = "CREATE TABLE " + ORDER_TABLE + "(" +
            ORDER_ID + " TEXT NOT NULL PRIMARY KEY," +
            UID + " TEXT," +
            PID + " TEXT," +
            ADDRESS_ID + " TEXT," +
            DELIVERY_FEE + " INTEGER," +
            FINAL_AMOUNT + " INTEGER," +
            PAYMENT_MODE + " INTEGER," +
            QTY + " INTEGER," +
            COUPON_ID + " TEXT," +
            ORDER_APPROVE + " INTEGER," +
            ORDER_APPROVE_TIME + " INTEGER," +
            PACKED + " INTEGER," +
            PACKED_TIME + " INTEGER," +
            SHIPPED + " INTEGER," +
            SHIPPED_TIME + " INTEGER," +
            DELIVED + " INTEGER," +
            DELIVERED_TIME + " INTEGER," +
            EXPECTED_DELIVERY_TIME + " TEXT)";

    private static final String CREATE_OFFER_TREND_TABLE = "CREATE TABLE " + OFFER_TREND + "(" +
            ID + " TEXT," +
            URL + " TEXT," +
            OFFER_PRODUCT + " TEXT," +
            DISCOUNT + " INTEGER)";

    private static final String CREATE_SUGGESTION_TABLE = "CREATE TABLE " + SUGGESTION_TABLE + "(" +
            UID + " TEXT," +
            CID + " TEXT)";

    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_USER_TABLE);
        db.execSQL(CREATE_GENERAL_CATEGORY_TABLE);
        db.execSQL(CREATE_CATEGORY_TABLE);
        db.execSQL(CREATE_ADDRESS_TABLE);
        db.execSQL(CREATE_CART_TABLE);
        db.execSQL(CREATE_ORDER_TABLE);
        db.execSQL(CREATE_COUPONS_TABLE);
        db.execSQL(CREATE_REVIEW_TABLE);
        db.execSQL(CREATE_PRODUCT_TABLE);
        db.execSQL(CREATE_PRODUCT_IMAGE_TABLE);
        db.execSQL(CREATE_WISHLIST_TABLE);
        db.execSQL(CREATE_SEARCH_HISTORY_TABLE);
        db.execSQL(CREATE_OFFER_TREND_TABLE);
        db.execSQL(CREATE_SUGGESTION_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }


    public boolean isDBLoaded() {
        SQLiteDatabase db = this.getReadableDatabase();
        try {
            boolean isLoaded;
            Cursor cursor = db.query(GENERAL_CATEGORY_TABLE, null, null, null, null, null, null, null);
            isLoaded = cursor.moveToFirst();
            cursor.close();
            return isLoaded;
        } catch (SQLiteException ex) {
            DatabaseErrorHandler.handleError("DB_LOAD", ex.getMessage());
        }
        return false;
    }

    private static HashMap<String, String> buildColumnMap() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(KEY_WORD, KEY_WORD);
        map.put(BaseColumns._ID, "rowid AS " +
                BaseColumns._ID);
        map.put(SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID, "rowid AS " +
                SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID);
        map.put(SearchManager.SUGGEST_COLUMN_SHORTCUT_ID, "rowid AS " +
                SearchManager.SUGGEST_COLUMN_SHORTCUT_ID);
        return map;
    }

    public boolean dataInitialization() {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            String jsonString = Utils.getStringJSON(context);
            try {
                JSONObject jsonObject = new JSONObject(jsonString);
                JSONArray jsonArray = jsonObject.getJSONArray(AppConstant.GENERAL_CATEGORY);
                for (int i = 0; i < jsonArray.length(); i++) {
                    String gcId = UUID.randomUUID().toString();
                    JSONObject gcObj = jsonArray.getJSONObject(i);
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(GCID, gcId);
                    contentValues.put(NAME, gcObj.getString(AppConstant.NAME));
                    contentValues.put(URL, gcObj.getString(AppConstant.IMAGE_URL));
                    db.insertOrThrow(GENERAL_CATEGORY_TABLE, null, contentValues);

                    JSONArray subArray = gcObj.getJSONArray(AppConstant.SUB_CATEGORY);
                    for (int j = 0; j < subArray.length(); j++) {
                        JSONObject subCategoryObj = subArray.getJSONObject(j);
                        JSONArray productArray = subCategoryObj.getJSONArray(AppConstant.PRODUCTS);
                        for (int k = 0; k < productArray.length(); k++) {
                            JSONObject productObj = productArray.getJSONObject(k);
                            JSONObject productBaseObj = productObj.getJSONObject(AppConstant.PRODUCT_BASE_INFO);
                            JSONObject specObj = productBaseObj.getJSONObject(AppConstant.SPECIFICATION);
                            String cId = UUID.randomUUID().toString();
                            ContentValues categoryContentValue = new ContentValues();
                            categoryContentValue.put(CID, cId);
                            categoryContentValue.put(GCID, gcId);
                            categoryContentValue.put(NAME, specObj.getString(AppConstant.P_TYPE));
                            categoryContentValue.put(URL, productBaseObj.getString(AppConstant.CATEGORY_IMAGE));
                            db.insertOrThrow(CATEGORY_TABLE, null, categoryContentValue);

                            JSONArray variants = productObj.getJSONArray(AppConstant.VARIANTS);
                            for (int m = 0; m < variants.length(); m++) {
                                JSONObject mVariant = variants.getJSONObject(m);
                                JSONArray specVariantArray = specObj.getJSONArray(AppConstant.SPEC_VARIANT);
                                for (int n = 0; n < specVariantArray.length(); n++) {
                                    String pId = UUID.randomUUID().toString();
                                    JSONObject mSpecVariant = specVariantArray.getJSONObject(n);
                                    ContentValues productContentValues = new ContentValues();
                                    if (!TextUtils.isEmpty(mSpecVariant.getString(AppConstant.COLOR_NAME))) {
                                        productContentValues.put(COLOR, mSpecVariant.getString(AppConstant.COLOR_NAME));
                                    }
                                    if (!TextUtils.isEmpty(mSpecVariant.getString(AppConstant.AVAILABLE_RAM))) {
                                        productContentValues.put(RAM, mSpecVariant.getString(AppConstant.AVAILABLE_RAM));
                                    }
                                    if (!TextUtils.isEmpty(mSpecVariant.getString(AppConstant.AVAILABLE_HERTZ))) {
                                        productContentValues.put(HERTZ, mSpecVariant.getString(AppConstant.AVAILABLE_HERTZ));
                                    }
                                    if (!TextUtils.isEmpty(mSpecVariant.getString(AppConstant.AVAILABLE_MEM))) {
                                        productContentValues.put(MEM, mSpecVariant.getString(AppConstant.AVAILABLE_MEM));
                                    }

                                    productContentValues.put(PID, pId);
                                    productContentValues.put(CID, cId);
                                    productContentValues.put(GCID, gcId);
                                    productContentValues.put(NAME, mVariant.getString(AppConstant.TITLE));
                                    productContentValues.put(AMOUNT, mVariant.getInt(AppConstant.AMOUNT));
                                    productContentValues.put(DISCOUNT, mVariant.getInt(AppConstant.DISCOUNT_PERCENTAGE));
                                    productContentValues.put(STOCK_COUNT, productBaseObj.getInt(AppConstant.STOCK_COUNT));
                                    productContentValues.put(PRODUCT_DESC, productBaseObj.getString(AppConstant.PRODUCT_DESC));
                                    productContentValues.put(WARRENTY, productBaseObj.getString(AppConstant.WARRENTY));
                                    productContentValues.put(PRODUCT_BRAND, productBaseObj.getString(AppConstant.PRODUCT_BRAND));
                                    productContentValues.put(WEIGHT, specObj.getString(AppConstant.WEIGHT));
                                    if (!TextUtils.isEmpty(specObj.getString(AppConstant.SIM_TYPE))) {
                                        productContentValues.put(SIM_TYPE, specObj.getString(AppConstant.SIM_TYPE));
                                    }
                                    productContentValues.put(MODEL_NAME, specObj.getString(AppConstant.MODEL_NAME));
                                    productContentValues.put(MODEL_NUMBER, specObj.getString(AppConstant.MODEL_NUMBER));

                                    db.insertOrThrow(PRODUCT_TABLE, null, productContentValues);

                                    JSONObject imgObj = jsonObject.getJSONObject(AppConstant.IMAGE_URLS);
                                    JSONArray imgArray = imgObj.getJSONArray(mVariant.getString(AppConstant.TYPE));
                                    for (int s = 0; s < imgArray.length(); s++) {
                                        ContentValues imgContent = new ContentValues();
                                        imgContent.put(PID, pId);
                                        imgContent.put(URL, imgArray.getString(s));
                                        db.insertOrThrow(PRODUCT_IMAGE_TABLE, null, imgContent);
                                    }
                                }
                            }
                        }
                    }

                }
                JSONArray trendArray = jsonObject.getJSONArray(AppConstant.TRENDS);
                for (int t = 0; t < trendArray.length(); t++) {
                    JSONObject mTrendObj = trendArray.getJSONObject(t);
                    Cursor cursor = db.query(PRODUCT_TABLE, new String[]{mTrendObj.getString(AppConstant.OFFER_PRODUCT).equals("C") ? CID : PID}, NAME + " =?", new String[]{mTrendObj.getString(AppConstant.NAME)}, null, null, null, null);
                    if (cursor.moveToFirst()) {
                        ContentValues trendsValue = new ContentValues();
                        trendsValue.put(ID, cursor.getString(0));
                        trendsValue.put(URL, mTrendObj.getString("url"));
                        trendsValue.put(DISCOUNT, mTrendObj.getInt(DISCOUNT));
                        trendsValue.put(OFFER_PRODUCT, mTrendObj.getString(AppConstant.OFFER_PRODUCT));
                        db.insertOrThrow(OFFER_TREND, null, trendsValue);
                        cursor.close();
                    }
                }
                return true;
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        } catch (SQLiteException ex) {
            ex.printStackTrace();
            DatabaseErrorHandler.handleError("DB_DATA_INIT_ERROR", ex.getMessage());
        }
        return false;
    }

    public User isUserAlreadyExist(String mobileNo) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;
        User user;
        try {
            cursor = db.query(USER_TABLE, null, MOBILE_NO + " = ?", new String[]{mobileNo}, null, null, null, null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                user = new User();
                user.setUid(cursor.getString(cursor.getColumnIndex(UID)));
                user.setName(cursor.getString(cursor.getColumnIndex(NAME)));
                user.setEmail(cursor.getString(cursor.getColumnIndex(EMAIL)));
                user.setMobileNo(cursor.getString(cursor.getColumnIndex(MOBILE_NO)));
                user.setProfileUri(cursor.getString(cursor.getColumnIndex(PROFILE)));
                return user;
            } else {
                return null;
            }
        } catch (SQLiteException ex) {
            DatabaseErrorHandler.handleError("CHECK_USER_EXIST", ex.getMessage());
        }
        if (cursor != null) {
            cursor.close();
        }
        return null;
    }

    public boolean createUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(UID, user.getUid());
            contentValues.put(NAME, user.getName());
            contentValues.put(EMAIL, user.getEmail());
            contentValues.put(MOBILE_NO, user.getMobileNo());
            db.insertOrThrow(USER_TABLE, null, contentValues);
            return prepareCoupons(user.getUid());
        } catch (SQLiteException ex) {
            DatabaseErrorHandler.handleError("CREATE_USER", ex.getMessage());
        }
        return false;
    }

    public int updateUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(UID, user.getUid());
            contentValues.put(NAME, user.getName());
            contentValues.put(EMAIL, user.getEmail());
            contentValues.put(MOBILE_NO, user.getMobileNo());
            contentValues.put(PROFILE, user.getProfileUri());
            return db.update(USER_TABLE, contentValues, UID + " =?", new String[]{user.getUid()});
        } catch (SQLiteException ex) {
            DatabaseErrorHandler.handleError("UPDATE_USER", ex.getMessage());
        }
        return 0;
    }

    private boolean prepareCoupons(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            try {
                String jsonString = Utils.getStringJSON(context);
                JSONObject jsonObject = new JSONObject(jsonString);
                JSONArray jsonArray = jsonObject.getJSONArray("coupons");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject mJsonObject = jsonArray.getJSONObject(i);
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(UID, id);
                    contentValues.put(COUPON_ID, UUID.randomUUID().toString());
                    contentValues.put(COUPON_TITLE, mJsonObject.getString(AppConstant.TITLE));
                    contentValues.put(DISCOUNT, mJsonObject.getInt(DISCOUNT));
                    contentValues.put(URL, mJsonObject.getString(URL));
                    contentValues.put(REDEEMED_STATE, "0");
                    db.insertOrThrow(COUPONS_TABLE, null, contentValues);
                }
                return true;
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        } catch (SQLiteException ex) {
            DatabaseErrorHandler.handleError("PREPARE_COUPON", ex.getMessage());
        }
        return false;
    }

    public int updateUserAddress(ContentValues contentValues, String selection, String[] selectionArgs) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            return db.update(USER_TABLE, contentValues, selection, selectionArgs);
        } catch (SQLiteException ex) {
            DatabaseErrorHandler.handleError("UPDATE_USER_ADDRESS", ex.getMessage());
        }
        return 0;
    }

    public int updateAddress(ContentValues contentValues, String selection, String[] selectionArgs) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            return db.update(ADDRESS_TABLE, contentValues, selection, selectionArgs);
        } catch (SQLiteException ex) {
            DatabaseErrorHandler.handleError("UPDATE_ADDRESS", ex.getMessage());
        }
        return 0;
    }

    public Cursor getCouponList(@Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs) {
        SQLiteDatabase db = this.getReadableDatabase();
        try {
            return db.query(COUPONS_TABLE, projection, selection, selectionArgs, null, null, null, null);
        } catch (SQLiteException ex) {
            DatabaseErrorHandler.handleError("GET_LIST_COUPON", ex.getMessage());
        }
        return null;
    }

    public Cursor getAddressById(String[] projection, String selection, String[] selectionArgs) {
        SQLiteDatabase db = this.getReadableDatabase();
        try {
            return db.query(ADDRESS_TABLE, projection, selection, selectionArgs, null, null, null, null);
        } catch (SQLiteException ex) {
            DatabaseErrorHandler.handleError("GET_ADDRESS_BY_ID", ex.getMessage());
        }
        return null;
    }

    public List<Address> getAddresses(String uid) {
        SQLiteDatabase db = this.getReadableDatabase();
        List<Address> list = new ArrayList<>();
        try {
            Cursor cursor = db.query(ADDRESS_TABLE, null, UID + " =?", new String[]{uid}, null, null, null, null);
            if (cursor.moveToFirst()) {
                do {
                    Address address = new Address();
                    address.setAddressId(cursor.getString(cursor.getColumnIndex(ADDRESS_ID)));
                    address.setFirstName(cursor.getString(cursor.getColumnIndex(FIRST_NAME)));
                    address.setLastName(cursor.getString(cursor.getColumnIndex(LAST_NAME)));
                    address.setDoorNo(cursor.getString(cursor.getColumnIndex(DOOR_NO)));
                    address.setArea(cursor.getString(cursor.getColumnIndex(AREA)));
                    address.setCity(cursor.getString(cursor.getColumnIndex(CITY)));
                    address.setState(cursor.getString(cursor.getColumnIndex(STATE)));
                    address.setPinCode(cursor.getString(cursor.getColumnIndex(PINCODE)));
                    address.setAltNo(cursor.getString(cursor.getColumnIndex(ALT_MOBILE_NO)));
                    address.setAddressType(cursor.getString(cursor.getColumnIndex(ADDRESS_TYPE)));
                    list.add(address);
                } while (cursor.moveToNext());
                cursor.close();
            }
        } catch (SQLiteException ex) {
            DatabaseErrorHandler.handleError("GET_ADDRESSES", ex.getMessage());
        }

        return list;
    }

    public HomeFeed getHomeFeed(User user, int pagination) {
        int start = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        try {
            HomeFeed homeFeed = new HomeFeed();
            if (pagination == 0) {
                Cursor trendCursor = db.query(OFFER_TREND, null, null, null, null, null, null, null);
                if (trendCursor.moveToFirst()) {
                    List<TrendsDeal> trendsDealList = new ArrayList<>();
                    do {
                        TrendsDeal trendsDeal = new TrendsDeal();
                        if (trendCursor.getString(2).equals("C")) {
                            Cursor categoryCursor = db.query(CATEGORY_TABLE, null, CID + " =?", new String[]{trendCursor.getString(0)}, null, null, null, null);
                            categoryCursor.moveToFirst();
                            trendsDeal.setName(categoryCursor.getString(categoryCursor.getColumnIndex(NAME)));
                            trendsDeal.setcId(trendCursor.getString(0));
                            trendsDeal.setDisCount(trendCursor.getInt(3));
                            categoryCursor.close();
                        } else {
                            Cursor prodCursor = db.query(PRODUCT_TABLE, new String[]{NAME, DISCOUNT}, PID + " =?", new String[]{trendCursor.getString(0)}, null, null, null, null);
                            prodCursor.moveToFirst();
                            trendsDeal.setpId(trendCursor.getString(0));
                            trendsDeal.setName(prodCursor.getString(0));
                            trendsDeal.setDisCount(prodCursor.getInt(1));
                            prodCursor.close();
                        }
                        trendsDeal.setUrl(trendCursor.getString(1));
                        trendsDeal.setOfferProduct(trendCursor.getString(2));
                        trendsDealList.add(trendsDeal);
                    } while (trendCursor.moveToNext());
                    trendCursor.close();
                    homeFeed.setTrendsDealList(trendsDealList);
                }

                Cursor gCursor = db.query(GENERAL_CATEGORY_TABLE, null, null, null, null, null, null, null);
                if (gCursor.moveToFirst()) {
                    List<GeneralCategory> generalCategoryList = new ArrayList<>();
                    do {
                        GeneralCategory generalCategory = new GeneralCategory();
                        generalCategory.setGcId(gCursor.getString(0));
                        generalCategory.setName(gCursor.getString(1));
                        generalCategory.setUrl(gCursor.getString(2));
                        generalCategoryList.add(generalCategory);
                    } while (gCursor.moveToNext());
                    gCursor.close();
                    homeFeed.setGeneralCategoryList(generalCategoryList);
                }
            }

            String[] selectionArgs = new String[user.getSelecedIds().size()];
            selectionArgs = user.getSelecedIds().toArray(selectionArgs);
            //Cursor suggestedProduct = db.query(PRODUCT_TABLE, null, CID + " = ?", selectionArgs, null, null, null, "10");
            if (pagination != 0) {
                start = pagination * OFFSET;
            }
            Cursor suggestedProduct = db.rawQuery("SELECT * FROM " + PRODUCT_TABLE + " WHERE " + CID + " IN (" + makePlaceholders(selectionArgs.length) + ") LIMIT 10 OFFSET " + start, selectionArgs);
            if (suggestedProduct.moveToFirst()) {
                List<Product> productList = new ArrayList<>();
                do {
                    Cursor productImage = db.query(PRODUCT_IMAGE_TABLE, null, PID + " = ?", new String[]{suggestedProduct.getString(suggestedProduct.getColumnIndex(PID))}, null, null, null, null);
                    Product product = new Product();
                    if (productImage.moveToFirst()) {
                        product.setpId(suggestedProduct.getString(suggestedProduct.getColumnIndex(PID)));
                        product.setGcId(suggestedProduct.getString(suggestedProduct.getColumnIndex(GCID)));
                        product.setcId(suggestedProduct.getString(suggestedProduct.getColumnIndex(CID)));
                        product.setName(suggestedProduct.getString(suggestedProduct.getColumnIndex(NAME)));
                        product.setAmount(suggestedProduct.getInt(suggestedProduct.getColumnIndex(AMOUNT)));
                        product.setDisCount(suggestedProduct.getInt(suggestedProduct.getColumnIndex(DISCOUNT)));
                        product.setUrl(productImage.getString(productImage.getColumnIndex(URL)));
                        product.setProductBrand(suggestedProduct.getString(suggestedProduct.getColumnIndex(PRODUCT_BRAND)));
                    }
                    productImage.close();
                    productList.add(product);
                } while (suggestedProduct.moveToNext());
                suggestedProduct.close();
                Collections.shuffle(productList);
                homeFeed.setProductList(productList);
            }
            return homeFeed;
        } catch (SQLiteException ex) {
            DatabaseErrorHandler.handleError("GET_HOME_FEED", ex.getMessage());
        }
        return null;
    }


    public List<Category> getCategories(String gcId) {
        SQLiteDatabase db = this.getReadableDatabase();
        String selection = gcId != null ? GCID + " =? " : null;
        String[] selectionArgs = gcId != null ? new String[]{gcId} : null;
        try {
            Cursor cursor = db.query(CATEGORY_TABLE, null, selection, selectionArgs, null, null, null, null);
            if (cursor.moveToFirst()) {
                List<Category> categories = new ArrayList<>();
                do {
                    Category category = new Category();
                    category.setName(cursor.getString(cursor.getColumnIndex(NAME)));
                    category.setGcId(cursor.getString(cursor.getColumnIndex(GCID)));
                    category.setUrl(cursor.getString(cursor.getColumnIndex(URL)));
                    category.setcId(cursor.getString(cursor.getColumnIndex(CID)));
                    categories.add(category);
                } while (cursor.moveToNext());
                cursor.close();
                return categories;
            }
        } catch (SQLiteException ex) {
            DatabaseErrorHandler.handleError("GET_CATEGORIES", ex.getMessage());
        }
        return null;
    }

    public boolean registerUserInterest(List<Category> list, User user) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            for (Category category : list) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(UID, user.getUid());
                contentValues.put(CID, category.getcId());
                db.insertOrThrow(SUGGESTION_TABLE, null, contentValues);
            }
            return true;
        } catch (SQLiteException ex) {
            DatabaseErrorHandler.handleError("REG_INTEREST", ex.getMessage());
        }
        return false;
    }

    public List<String> getUserInterest(String uid) {
        SQLiteDatabase db = this.getReadableDatabase();
        List<String> list = new ArrayList<>();
        try {
            Cursor cursor = db.query(SUGGESTION_TABLE, null, UID + " =?", new String[]{uid}, null, null, null, null);
            if (cursor.moveToFirst()) {
                do {
                    list.add(cursor.getString(1));
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (SQLiteException ex) {
            DatabaseErrorHandler.handleError("GET_INTEREST", ex.getMessage());
        }
        return list;
    }

    public Product getProductInfo(String productId) {
        User user = PreferenceManager.getUserInfo(context);
        SQLiteDatabase db = this.getReadableDatabase();
        Product product = null;
        try {
            Cursor cursor = db.query(PRODUCT_TABLE, null, PID + " =?", new String[]{productId}, null, null, null, null);
            if (cursor.moveToFirst()) {
                product = new Product();
                product.setGcId(cursor.getString(cursor.getColumnIndex(GCID)));
                product.setpId(cursor.getString(cursor.getColumnIndex(PID)));
                product.setcId(cursor.getString(cursor.getColumnIndex(CID)));
                product.setName(cursor.getString(cursor.getColumnIndex(NAME)));
                product.setProductBrand(cursor.getString(cursor.getColumnIndex(PRODUCT_BRAND)));
                product.setProductDesc(cursor.getString(cursor.getColumnIndex(PRODUCT_DESC)));
                product.setDisCount(cursor.getInt(cursor.getColumnIndex(DISCOUNT)));
                product.setAmount(cursor.getInt(cursor.getColumnIndex(AMOUNT)));
                product.setColor(cursor.getString(cursor.getColumnIndex(COLOR)));
                product.setMemory(cursor.getString(cursor.getColumnIndex(MEM)));
                product.setRam(cursor.getString(cursor.getColumnIndex(RAM)));
                product.setHertz(cursor.getString(cursor.getColumnIndex(HERTZ)));
                product.setModelName(cursor.getString(cursor.getColumnIndex(MODEL_NAME)));
                product.setModelNumber(cursor.getString(cursor.getColumnIndex(MODEL_NUMBER)));
                product.setSimType(cursor.getString(cursor.getColumnIndex(SIM_TYPE)));
                product.setWarrenty(cursor.getString(cursor.getColumnIndex(WARRENTY)));
                product.setWeight(cursor.getString(cursor.getColumnIndex(WEIGHT)));

                Cursor colorCount = db.rawQuery("SELECT * FROM " + PRODUCT_TABLE + " WHERE " + NAME + "=? AND " + CID + " =? AND " + COLOR + " IS NOT NULL", new String[]{cursor.getString(cursor.getColumnIndex(NAME)), cursor.getString(cursor.getColumnIndex(CID))});
                Cursor ramCount = db.rawQuery("SELECT * FROM " + PRODUCT_TABLE + " WHERE " + NAME + "=? AND " + CID + " =? AND " + RAM + " IS NOT NULL", new String[]{cursor.getString(cursor.getColumnIndex(NAME)), cursor.getString(cursor.getColumnIndex(CID))});
                Cursor storageCount = db.rawQuery("SELECT * FROM " + PRODUCT_TABLE + " WHERE " + NAME + "=? AND " + CID + " =? AND " + MEM + " IS NOT NULL", new String[]{cursor.getString(cursor.getColumnIndex(NAME)), cursor.getString(cursor.getColumnIndex(CID))});
                Cursor hertzCount = db.rawQuery("SELECT * FROM " + PRODUCT_TABLE + " WHERE " + NAME + "=? AND " + CID + " =? AND " + HERTZ + " IS NOT NULL", new String[]{cursor.getString(cursor.getColumnIndex(NAME)), cursor.getString(cursor.getColumnIndex(CID))});

                product.setColorCount(colorCount.getCount());
                if (colorCount.moveToFirst()) {
                    List<String> colorList = new ArrayList<>();
                    do {
                        colorList.add(colorCount.getString(colorCount.getColumnIndex(COLOR)));
                    } while (colorCount.moveToNext());
                    product.setColorList(colorList);
                }

                product.setRamCount(ramCount.getCount());
                if (ramCount.moveToFirst()) {
                    List<String> ramList = new ArrayList<>();
                    do {
                        ramList.add(ramCount.getString(ramCount.getColumnIndex(RAM)));
                    } while (ramCount.moveToNext());
                    product.setRamList(ramList);
                }
                product.setStorageCount(storageCount.getCount());
                if (storageCount.moveToFirst()) {
                    List<String> storageList = new ArrayList<>();
                    do {
                        storageList.add(storageCount.getString(storageCount.getColumnIndex(MEM)));
                    } while (storageCount.moveToNext());
                    product.setMemList(storageList);
                }

                product.setHertzCount(hertzCount.getCount());
                if (hertzCount.moveToFirst()) {
                    List<String> hertzList = new ArrayList<>();
                    do {
                        hertzList.add(hertzCount.getString(hertzCount.getColumnIndex(HERTZ)));
                    } while (hertzCount.moveToNext());
                    product.setHertzList(hertzList);
                }

                colorCount.close();
                ramCount.close();
                storageCount.close();
                hertzCount.close();
                List<Review> reviewList = new ArrayList<>();
                Cursor ratingCountCursor = db.query(REVIEW_TABLE, null, PID + " =?", new String[]{productId}, null, null, null, null);
                Cursor ratingCursor = db.query(REVIEW_TABLE, null, PID + " =? ", new String[]{productId}, null, null, null, null);
                product.setReviewCount(ratingCountCursor.getCount());
                double count = 0;
                if (ratingCountCursor.moveToFirst()) {
                    product.setReviewCount(ratingCountCursor.getCount());
                    int total = ratingCountCursor.getCount();
                    do {
                        count = count + Double.parseDouble(ratingCountCursor.getString(ratingCountCursor.getColumnIndex(RATING)));
                    } while (ratingCountCursor.moveToNext());
                    double i = ((double) count / total);
                    product.setRating(i);
                }
                ratingCountCursor.close();
                if (ratingCursor.moveToFirst()) {
                    do {
                        Review review = new Review();
                        review.setName(ratingCursor.getString(ratingCursor.getColumnIndex(NAME)));
                        review.setUid(ratingCursor.getString(ratingCursor.getColumnIndex(UID)));
                        review.setContent(ratingCursor.getString(ratingCursor.getColumnIndex(REVIEW_CONTENT)));
                        List<String> pathList = new ArrayList<>();
                        if (!TextUtils.isEmpty(ratingCursor.getString(ratingCursor.getColumnIndex(IMG_ONE)))) {
                            pathList.add(ratingCursor.getString(ratingCursor.getColumnIndex(IMG_ONE)));
                        }
                        if (!TextUtils.isEmpty(ratingCursor.getString(ratingCursor.getColumnIndex(IMG_TWO)))) {
                            pathList.add(ratingCursor.getString(ratingCursor.getColumnIndex(IMG_TWO)));
                        }
                        if (!TextUtils.isEmpty(ratingCursor.getString(ratingCursor.getColumnIndex(IMG_THREE)))) {
                            pathList.add(ratingCursor.getString(ratingCursor.getColumnIndex(IMG_THREE)));
                        }
                        review.setList(pathList);
                        review.setRating(ratingCursor.getString(ratingCursor.getColumnIndex(RATING)));
                        reviewList.add(review);
                    } while (ratingCursor.moveToNext());
                }
                ratingCursor.close();
                product.setReviewList(reviewList);
                List<String> imgList = new ArrayList<>();
                Cursor imgCursor = db.query(PRODUCT_IMAGE_TABLE, null, PID + " =?", new String[]{cursor.getString(cursor.getColumnIndex(PID))}, null, null, null, null);
                if (imgCursor.moveToFirst()) {
                    do {
                        imgList.add(imgCursor.getString(1));
                    } while (imgCursor.moveToNext());
                    product.setUrl(imgList);
                }
                imgCursor.close();
            }
            cursor.close();
            if (product != null) {
                setSpecList(product);
            }
            return product;
        } catch (SQLiteException ex) {
            DatabaseErrorHandler.handleError("GET_PRODUCT_INFO", ex.getMessage());
        }
        return product;
    }

    public Product getProductBySpecification(Product product) {
        SQLiteDatabase db = this.getReadableDatabase();
        try {
            List<String> selectionArgs = new ArrayList<>();
            selectionArgs.add(product.getcId());
            selectionArgs.add(product.getName());
            String selection = CID + " =? AND " + NAME + " =?";
            if (!TextUtils.isEmpty(product.getColor())) {
                selection += " AND " + COLOR + " =?";
                selectionArgs.add(product.getColor());
            }
            if (!TextUtils.isEmpty(product.getRam())) {
                selection += " AND " + RAM + " =?";
                selectionArgs.add(product.getRam());
            }
            if (!TextUtils.isEmpty(product.getMemory())) {
                selection += " AND " + MEM + " =?";
                selectionArgs.add(product.getMemory());
            }
            if (!TextUtils.isEmpty(product.getHertz())) {
                selection += " AND " + HERTZ + " =?";
                selectionArgs.add(product.getHertz());
            }

            String[] sArgs = new String[selectionArgs.size()];
            sArgs = selectionArgs.toArray(sArgs);
            Cursor cursor = db.query(PRODUCT_TABLE, null, selection, sArgs, null, null, null, null);
            if (cursor.moveToFirst()) {
                String pId = cursor.getString(cursor.getColumnIndex(PID));
                cursor.close();
                return getProductInfo(pId);
            }
        } catch (SQLiteException ex) {
            DatabaseErrorHandler.handleError("GET_PROD_SPEC", ex.getMessage());
        }
        return null;
    }

    public List<Product> getProductsByFilter(Product product, int pagination, String sort) {
        int start = 0;
        if (pagination != 0) {
            start = pagination * OFFSET;
        }
        List<String> selectionArgs = new ArrayList<>();
        selectionArgs.add(product.getcId());
        selectionArgs.add(String.valueOf(product.getAmount()));
        String selection = CID + " =? AND (" + AMOUNT + " >?";
        if (!TextUtils.isEmpty(product.getColor())) {
            selection += " AND " + COLOR + " =?";
            selectionArgs.add(product.getColor());
        }

        if (!TextUtils.isEmpty(product.getRam())) {
            selection += " AND " + RAM + " =?";
            selectionArgs.add(product.getRam());
        }
        if (!TextUtils.isEmpty(product.getMemory())) {
            selection += " AND " + MEM + " =?";
            selectionArgs.add(product.getMemory());
        }
        if (!TextUtils.isEmpty(product.getHertz())) {
            selection += " AND " + HERTZ + " =? ) ";
            selectionArgs.add(product.getHertz());
        } else {
            selection += " )";
        }

        if (!TextUtils.isEmpty(sort)) {
            selection += " ORDER BY amount " + sort;
        }
        selection += " LIMIT 10 OFFSET " + start;
        String[] sArgs = new String[selectionArgs.size()];
        sArgs = selectionArgs.toArray(sArgs);
        SQLiteDatabase db = this.getReadableDatabase();
        List<Product> list = new ArrayList<>();
        try {

            Cursor cursor = db.query(PRODUCT_TABLE, null, selection, sArgs, null, null, null, null);
            if (cursor.moveToFirst()) {
                do {
                    Product mproduct = new Product();
                    mproduct.setGcId(cursor.getString(cursor.getColumnIndex(GCID)));
                    mproduct.setcId(cursor.getString(cursor.getColumnIndex(CID)));
                    mproduct.setpId(cursor.getString(cursor.getColumnIndex(PID)));
                    mproduct.setColor(cursor.getString(cursor.getColumnIndex(COLOR)));
                    mproduct.setName(cursor.getString(cursor.getColumnIndex(NAME)));
                    mproduct.setAmount(cursor.getInt(cursor.getColumnIndex(AMOUNT)));
                    mproduct.setDisCount(cursor.getInt(cursor.getColumnIndex(DISCOUNT)));
                    Cursor productImage = db.query(PRODUCT_IMAGE_TABLE, null, PID + " = ?", new String[]{cursor.getString(cursor.getColumnIndex(PID))}, null, null, null, null);
                    productImage.moveToFirst();
                    mproduct.setUrl(productImage.getString(productImage.getColumnIndex(URL)));
                    productImage.close();
                    double count = 0;
                    Cursor ratingCountCursor = db.query(REVIEW_TABLE, null, PID + " =?", new String[]{cursor.getString(cursor.getColumnIndex(PID))}, null, null, null, null);
                    if (ratingCountCursor.moveToFirst()) {
                        mproduct.setReviewCount(ratingCountCursor.getCount());
                        int total = ratingCountCursor.getCount();
                        do {
                            count = count + Double.parseDouble(ratingCountCursor.getString(ratingCountCursor.getColumnIndex(RATING)));
                        } while (ratingCountCursor.moveToNext());
                        double i = ((double) count / total);
                        mproduct.setRating(i);
                    }

                    ratingCountCursor.close();
                    list.add(mproduct);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (SQLiteException ex) {
            DatabaseErrorHandler.handleError("GET_PROD_FILTER", ex.getMessage());
        }
        return list;
    }

    public Product getSpecificationByProduct(String cid) {
        SQLiteDatabase db = this.getReadableDatabase();
        try {
            Product product = new Product();
            Cursor maxValue = db.query(PRODUCT_TABLE, new String[]{"MAX(" + AMOUNT + ") AS MAX", "MIN(" + AMOUNT + ") AS MIN"}, CID + " =?", new String[]{cid}, null, null, null);
            maxValue.moveToFirst();
            product.setMax(maxValue.getInt(0));
            product.setMin(maxValue.getInt(1));
            maxValue.close();
            Cursor colorCount = db.rawQuery("SELECT DISTINCT " + COLOR + " FROM " + PRODUCT_TABLE + " WHERE " + CID + " =? AND " + COLOR + " IS NOT NULL", new String[]{cid});
            Cursor ramCount = db.rawQuery("SELECT DISTINCT  " + RAM + "  FROM " + PRODUCT_TABLE + " WHERE " + CID + " =? AND " + RAM + " IS NOT NULL", new String[]{cid});
            Cursor storageCount = db.rawQuery("SELECT DISTINCT " + MEM + "  FROM " + PRODUCT_TABLE + " WHERE " + CID + " =? AND " + MEM + " IS NOT NULL", new String[]{cid});
            Cursor hertzCount = db.rawQuery("SELECT DISTINCT " + HERTZ + " FROM " + PRODUCT_TABLE + " WHERE " + CID + " =? AND " + HERTZ + " IS NOT NULL", new String[]{cid});

            product.setColorCount(colorCount.getCount());
            if (colorCount.moveToFirst()) {
                List<String> colorList = new ArrayList<>();
                do {
                    colorList.add(colorCount.getString(colorCount.getColumnIndex(COLOR)));
                } while (colorCount.moveToNext());
                product.setColorList(colorList);
            }

            product.setRamCount(ramCount.getCount());
            if (ramCount.moveToFirst()) {
                List<String> ramList = new ArrayList<>();
                do {
                    ramList.add(ramCount.getString(ramCount.getColumnIndex(RAM)));
                } while (ramCount.moveToNext());
                product.setRamList(ramList);
            }
            product.setStorageCount(storageCount.getCount());
            if (storageCount.moveToFirst()) {
                List<String> storageList = new ArrayList<>();
                do {
                    storageList.add(storageCount.getString(storageCount.getColumnIndex(MEM)));
                } while (storageCount.moveToNext());
                product.setMemList(storageList);
            }

            product.setHertzCount(hertzCount.getCount());
            if (hertzCount.moveToFirst()) {
                List<String> hertzList = new ArrayList<>();
                do {
                    hertzList.add(hertzCount.getString(hertzCount.getColumnIndex(HERTZ)));
                } while (hertzCount.moveToNext());
                product.setHertzList(hertzList);
            }

            colorCount.close();
            ramCount.close();
            storageCount.close();
            hertzCount.close();
            return product;
        } catch (SQLiteException ex) {
            DatabaseErrorHandler.handleError("GET_SPEC_BY_PROD", ex.getMessage());
        }
        return null;
    }

    public List<Product> getRelatedProduct(String id, int pagination) {
        int start = 0;
        if (pagination != 0) {
            start = pagination * OFFSET;
        }
        SQLiteDatabase db = this.getReadableDatabase();
        List<Product> list = new ArrayList<>();
        try {
            Cursor relatedProduct = db.rawQuery("SELECT * FROM " + PRODUCT_TABLE + " WHERE " + CID + "=? LIMIT 10 OFFSET " + start, new String[]{id});
            if (relatedProduct.moveToFirst()) {
                do {
                    Product mproduct = new Product();
                    mproduct.setGcId(relatedProduct.getString(relatedProduct.getColumnIndex(GCID)));
                    mproduct.setcId(relatedProduct.getString(relatedProduct.getColumnIndex(CID)));
                    mproduct.setpId(relatedProduct.getString(relatedProduct.getColumnIndex(PID)));
                    mproduct.setName(relatedProduct.getString(relatedProduct.getColumnIndex(NAME)));
                    mproduct.setAmount(relatedProduct.getInt(relatedProduct.getColumnIndex(AMOUNT)));
                    mproduct.setDisCount(relatedProduct.getInt(relatedProduct.getColumnIndex(DISCOUNT)));
                    Cursor productImage = db.query(PRODUCT_IMAGE_TABLE, null, PID + " = ?", new String[]{relatedProduct.getString(relatedProduct.getColumnIndex(PID))}, null, null, null, null);
                    productImage.moveToFirst();
                    mproduct.setUrl(productImage.getString(productImage.getColumnIndex(URL)));
                    productImage.close();
                    double count = 0;
                    Cursor ratingCountCursor = db.query(REVIEW_TABLE, null, PID + " =?", new String[]{id}, null, null, null, null);
                    if (ratingCountCursor.moveToFirst()) {
                        mproduct.setReviewCount(ratingCountCursor.getCount());
                        int total = ratingCountCursor.getCount();
                        do {
                            count = count + Double.parseDouble(ratingCountCursor.getString(ratingCountCursor.getColumnIndex(RATING)));
                        } while (ratingCountCursor.moveToNext());
                        double i = ((double) count / total);
                        mproduct.setRating(i);
                    }

                    ratingCountCursor.close();
                    list.add(mproduct);
                } while (relatedProduct.moveToNext());
                relatedProduct.close();
                return list;
            }
        } catch (SQLiteException ex) {
            DatabaseErrorHandler.handleError("GET_RELATED_PROD", ex.getMessage());
        }
        return list;
    }

    public List<Product> getProductList(String keyWord, int pagination, String sort) {
        int start = 0;
        if (pagination != 0) {
            start = pagination * OFFSET;
        }
        String selection = NAME + " LIKE ? OR " + PRODUCT_DESC + " LIKE ? ";
        if (!TextUtils.isEmpty(sort)) {
            selection += " ORDER BY amount " + sort;
        }
        selection += " LIMIT 10 OFFSET " + start;
        String selectionArgs = '%' + keyWord + '%';
        SQLiteDatabase db = this.getReadableDatabase();
        List<Product> list = new ArrayList<>();
        try {
            Cursor relatedProduct = db.rawQuery("SELECT * FROM " + PRODUCT_TABLE + " WHERE " + selection, new String[]{selectionArgs, selectionArgs});
            if (relatedProduct.moveToFirst()) {
                do {
                    Product mproduct = new Product();
                    mproduct.setGcId(relatedProduct.getString(relatedProduct.getColumnIndex(GCID)));
                    mproduct.setcId(relatedProduct.getString(relatedProduct.getColumnIndex(CID)));
                    mproduct.setpId(relatedProduct.getString(relatedProduct.getColumnIndex(PID)));
                    mproduct.setName(relatedProduct.getString(relatedProduct.getColumnIndex(NAME)));
                    mproduct.setAmount(relatedProduct.getInt(relatedProduct.getColumnIndex(AMOUNT)));
                    mproduct.setDisCount(relatedProduct.getInt(relatedProduct.getColumnIndex(DISCOUNT)));
                    Cursor productImage = db.query(PRODUCT_IMAGE_TABLE, null, PID + " = ?", new String[]{relatedProduct.getString(relatedProduct.getColumnIndex(PID))}, null, null, null, null);
                    productImage.moveToFirst();
                    mproduct.setUrl(productImage.getString(productImage.getColumnIndex(URL)));
                    productImage.close();
                    double count = 0;
                    Cursor ratingCountCursor = db.query(REVIEW_TABLE, null, PID + " =?", new String[]{relatedProduct.getString(relatedProduct.getColumnIndex(PID))}, null, null, null, null);
                    if (ratingCountCursor.moveToFirst()) {
                        mproduct.setReviewCount(ratingCountCursor.getCount());
                        int total = ratingCountCursor.getCount();
                        do {
                            count = count + Double.parseDouble(ratingCountCursor.getString(ratingCountCursor.getColumnIndex(RATING)));
                        } while (ratingCountCursor.moveToNext());
                        double i = ((double) count / total);
                        mproduct.setRating(i);
                    }

                    ratingCountCursor.close();
                    list.add(mproduct);
                } while (relatedProduct.moveToNext());
                relatedProduct.close();
                return list;
            }
        } catch (SQLiteException ex) {
            DatabaseErrorHandler.handleError("GET_PROD_LIST", ex.getMessage());
        }
        return list;
    }

    public List<Product> getProductListByCategory(String cid, int pagination, String sort) {
        int start = 0;
        if (pagination != 0) {
            start = pagination * OFFSET;
        }
        String selection = CID + " = ? ";
        if (!TextUtils.isEmpty(sort)) {
            selection += " ORDER BY amount " + sort;
        }
        selection += " LIMIT 10 OFFSET " + start;
        SQLiteDatabase db = this.getReadableDatabase();
        List<Product> list = new ArrayList<>();
        try {
            Cursor relatedProduct = db.rawQuery("SELECT * FROM " + PRODUCT_TABLE + " WHERE " + selection, new String[]{cid});
            if (relatedProduct.moveToFirst()) {
                do {
                    Product mproduct = new Product();
                    mproduct.setGcId(relatedProduct.getString(relatedProduct.getColumnIndex(GCID)));
                    mproduct.setcId(relatedProduct.getString(relatedProduct.getColumnIndex(CID)));
                    mproduct.setpId(relatedProduct.getString(relatedProduct.getColumnIndex(PID)));
                    mproduct.setName(relatedProduct.getString(relatedProduct.getColumnIndex(NAME)));
                    mproduct.setAmount(relatedProduct.getInt(relatedProduct.getColumnIndex(AMOUNT)));
                    mproduct.setDisCount(relatedProduct.getInt(relatedProduct.getColumnIndex(DISCOUNT)));
                    Cursor productImage = db.query(PRODUCT_IMAGE_TABLE, null, PID + " = ?", new String[]{relatedProduct.getString(relatedProduct.getColumnIndex(PID))}, null, null, null, null);
                    productImage.moveToFirst();
                    mproduct.setUrl(productImage.getString(productImage.getColumnIndex(URL)));
                    productImage.close();
                    double count = 0;
                    Cursor ratingCountCursor = db.query(REVIEW_TABLE, null, PID + " =?", new String[]{relatedProduct.getString(relatedProduct.getColumnIndex(PID))}, null, null, null, null);
                    if (ratingCountCursor.moveToFirst()) {
                        mproduct.setReviewCount(ratingCountCursor.getCount());
                        int total = ratingCountCursor.getCount();
                        do {
                            count = count + Double.parseDouble(ratingCountCursor.getString(ratingCountCursor.getColumnIndex(RATING)));
                        } while (ratingCountCursor.moveToNext());
                        double i = ((double) count / total);
                        mproduct.setRating(i);
                    }

                    ratingCountCursor.close();
                    list.add(mproduct);
                } while (relatedProduct.moveToNext());
                relatedProduct.close();
                return list;
            }
        } catch (SQLiteException ex) {
            DatabaseErrorHandler.handleError("PROD_BY_CATRGY", ex.getMessage());
        }
        return list;
    }

    private void setSpecList(Product product) {
        List<Specification> list = new ArrayList<>();
        if (!TextUtils.isEmpty(product.getModelName())) {
            list.add(new Specification(context.getString(R.string.modelName), product.getModelName()));
        }
        if (!TextUtils.isEmpty(product.getModelNumber())) {
            list.add(new Specification(context.getString(R.string.modelNumber), product.getModelNumber()));
        }
        if (!TextUtils.isEmpty(product.getProductBrand())) {
            list.add(new Specification(context.getString(R.string.productBrand), product.getProductBrand()));
        }
        if (!TextUtils.isEmpty(product.getSimType())) {
            list.add(new Specification(context.getString(R.string.simType), product.getSimType()));
        }
        if (!TextUtils.isEmpty(product.getWeight())) {
            list.add(new Specification(context.getString(R.string.weight), product.getWeight()));
        }
        product.setSpecificationList(list);
    }

    public int getRatingForProduct(String id) {
        SQLiteDatabase db = this.getReadableDatabase();
        double count = 0;
        try {
            Cursor cursor = db.query(REVIEW_TABLE, null, PID + " =?", new String[]{id}, null, null, null, null);
            if (cursor.moveToFirst()) {
                int total = cursor.getCount();
                do {
                    count = count + Double.parseDouble(cursor.getString(cursor.getColumnIndex(RATING)));
                } while (cursor.moveToNext());
                double i = ((double) count / total);
                return (int) Math.round(i);
            }
            cursor.close();
        } catch (SQLiteException ex) {
            ex.printStackTrace();
        }
        return 0;
    }

    public boolean updateWishList(String uId, String pId, int qty, boolean isForAdd) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            if (isForAdd) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(UID, uId);
                contentValues.put(PID, pId);
                contentValues.put(QTY, qty);
                db.insertOrThrow(WISH_LIST_TABLE, null, contentValues);
            } else {
                String selection = UID + " =? AND " + PID + " =? ";
                db.delete(WISH_LIST_TABLE, selection, new String[]{uId, pId});
            }
            return true;
        } catch (SQLiteException ex) {
            DatabaseErrorHandler.handleError("WISHLIST_UPDATE", ex.getMessage());
        }
        return false;
    }

    public boolean isProductInWishList(String uId, String pId) {
        SQLiteDatabase db = this.getReadableDatabase();
        try {
            Cursor cursor = db.query(WISH_LIST_TABLE, null, UID + " =? AND " + PID + " =? ", new String[]{uId, pId}, null, null, null, null);
            boolean isAvailable = cursor.moveToFirst();
            cursor.close();
            return isAvailable;
        } catch (SQLiteException ex) {
            DatabaseErrorHandler.handleError("CHK_WISHLIST", ex.getMessage());
        }
        return false;
    }

    public void addToCart(String uId, String pId, int qty, boolean isForAdd) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            if (isForAdd) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(UID, uId);
                contentValues.put(PID, pId);
                contentValues.put(QTY, qty);
                db.insertOrThrow(CART_TABLE, null, contentValues);
            } else {
                String selection = UID + " =? AND " + PID + " =? ";
                db.delete(CART_TABLE, selection, new String[]{uId, pId});
            }
        } catch (SQLiteException ex) {
            DatabaseErrorHandler.handleError("ADD_TO_CART", ex.getMessage());
        }
    }

    public int isProductInCart(String uId, String pId) {
        SQLiteDatabase db = this.getReadableDatabase();
        int count = 0;
        try {
            Cursor cursor = db.query(CART_TABLE, null, UID + " =? AND " + PID + " =? ", new String[]{uId, pId}, null, null, null, null);
            if (cursor.moveToFirst()) {
                count = cursor.getInt(cursor.getColumnIndex(QTY));
            }
            cursor.close();
            return count;
        } catch (SQLiteException ex) {
            DatabaseErrorHandler.handleError("CHK_CART", ex.getMessage());
        }
        return count;
    }

    public long insertAddress(ContentValues contentValues) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            return db.insertOrThrow(ADDRESS_TABLE, null, contentValues);
        } catch (SQLiteException ex) {
            DatabaseErrorHandler.handleError("INSERT_ADDRESS", ex.getMessage());
        }
        return -1;
    }

    public List<Product> getCartList(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        List<Product> list = new ArrayList<>();
        try {

            Cursor cursor = db.query(CART_TABLE, null, UID + " =?", new String[]{id}, null, null, null, null);
            if (cursor.moveToFirst()) {
                do {
                    Product product = getProductInfo(cursor.getString(cursor.getColumnIndex(PID)));
                    product.setQty(cursor.getInt(cursor.getColumnIndex(QTY)));
                    list.add(product);
                } while (cursor.moveToNext());
                cursor.close();
            }

        } catch (SQLiteException ex) {
            DatabaseErrorHandler.handleError("GET_CART_LIST", ex.getMessage());
        }
        return list;
    }

    public List<Product> getWishList(String uid) {

        SQLiteDatabase db = this.getWritableDatabase();
        List<Product> list = new ArrayList<>();
        try {
            Cursor cursor = db.query(WISH_LIST_TABLE, null, UID + " =?", new String[]{uid}, null, null, null, null);
            if (cursor.moveToFirst()) {
                do {
                    Product product = getProductInfo(cursor.getString(cursor.getColumnIndex(PID)));
                    product.setQty(cursor.getInt(cursor.getColumnIndex(QTY)));
                    list.add(product);
                } while (cursor.moveToNext());
                cursor.close();
            }

        } catch (SQLiteException ex) {
            DatabaseErrorHandler.handleError("GET_WISH_LIST", ex.getMessage());
        }
        return list;
    }


    public int updateCart(String uid, String pid, String qty) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(QTY, Integer.valueOf(qty));
            String selection = UID + " =? AND " + PID + " =?";
            return db.update(CART_TABLE, contentValues, selection, new String[]{uid, pid});
        } catch (SQLiteException ex) {
            DatabaseErrorHandler.handleError("UPDATE_CART", ex.getMessage());
        }
        return 0;
    }

    public int getMyCartCount(String uid) {
        int count = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(CART_TABLE, null, UID + " =?", new String[]{uid}, null, null, null, null);
        if (cursor.moveToFirst()) {
            count = cursor.getCount();
        }
        cursor.close();
        return count;
    }

    public int getMyWishListCount(String uid) {
        int count = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(WISH_LIST_TABLE, null, UID + " =?", new String[]{uid}, null, null, null, null);
        if (cursor.moveToFirst()) {
            count = cursor.getCount();
        }
        cursor.close();
        return count;
    }

    public int getCouponCount(String uid) {
        int count = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(COUPONS_TABLE, null, UID + " =?", new String[]{uid}, null, null, null, null);
        if (cursor.moveToFirst()) {
            count = cursor.getCount();
        }
        cursor.close();
        return count;
    }

    public int getOrderCount(String uid) {
        int count = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(ORDER_TABLE, null, UID + " =?", new String[]{uid}, null, null, null, null);
        if (cursor.moveToFirst()) {
            count = cursor.getCount();
        }
        cursor.close();
        return count;
    }

    public int deleteCartItem(String uid, String pid) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            String selection = UID + " =? AND " + PID + " =?";
            return db.delete(CART_TABLE, selection, new String[]{uid, pid});
        } catch (SQLiteException ex) {
            DatabaseErrorHandler.handleError("DELETE_CART", ex.getMessage());
        }
        return 0;
    }

    public Cursor getMatchedData(String keyWord) {
        SQLiteDatabase db = this.getReadableDatabase();
        try {
            String selection = NAME + " LIKE ? OR " + PRODUCT_DESC + " LIKE ? LIMIT 15";
            String selectionArgs = '%' + keyWord + '%';
            //Cursor cursor = db.query(PRODUCT_TABLE ,null,selection ,new String[]{selectionArgs,selectionArgs},null,null,null,null);

            Cursor cursor = db.rawQuery("SELECT rowid,* FROM " + PRODUCT_TABLE + " WHERE " + selection, new String[]{selectionArgs, selectionArgs});
            return cursor;
        } catch (SQLiteException ex) {
            DatabaseErrorHandler.handleError("SEARCH_PRODUCT", ex.getMessage());
        }
        return null;
    }

    public void placeOrder(User user, Product product, Price price) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(ORDER_ID, UUID.randomUUID().toString());
            contentValues.put(UID, user.getUid());
            contentValues.put(PID, product.getpId());
            contentValues.put(ADDRESS_ID, user.getAddressId());
            contentValues.put(DELIVERY_FEE, price.getDelivery());
            contentValues.put(FINAL_AMOUNT, product.getAmount());
            contentValues.put(PAYMENT_MODE, price.getPayMode());
            contentValues.put(ORDER_APPROVE, "1");
            long numDay = Utils.getNumDays(product.getDeliveryInfo());
            long milliseconds = System.currentTimeMillis() + TimeUnit.DAYS.toMillis(numDay);
            contentValues.put(EXPECTED_DELIVERY_TIME, String.valueOf(milliseconds));
            contentValues.put(QTY, product.getQty());
            if (!TextUtils.isEmpty(price.getCouponId())) {
                contentValues.put(COUPON_ID, price.getCouponId());
            }
            contentValues.put(ORDER_APPROVE_TIME, String.valueOf(System.currentTimeMillis()));
            db.insertOrThrow(ORDER_TABLE, null, contentValues);
        } catch (SQLiteException ex) {
            ex.printStackTrace();
            DatabaseErrorHandler.handleError("PLACE_ORDER", ex.getMessage());
        }
    }

    public void placeOrder(User user, List<Product> productList, Price price) {
        for (Product product : productList) {
            placeOrder(user, product, price);
        }
    }

    public int redeemCoupon(String uid, String couponId) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(REDEEMED_STATE, "1");
            return db.update(COUPONS_TABLE, contentValues, UID + " =? AND " + COUPON_ID + " =?", new String[]{uid, couponId});
        } catch (SQLiteException ex) {
            DatabaseErrorHandler.handleError("REDEEM_COUPON", ex.getMessage());
        }
        return 0;
    }

    public int cancelOrder(String uid, String orderId) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            return db.delete(ORDER_TABLE, UID + " =? AND " + ORDER_ID + " =?", new String[]{uid, orderId});
        } catch (SQLiteException ex) {
            DatabaseErrorHandler.handleError("CANCEL_ORDER", ex.getMessage());
        }
        return 0;
    }

    public List<Order> getMyOrderList(String uid) {
        SQLiteDatabase db = this.getReadableDatabase();
        List<Order> list = new ArrayList<>();
        try {
            Cursor cursor = db.query(ORDER_TABLE, null, UID + " =?", new String[]{uid}, null, null, ORDER_APPROVE_TIME + " DESC", null);
            if (cursor.moveToFirst()) {
                do {
                    Order order = new Order();
                    order.setProduct(getProductInfo(cursor.getString(cursor.getColumnIndex(PID))));
                    order.setOrderId(cursor.getString(cursor.getColumnIndex(ORDER_ID)));
                    order.setPid(cursor.getString(cursor.getColumnIndex(PID)));
                    order.setAddressId(cursor.getString(cursor.getColumnIndex(ADDRESS_ID)));
                    order.setDeliveryStatus(cursor.getString(cursor.getColumnIndex(DELIVED)));
                    order.setQty(cursor.getString(cursor.getColumnIndex(QTY)));
                    order.setCouponId(cursor.getString(cursor.getColumnIndex(COUPON_ID)));
                    order.setDeliveryTime(cursor.getString(cursor.getColumnIndex(DELIVERED_TIME)));
                    order.setOrderedTime(cursor.getString(cursor.getColumnIndex(ORDER_APPROVE_TIME)));
                    order.setOrderedStatus(cursor.getString(cursor.getColumnIndex(ORDER_APPROVE)));
                    order.setPackedStatus(cursor.getString(cursor.getColumnIndex(PACKED)));
                    order.setPackedTime(cursor.getString(cursor.getColumnIndex(PACKED_TIME)));
                    order.setExpectedTime(Long.parseLong(cursor.getString(cursor.getColumnIndex(EXPECTED_DELIVERY_TIME))));
                    order.setCouponDisCount(getCoupon(uid, cursor.getString(cursor.getColumnIndex(PID))).getDiscount());
                    list.add(order);
                } while (cursor.moveToNext());
                cursor.close();
            }
        } catch (SQLiteException ex) {
            DatabaseErrorHandler.handleError("GET_MY_ORDER", ex.getMessage());
        }
        return list;
    }

    public long addReview(String uid, String name, String pid, String rating, String content, List<String> paths) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues contentValues = new ContentValues();
            if (Utils.hasIndex(0, paths)) {
                contentValues.put(IMG_ONE, paths.get(0));
            }
            if (Utils.hasIndex(1, paths)) {
                contentValues.put(IMG_TWO, paths.get(1));
            }
            if (Utils.hasIndex(2, paths)) {
                contentValues.put(IMG_THREE, paths.get(2));
            }
            contentValues.put(UID, uid);
            contentValues.put(PID, pid);
            contentValues.put(NAME, name);
            contentValues.put(REVIEW_CONTENT, content);
            contentValues.put(RATING, rating);
            return db.insertOrThrow(REVIEW_TABLE, null, contentValues);
        } catch (SQLiteException ex) {
            DatabaseErrorHandler.handleError("ADD_REVIEW", ex.getMessage());
        }
        return 0;
    }

    public Coupon getCoupon(String uid, String couponId) {
        SQLiteDatabase db = this.getReadableDatabase();
        Coupon coupon = new Coupon();
        try {
            Cursor cursor = db.query(COUPONS_TABLE, null, UID + " =? AND " + COUPON_ID + " =?", new String[]{uid, couponId}, null, null, null, null);
            if (cursor.moveToFirst()) {
                coupon.setDiscount(cursor.getInt(cursor.getColumnIndex(DISCOUNT)));
            }
            cursor.close();
        } catch (SQLiteException ex) {
            DatabaseErrorHandler.handleError("GET_COUPON", ex.getMessage());
        }
        return coupon;
    }

    public void closeConnection() {
        this.getWritableDatabase().close();
    }


    private String makePlaceholders(int len) {
        if (len < 1) {
            // It will lead to an invalid query anyway ..
            throw new RuntimeException("No placeholders");
        } else {
            StringBuilder sb = new StringBuilder(len * 2 - 1);
            sb.append("?");
            for (int i = 1; i < len; i++) {
                sb.append(",?");
            }
            return sb.toString();
        }
    }


}
