package com.example.e_commerce.model;

import android.app.SearchManager;
import android.content.SearchRecentSuggestionsProvider;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.provider.BaseColumns;
import android.text.TextUtils;

import com.example.e_commerce.R;

import static com.example.e_commerce.model.DBConstant.CID;
import static com.example.e_commerce.model.DBConstant.NAME;
import static com.example.e_commerce.model.DBConstant.PID;

public class CustomSuggestionProvider extends SearchRecentSuggestionsProvider {

    public static final String authority = "com.example.e_commerce.model.CustomSuggestionProvider.provider";
    public final static int MODE = DATABASE_MODE_QUERIES | DATABASE_MODE_2LINES;
    public DataBaseHelper dataBaseHelper;

    public CustomSuggestionProvider() {
        setupSuggestions(authority, MODE);
    }

    @Override
    public boolean onCreate() {
        dataBaseHelper = new DataBaseHelper(getContext());
        return super.onCreate();
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        MatrixCursor matrixCursor = new MatrixCursor(new String[]{BaseColumns._ID, SearchManager.SUGGEST_COLUMN_TEXT_1, SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID, SearchManager.SUGGEST_COLUMN_ICON_1});
        if (!TextUtils.isEmpty(selectionArgs[0])) {
            Object[] objects = new Object[4];
            Cursor cursor = dataBaseHelper.getMatchedData(selectionArgs[0]);
            if (cursor.moveToFirst()) {
                do {
                    matrixCursor.newRow()
                            .add(BaseColumns._ID, cursor.getInt(cursor.getColumnIndex("rowid")))
                            .add(SearchManager.SUGGEST_COLUMN_TEXT_1, cursor.getString(cursor.getColumnIndex(NAME)))
                            .add(SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID, cursor.getString(cursor.getColumnIndex(NAME)))
                            .add(SearchManager.SUGGEST_COLUMN_ICON_1, R.drawable.ic_search)
                            .add(SearchManager.FLAG_QUERY_REFINEMENT);
                } while (cursor.moveToNext());
            }
            cursor.close();
            return matrixCursor;

        }
        return super.query(uri, projection, selection, selectionArgs, sortOrder);
    }
}
