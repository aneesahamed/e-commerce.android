package com.example.e_commerce.model.entity;

public class GeneralCategory {
    private String gcId;
    private String name;
    private String url;

    public void setName(String name) {
        this.name = name;
    }

    public void setGcId(String gcId) {
        this.gcId = gcId;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public String getGcId() {
        return gcId;
    }

    public String getUrl() {
        return url;
    }
}
