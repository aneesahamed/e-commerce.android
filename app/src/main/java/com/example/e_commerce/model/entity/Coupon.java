package com.example.e_commerce.model.entity;

public class Coupon {
    private String couponId;
    private String couponTitle;
    private String couponDesc;
    private int discount;
    private String url;
    private String status;

    public void setCouponDesc(String couponDesc) {
        this.couponDesc = couponDesc;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public void setCouponTitle(String couponTitle) {
        this.couponTitle = couponTitle;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getDiscount() {
        return discount;
    }

    public String getCouponDesc() {
        return couponDesc;
    }

    public String getCouponId() {
        return couponId;
    }

    public String getCouponTitle() {
        return couponTitle;
    }

    public String getStatus() {
        return status;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
