package com.example.e_commerce.model.entity;

public class TrendsDeal {
    private String cId;
    private String pId;
    private String name;
    private String url;
    private String offerProduct;
    private int disCount;

    public void setDisCount(int disCount) {
        this.disCount = disCount;
    }

    public int getDisCount() {
        return disCount;
    }

    public void setOfferProduct(String offerProduct) {
        this.offerProduct = offerProduct;
    }

    public String getOfferProduct() {
        return offerProduct;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setcId(String cId) {
        this.cId = cId;
    }

    public void setpId(String pId) {
        this.pId = pId;
    }

    public String getName() {
        return name;
    }

    public String getcId() {
        return cId;
    }

    public String getpId() {
        return pId;
    }
}
