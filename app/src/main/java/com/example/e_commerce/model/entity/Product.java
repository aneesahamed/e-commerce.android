package com.example.e_commerce.model.entity;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class Product implements Parcelable {
    private String pId;
    private String gcId;
    private String cId;
    private String name;
    private int amount;
    private int disCount;
    private int reviewCount;
    private double rating;
    private String stockCount;
    private String productDesc;
    private String warrenty;
    private String productBrand;
    private String ram;
    private String color;
    private String memory;
    private String url;
    private String hertz;
    private String weight;
    private String modelNumber;
    private String modelName;
    private String simType;
    private int colorCount;
    private int ramCount;
    private int storageCount;
    private int hertzCount;
    private int qty = 1;
    private List<String> colorList;
    private List<String> ramList;
    private List<String> memList;
    private List<String> hertzList;
    private List<String> imgList;
    private List<Review> reviewList;
    private List<Specification> specificationList;
    private Price price;
    private int min;
    private int max;
    private long deliveryInfo;

    public void setMax(int max) {
        this.max = max;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public int getMin() {
        return min;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public Price getPrice() {
        return price;
    }

    public static final Parcelable.Creator<Product> CREATOR = new Parcelable.Creator<Product>() {
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        public Product[] newArray(int size) {
            return new Product[size];
        }
    };


    public Product() {

    }

    public Product(Parcel in) {
        this.pId = in.readString();
        this.gcId = in.readString();
        this.cId = in.readString();
        this.name = in.readString();
        this.qty = in.readInt();
        this.amount = in.readInt();
        this.disCount = in.readInt();
        this.reviewCount = in.readInt();
        this.rating = in.readDouble();
        this.stockCount = in.readString();
        this.productDesc = in.readString();
        this.warrenty = in.readString();
        this.productBrand = in.readString();
        this.ram = in.readString();
        this.color = in.readString();
        this.memory = in.readString();
        this.url = in.readString();
        this.hertz = in.readString();
        this.weight = in.readString();
        this.modelName = in.readString();
        this.modelNumber = in.readString();
        this.simType = in.readString();
        this.colorCount = in.readInt();
        this.ramCount = in.readInt();
        this.storageCount = in.readInt();
        this.hertzCount = in.readInt();
        in.readStringList(colorList);
        in.readStringList(ramList);
        in.readStringList(memList);
        in.readStringList(hertzList);
        in.readStringList(imgList);
        this.reviewList = in.readArrayList(Review.class.getClassLoader());
        this.specificationList = in.readArrayList(Specification.class.getClassLoader());
        this.price =  in.readParcelable(Price.class.getClassLoader());
        this.min = in.readInt();
        this.max = in.readInt();
        this.deliveryInfo = in.readLong();

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.pId);
        dest.writeString(this.gcId);
        dest.writeString(this.cId);
        dest.writeString(this.name);
        dest.writeInt(this.amount);
        dest.writeInt(this.qty);
        dest.writeInt(this.disCount);
        dest.writeInt(this.reviewCount);
        dest.writeDouble(this.rating);
        dest.writeString(this.stockCount);
        dest.writeString(this.productDesc);
        dest.writeString(this.warrenty);
        dest.writeString(this.productBrand);
        dest.writeString(this.ram);
        dest.writeString(this.color);
        dest.writeString(this.memory);
        dest.writeString(this.url);
        dest.writeString(this.hertz);
        dest.writeString(this.weight);
        dest.writeString(this.modelName);
        dest.writeString(this.modelNumber);
        dest.writeString(this.simType);
        dest.writeInt(this.colorCount);
        dest.writeInt(this.storageCount);
        dest.writeInt(this.ramCount);
        dest.writeInt(this.hertzCount);
        dest.writeStringList(this.colorList);
        dest.writeStringList(this.ramList);
        dest.writeStringList(this.memList);
        dest.writeStringList(this.hertzList);
        dest.writeStringList(this.imgList);
        dest.writeList(this.reviewList);
        dest.writeList(this.specificationList);
        dest.writeParcelable(this.price, flags);
        dest.writeInt(this.min);
        dest.writeInt(this.max);
        dest.writeLong(this.deliveryInfo);

    }

    public void setDeliveryInfo(long deliveryInfo) {
        this.deliveryInfo = deliveryInfo;
    }

    public long getDeliveryInfo() {
        return deliveryInfo;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getQty() {
        return qty;
    }

    public void setColorList(List<String> colorList) {
        this.colorList = colorList;
    }

    public void setHertzList(List<String> hertzList) {
        this.hertzList = hertzList;
    }

    public void setMemList(List<String> memList) {
        this.memList = memList;
    }

    public void setRamList(List<String> ramList) {
        this.ramList = ramList;
    }

    public List<String> getColorList() {
        return colorList;
    }

    public List<String> getHertzList() {
        return hertzList;
    }

    public List<String> getMemList() {
        return memList;
    }

    public List<String> getRamList() {
        return ramList;
    }

    public void setColorCount(int colorCount) {
        this.colorCount = colorCount;
    }

    public void setHertzCount(int hertzCount) {
        this.hertzCount = hertzCount;
    }

    public void setRamCount(int ramCount) {
        this.ramCount = ramCount;
    }

    public void setStorageCount(int storageCount) {
        this.storageCount = storageCount;
    }

    public int getColorCount() {
        return colorCount;
    }

    public int getHertzCount() {
        return hertzCount;
    }

    public int getRamCount() {
        return ramCount;
    }

    public int getStorageCount() {
        return storageCount;
    }

    public void setReviewCount(int reviewCount) {
        this.reviewCount = reviewCount;
    }

    public int getReviewCount() {
        return reviewCount;
    }

    public void setSpecificationList(List<Specification> specificationList) {
        this.specificationList = specificationList;
    }

    public List<Specification> getSpecificationList() {
        return specificationList;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public double getRating() {
        return rating;
    }

    public void setReviewList(List<Review> reviewList) {
        this.reviewList = reviewList;
    }

    public List<Review> getReviewList() {
        return reviewList;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<String> getImgList() {
        return imgList;
    }

    public void setUrl(List<String> url) {
        this.imgList = url;
    }

    public String getUrl() {
        return url;
    }

    public void setpId(String pId) {
        this.pId = pId;
    }

    public String getpId() {
        return pId;
    }

    public void setcId(String cId) {
        this.cId = cId;
    }

    public String getcId() {
        return cId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGcId(String gcId) {
        this.gcId = gcId;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setDisCount(int disCount) {
        this.disCount = disCount;
    }

    public void setHertz(String hertz) {
        this.hertz = hertz;
    }

    public void setMemory(String memory) {
        this.memory = memory;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public void setModelNumber(String modelNumber) {
        this.modelNumber = modelNumber;
    }

    public void setProductBrand(String productBrand) {
        this.productBrand = productBrand;
    }

    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc;
    }

    public void setRam(String ram) {
        this.ram = ram;
    }

    public void setSimType(String simType) {
        this.simType = simType;
    }

    public void setStockCount(String stockCount) {
        this.stockCount = stockCount;
    }

    public void setWarrenty(String warrenty) {
        this.warrenty = warrenty;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public String getGcId() {
        return gcId;
    }

    public int getAmount() {
        return amount;
    }

    public int getDisCount() {
        return disCount;
    }

    public String getColor() {
        return color;
    }

    public String getHertz() {
        return hertz;
    }

    public String getMemory() {
        return memory;
    }

    public String getProductBrand() {
        return productBrand;
    }

    public String getModelName() {
        return modelName;
    }

    public String getModelNumber() {
        return modelNumber;
    }

    public String getProductDesc() {
        return productDesc;
    }

    public String getRam() {
        return ram;
    }

    public String getSimType() {
        return simType;
    }

    public String getStockCount() {
        return stockCount;
    }

    public String getWarrenty() {
        return warrenty;
    }

    public String getWeight() {
        return weight;
    }
}
