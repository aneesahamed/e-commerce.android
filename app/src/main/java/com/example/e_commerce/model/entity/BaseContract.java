package com.example.e_commerce.model.entity;

import android.net.Uri;

import com.example.e_commerce.BuildConfig;

public class BaseContract {
    public static final String CONTENT_AUTHORITY = BuildConfig.APPLICATION_ID+".provider";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
}
