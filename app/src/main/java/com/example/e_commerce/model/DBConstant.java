package com.example.e_commerce.model;

import java.util.Random;

public class DBConstant {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "eCommerce.db";
    public static final int OFFSET = 10;
    public static final String TAG = "db_tag";
    //Table names
    public static final String ROW_ID = "rowid";
    public static final String USER_TABLE = "user";
    public static final String GENERAL_CATEGORY_TABLE = "generalCategory";
    public static final String CATEGORY_TABLE = "categoryTable";
    public static final String PRODUCT_TABLE = "productTable";
    public static final String CART_TABLE = "cartTable";
    public static final String WISH_LIST_TABLE ="wishListTable";
    public static final String ADDRESS_TABLE = "addressTable";
    public static final String COUPONS_TABLE = "couponsTable";
    public static final String REVIEW_TABLE = "reviewTable";
    public static final String SEARCH_HISTORY = "searchHistory";
    public static final String PRODUCT_IMAGE_TABLE = "productImageTable";
    public static final String ORDER_TABLE = "orderTable";
    public static final String OFFER_TREND = "offerTrend";
    public static final String SUGGESTION_TABLE = "suggestionTable";

    //Table Columns
    public static final String ID = "id";
    public static final String UID = "uid";
    public static final String NAME = "name";
    public static final String EMAIL = "email";
    public static final String MOBILE_NO = "mobileNo";
    public static final String PROFILE = "profile";
    public static final String GCID = "gcid";
    public static final String URL = "url";
    public static final String CID = "cid";
    public static final String PID = "pid";
    public static final String AMOUNT = "amount";
    public static final String DISCOUNT = "discount";
    public static final String STOCK_COUNT = "stockCount";
    public static final String DESC = "desc";
    public static final String PRODUCT_DESC = "productDesc";
    public static final String WARRENTY = "warrenty";
    public static final String PRODUCT_BRAND = "productBrand";
    public static final String RAM = "ram";
    public static final String COLOR = "color";
    public static final String MEM = "mem";
    public static final String HERTZ = "hertz";
    public static final String WEIGHT = "weight";
    public static final String OFFER_PRODUCT = "offerProduct";
    public static final String MODEL_NUMBER ="modelNumber";
    public static final String MODEL_NAME = "modelName";
    public static final String SIM_TYPE = "simType";
    public static final String QTY = "qty";
    public static final String ADDRESS_ID = "addressId";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String DOOR_NO = "doorNo";
    public static final String AREA = "area";
    public static final String CITY = "city";
    public static final String STATE = "state";
    public static final String PINCODE = "pincode";
    public static final String ALT_MOBILE_NO = "altMobileNo";
    public static final String ADDRESS_TYPE = "addressType";
    public static final String COUPON_ID = "couponId";
    public static final String COUPON_DESC = "couponDesc";
    public static final String COUPON_TITLE = "couponTitle";
    public static final String REDEEMED_STATE = "redeemedState";
    public static final String ORDER_ID = "orderId";
    public static final String DELIVERY_FEE = "deliveryFee";
    public static final String FINAL_AMOUNT = "finalAmount";
    public static final String PAYMENT_MODE = "paymentMode";
    public static final String ORDER_APPROVE = "orderApprove";
    public static final String ORDER_APPROVE_TIME = "orderApproveTime";
    public static final String PACKED = "packed";
    public static final String PACKED_TIME = "packedTime";
    public static final String SHIPPED = "shipped";
    public static final String SHIPPED_TIME = "shippedTime";
    public static final String DELIVED = "delivered";
    public static final String DELIVERED_TIME = "deliveredTime";
    public static final String REVIEW_CONTENT = "reviewContent";
    public static final String RATING = "rating";
    public static final String IMG_ONE = "imgOne";
    public static final String IMG_TWO = "imgTwo";
    public static final String IMG_THREE = "imgThree";
    public static final String KEYWORDS = "keywords";
    public static final String OFFER_TYPE = "offerType";
    public static final String EXPECTED_DELIVERY_TIME = "expectedTime";


}
