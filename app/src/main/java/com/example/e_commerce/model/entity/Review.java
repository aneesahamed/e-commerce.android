package com.example.e_commerce.model.entity;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class Review implements Parcelable {

    public Review(){}


    private String uid;
    private String rating;
    private List<String> list;
    private String name;
    private String content;

    public void setRating(String rating) {
        this.rating = rating;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setList(List<String> list) {
        this.list = list;
    }

    public String getName() {
        return name;
    }

    public String getUid() {
        return uid;
    }

    public List<String> getList() {
        return list;
    }

    public String getContent() {
        return content;
    }

    public String getRating() {
        return rating;
    }

    public static final Parcelable.Creator<Review> CREATOR = new Parcelable.Creator<Review>() {
        public Review createFromParcel(Parcel in) {
            return new Review(in);
        }

        public Review[] newArray(int size) {
            return new Review[size];
        }
    };


    public Review(Parcel in) {
        this.uid = in.readString();
        this.rating = in.readString();
        in.readStringList(this.list);
        this.name = in.readString();
        this.content = in.readString();

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.uid);
        dest.writeString(this.rating);
        dest.writeStringList(this.list);
        dest.writeString(this.name);
        dest.writeString(this.content);

    }
}
