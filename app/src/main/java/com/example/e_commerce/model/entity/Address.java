package com.example.e_commerce.model.entity;

import android.os.Parcel;
import android.os.Parcelable;

public class Address implements Parcelable {
    private String addressId;
    private String firstName;
    private String lastName;
    private String doorNo;
    private String area;
    private String city;
    private String state;
    private String pinCode;
    private String altNo;
    private String addressType;
    private boolean isSelected;
    private long deliveryInfo;
    public Address() {

    }

    public static final Parcelable.Creator<Address> CREATOR = new Parcelable.Creator<Address>() {
        public Address createFromParcel(Parcel in) {
            return new Address(in);
        }

        public Address[] newArray(int size) {
            return new Address[size];
        }
    };


    public Address(Parcel in) {
        this.addressId = in.readString();
        this.firstName = in.readString();
        this.lastName = in.readString();
        this.doorNo = in.readString();
        this.area = in.readString();
        this.city = in.readString();
        this.state = in.readString();
        this.pinCode = in.readString();
        this.altNo = in.readString();
        this.addressType = in.readString();
        this.isSelected =in.readByte() != 0;
        this.deliveryInfo = in.readLong();

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(this.addressId);
        dest.writeString(this.firstName);
        dest.writeString(this.lastName);
        dest.writeString(this.doorNo);
        dest.writeString(this.area);
        dest.writeString(this.city);
        dest.writeString(this.state);
        dest.writeString(this.pinCode);
        dest.writeString(this.altNo);
        dest.writeString(this.addressType);
        dest.writeByte((byte) (this.isSelected ? 1 : 0));
        dest.writeLong(this.deliveryInfo);

    }

    public void setDeliveryInfo(long deliveryInfo) {
        this.deliveryInfo = deliveryInfo;
    }

    public long getDeliveryInfo() {
        return deliveryInfo;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    public void setAddressType(String addressType) {
        this.addressType = addressType;
    }

    public void setAltNo(String altNo) {
        this.altNo = altNo;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setDoorNo(String doorNo) {
        this.doorNo = doorNo;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getAddressId() {
        return addressId;
    }

    public String getAddressType() {
        return addressType;
    }

    public String getAltNo() {
        return altNo;
    }

    public String getArea() {
        return area;
    }

    public String getCity() {
        return city;
    }

    public String getDoorNo() {
        return doorNo;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPinCode() {
        return pinCode;
    }

    public String getState() {
        return state;
    }
}
