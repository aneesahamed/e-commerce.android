package com.example.e_commerce.model.entity;

import android.os.Parcel;
import android.os.Parcelable;

public class Category implements Parcelable {
    private String gcId;
    private String url;
    private String name;
    private String cId;
    private boolean isChecked;

    public void setName(String name) {
        this.name = name;
    }

    public void setGcId(String pId) {
        this.gcId = pId;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public String getGcId() {
        return gcId;
    }

    public String getUrl() {
        return url;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setcId(String cId) {
        this.cId = cId;
    }

    public String getcId() {
        return cId;
    }

    public static final Parcelable.Creator<Category> CREATOR = new Parcelable.Creator<Category>() {
        public Category createFromParcel(Parcel in) {
            return new Category(in);
        }

        public Category[] newArray(int size) {
            return new Category[size];
        }
    };

    public Category() {
    }

    public Category(Parcel in) {
        this.gcId = in.readString();
        this.url = in.readString();
        this.name = in.readString();
        this.cId = in.readString();
        this.isChecked = in.readByte() != 0;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.gcId);
        dest.writeString(this.url);
        dest.writeString(this.name);
        dest.writeString(this.cId);
        dest.writeByte((byte) (this.isChecked ? 1 : 0));
    }
}
