package com.example.e_commerce.model.entity;

import android.os.Parcel;
import android.os.Parcelable;

public class Price implements Parcelable {
    private int itemCount;
    private String price;
    private String delivery;
    private String pay;
    private int total;
    private String payMode;
    private String couponId;


    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }

    public String getPayMode() {
        return payMode;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getTotal() {
        return total;
    }

    public void setDelivery(String delivery) {
        this.delivery = delivery;
    }

    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }

    public void setPay(String pay) {
        this.pay = pay;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getItemCount() {
        return itemCount;
    }

    public String getDelivery() {
        return delivery;
    }

    public String getPay() {
        return pay;
    }

    public String getPrice() {
        return price;
    }

    public static final Parcelable.Creator<Price> CREATOR = new Parcelable.Creator<Price>() {
        public Price createFromParcel(Parcel in) {
            return new Price(in);
        }

        public Price[] newArray(int size) {
            return new Price[size];
        }
    };

    public Price() {

    }

    public Price(Parcel in) {
        this.itemCount = in.readInt();
        this.price = in.readString();
        this.delivery = in.readString();
        this.pay = in.readString();
        this.total = in.readInt();
        this.payMode = in.readString();
        this.couponId = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.itemCount);
        dest.writeString(this.price);
        dest.writeString(this.delivery);
        dest.writeString(this.pay);
        dest.writeInt(this.total);
        dest.writeString(this.payMode);
        dest.writeString(this.couponId);
    }
}
