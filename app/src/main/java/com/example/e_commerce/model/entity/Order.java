package com.example.e_commerce.model.entity;

import android.os.Parcel;
import android.os.Parcelable;

public class Order implements Parcelable{
    private String pid;
    private String orderId;
    private String deliveryTime;
    private Product product;
    private String qty;
    private String couponId;
    private String deliveryStatus;
    private String orderedTime;
    private String orderedStatus;
    private String packedTime;
    private String packedStatus;
    private Address address;
    private int couponDisCount;
    private String addressId;
    private long expectedTime;

    public Order(){}
    public static final Parcelable.Creator<Order> CREATOR = new Parcelable.Creator<Order>() {
        public Order createFromParcel(Parcel in) {
            return new Order(in);
        }

        public Order[] newArray(int size) {
            return new Order[size];
        }
    };


    public Order(Parcel in) {
        this.pid = in.readString();
        this.orderId = in.readString();
        this.deliveryTime = in.readString();
        this.product = in.readParcelable(Product.class.getClassLoader());
        this.qty = in.readString();
        this.couponId = in.readString();
        this.deliveryStatus = in.readString();
        this.orderedTime = in.readString();
        this.orderedStatus = in.readString();
        this.packedTime = in.readString();
        this.packedStatus =in.readString();
        this.address = in.readParcelable(Address.class.getClassLoader());
        this.couponDisCount =in.readInt();
        this.addressId = in.readString();
        this.expectedTime = in.readLong();


    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(this.pid);
        dest.writeString(this.orderId);
        dest.writeString(this.deliveryTime);
        dest.writeParcelable(this.product, flags);
        dest.writeString(this.qty);
        dest.writeString(this.couponId);
        dest.writeString(this.deliveryStatus);
        dest.writeString(this.orderedTime);
        dest.writeString(this.orderedStatus);
        dest.writeString(this.packedTime);
        dest.writeString(this.packedStatus);
        dest.writeParcelable(this.address, flags);
        dest.writeInt(this.couponDisCount);
        dest.writeString(this.addressId);
        dest.writeLong(this.expectedTime);
    }


    public void setExpectedTime(long expectedTime) {
        this.expectedTime = expectedTime;
    }

    public long getExpectedTime() {
        return expectedTime;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    public String getAddressId() {
        return addressId;
    }

    public void setCouponDisCount(int couponDisCount) {
        this.couponDisCount = couponDisCount;
    }

    public int getCouponDisCount() {
        return couponDisCount;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Address getAddress() {
        return address;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public void setOrderedStatus(String orderedStatus) {
        this.orderedStatus = orderedStatus;
    }

    public void setOrderedTime(String orderedTime) {
        this.orderedTime = orderedTime;
    }

    public void setPackedStatus(String packedStatus) {
        this.packedStatus = packedStatus;
    }

    public void setPackedTime(String packedTime) {
        this.packedTime = packedTime;
    }

    public String getCouponId() {
        return couponId;
    }

    public String getOrderedStatus() {
        return orderedStatus;
    }

    public String getOrderedTime() {
        return orderedTime;
    }

    public String getPackedStatus() {
        return packedStatus;
    }

    public String getPackedTime() {
        return packedTime;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getQty() {
        return qty;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public void setDeliveryTime(String deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public Product getProduct() {
        return product;
    }

    public String getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryStatus(String deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getDeliveryStatus() {
        return deliveryStatus;
    }

    public String getOrderId() {
        return orderId;
    }

    public String getPid() {
        return pid;
    }
}
