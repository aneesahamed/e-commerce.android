package com.example.e_commerce.model.entity;

import java.util.List;

public class HomeFeed {
    private List<TrendsDeal> trendsDealList;
    private List<GeneralCategory> generalCategoryList;
    private List<Product> productList;
    public void setTrendsDealList(List<TrendsDeal> trendsDealList) {
        this.trendsDealList = trendsDealList;
    }

    public List<TrendsDeal> getTrendsDealList() {
        return trendsDealList;
    }

    public void setGeneralCategoryList(List<GeneralCategory> generalCategoryList) {
        this.generalCategoryList = generalCategoryList;
    }

    public List<GeneralCategory> getGeneralCategoryList() {
        return generalCategoryList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    public List<Product> getProductList() {
        return productList;
    }
}
