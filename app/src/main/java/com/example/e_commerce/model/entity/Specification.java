package com.example.e_commerce.model.entity;

import android.os.Parcel;
import android.os.Parcelable;

public class Specification implements Parcelable {


    private String title;
    private String value;

    public Specification(String title ,String value){
        this.title = title;
        this.value = value;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getTitle() {
        return title;
    }

    public String getValue() {
        return value;
    }

    public static final Parcelable.Creator<Specification> CREATOR = new Parcelable.Creator<Specification>() {
        public Specification createFromParcel(Parcel in) {
            return new Specification(in);
        }

        public Specification[] newArray(int size) {
            return new Specification[size];
        }
    };


    public Specification(Parcel in) {
        this.title = in.readString();
        this.value = in.readString();

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.value);

    }
}
