package com.example.e_commerce.model.entity;

import android.content.ContentUris;
import android.net.Uri;


import static com.example.e_commerce.model.DBConstant.USER_TABLE;

public class UserContract extends BaseContract {

    public static class UserEntry{
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(USER_TABLE).build();

        public static Uri getAddressUri(long id){
            return ContentUris.withAppendedId(CONTENT_URI ,id);
        }
    }
}
