package com.example.e_commerce.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.e_commerce.R;
import com.example.e_commerce.model.entity.Specification;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SpecificationAdapter extends RecyclerView.Adapter<SpecificationAdapter.SpecificationViewHolder> {

    private List<Specification> list;

    public SpecificationAdapter(List<Specification> list) {
        this.list = list;
    }

    public static class SpecificationViewHolder extends RecyclerView.ViewHolder {
        TextView txtName, txtValue;

        SpecificationViewHolder(View view) {
            super(view);
            txtName = view.findViewById(R.id.txtTitle);
            txtValue = view.findViewById(R.id.txtValue);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull SpecificationViewHolder holder, int position) {
        Specification specification = list.get(position);
        holder.txtName.setText(specification.getTitle());
        holder.txtValue.setText(specification.getValue());
    }

    @NonNull
    @Override
    public SpecificationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_spec, parent, false);
        return new SpecificationViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
