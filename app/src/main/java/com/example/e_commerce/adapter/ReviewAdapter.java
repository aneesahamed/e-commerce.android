package com.example.e_commerce.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.e_commerce.R;
import com.example.e_commerce.common.Utils;
import com.example.e_commerce.model.entity.Review;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.ReviewHolder> {
    private List<Review> reviewList;


    public ReviewAdapter(Context context ,List<Review> list) {
        this.reviewList = list;
    }

    public static class ReviewHolder extends RecyclerView.ViewHolder {
        TextView txtRating ,txtName,txtContent;
        ImageView imgOne,imgTwo,imgThree;
        LinearLayout imgRow;
        public ReviewHolder(View view) {
            super(view);
            txtRating = view.findViewById(R.id.txtRating);
            txtName = view.findViewById(R.id.txtName);
            txtContent = view.findViewById(R.id.txtContent);
            imgOne = view.findViewById(R.id.imgOne);
            imgTwo = view.findViewById(R.id.imgTwo);
            imgThree = view.findViewById(R.id.imgThree);
            imgRow = view.findViewById(R.id.imgRow);
        }
    }

    @NonNull
    @Override
    public ReviewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_review, parent, false);
        return new ReviewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ReviewHolder holder, int position) {
        Review review = reviewList.get(position);
        List<String> list = review.getList();
        holder.txtName.setText(review.getName());
        holder.txtRating.setText(review.getRating());
        holder.txtContent.setVisibility(TextUtils.isEmpty(review.getContent())?View.GONE:View.VISIBLE);
        holder.txtContent.setText(review.getContent());
        holder.imgRow.setVisibility(list.size()>0?View.VISIBLE:View.GONE);
        if(Utils.hasIndex(0,list)){
            holder.imgOne.setImageBitmap(Utils.decodeSampledBitmap(list.get(0),100,80));
        }
        if(Utils.hasIndex(1,list)){
            holder.imgTwo.setImageBitmap(Utils.decodeSampledBitmap(list.get(1),100,80));
        }
        if(Utils.hasIndex(2,list)){
            holder.imgThree.setImageBitmap(Utils.decodeSampledBitmap(list.get(2),100,80));
        }
    }

    @Override
    public int getItemCount() {
        return reviewList.size();
    }
}
