package com.example.e_commerce.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.e_commerce.R;
import com.example.e_commerce.common.AppController;
import com.example.e_commerce.common.PreferenceManager;
import com.example.e_commerce.common.Utils;
import com.example.e_commerce.common.imageUtils.ImageFetcher;
import com.example.e_commerce.model.entity.Product;
import com.example.e_commerce.model.entity.User;
import com.example.e_commerce.ui.common.ClickListener;
import com.google.android.material.card.MaterialCardView;

import java.util.Arrays;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class CartListAdapter extends RecyclerView.Adapter<CartListAdapter.CartViewHolder> {

    private Context context;
    private ImageFetcher imageFetcher;
    private List<Product> list;
    private String[] qtyList;
    private User user;
    private final Object object = new Object();
    private CartListener listener;
    public interface CartListener{
        void onClick(View view ,int position ,Object object);
    }
    public CartListAdapter(Context context, ImageFetcher imageFetcher, List<Product> list, CartListener listener) {
        this.context = context;
        this.imageFetcher = imageFetcher;
        this.list = list;
        this.qtyList = qtyList = context.getResources().getStringArray(R.array.qty);
        this.listener = listener;
        this.user = PreferenceManager.getUserInfo(context);
    }

    public static class CartViewHolder extends RecyclerView.ViewHolder {
        private TextView productName, productColor, txtRating, txtPrice, txtOrgPrice, txtOff, txtWarrenty, txtQty;
        Spinner qtySpinner;
        ImageView imgProduct;
        FrameLayout actionBuy, actionRemove;
        MaterialCardView actionQty;

        CartViewHolder(View view) {
            super(view);
            productName = view.findViewById(R.id.productName);
            productColor = view.findViewById(R.id.productColor);
            imgProduct = view.findViewById(R.id.imgProduct);
            txtRating = view.findViewById(R.id.txtRating);
            txtPrice = view.findViewById(R.id.txtProductPrice);
            txtOrgPrice = view.findViewById(R.id.txtProductPriceOrg);
            txtOff = view.findViewById(R.id.txtOff);
            txtWarrenty = view.findViewById(R.id.txtWarrenty);
            qtySpinner = view.findViewById(R.id.qtySpinner);
            txtQty = view.findViewById(R.id.txtQty);
            actionQty = view.findViewById(R.id.actionQty);
            actionBuy = view.findViewById(R.id.actionBuy);
            actionRemove = view.findViewById(R.id.actionRemove);
        }
    }

    @NonNull
    @Override
    public CartViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_cart, parent, false);

        return new CartViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CartViewHolder holder, final int position) {
        final Product product = list.get(position);
        imageFetcher.loadImage(product.getImgList().get(0), holder.imgProduct);
        holder.productName.setText(product.getName());
        if (!TextUtils.isEmpty(product.getColor())) {
            holder.productColor.setText(String.format(context.getString(R.string.prodColor), product.getColor()));
        }
        holder.txtPrice.setText(String.format(context.getString(R.string.currency), String.valueOf(Utils.getDiscountValue(product.getAmount(), product.getDisCount()))));
        holder.txtOrgPrice.setText(String.valueOf(product.getAmount()));
        holder.txtOrgPrice.setPaintFlags(holder.txtOrgPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.txtOff.setText(String.format(context.getString(R.string.offText), String.valueOf(product.getDisCount())));
        holder.txtWarrenty.setText(String.format(context.getString(R.string.warrenty), product.getWarrenty()));
        holder.txtQty.setText(String.format(context.getString(R.string.qty), String.valueOf(product.getQty())));

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(context,
                R.array.qty, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        holder.qtySpinner.setAdapter(adapter);
        holder.qtySpinner.setSelection(Arrays.asList(qtyList).indexOf(String.valueOf(product.getQty())));
        holder.qtySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                synchronized (object) {
                    int i = AppController.getInstance().getDataBaseHelper().updateCart(user.getUid(), product.getpId(), qtyList[position]);
                    if (i > 0) {
                        product.setQty(Integer.valueOf(qtyList[position]));
                        holder.txtQty.setText(String.format(context.getString(R.string.qty), qtyList[position]));
                        listener.onClick(holder.actionQty,holder.getAdapterPosition() ,product);
                    }
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        holder.actionQty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.qtySpinner.performClick();
            }
        });

        holder.actionBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener != null){
                    listener.onClick(holder.actionBuy ,holder.getAdapterPosition() ,list.get(holder.getAdapterPosition()));
                }
            }
        });

        holder.actionRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener != null){
                    listener.onClick(holder.actionRemove ,holder.getAdapterPosition() ,list.get(holder.getAdapterPosition()));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
