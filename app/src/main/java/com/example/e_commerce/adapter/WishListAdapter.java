package com.example.e_commerce.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.e_commerce.R;
import com.example.e_commerce.common.Utils;
import com.example.e_commerce.common.imageUtils.ImageFetcher;
import com.example.e_commerce.model.entity.Product;
import com.google.android.material.card.MaterialCardView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class WishListAdapter extends RecyclerView.Adapter<WishListAdapter.WishListViewHolder> {

    private List<Product> list;
    private Context context;
    private ImageFetcher imageFetcher;
    private CartListAdapter.CartListener listener;

    public WishListAdapter(List<Product> list, Context context, ImageFetcher imageFetcher, CartListAdapter.CartListener listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
        this.imageFetcher = imageFetcher;
    }

    public static class WishListViewHolder extends RecyclerView.ViewHolder {
        private TextView productName, productColor, txtRating, txtPrice, txtOrgPrice, txtOff, txtWarrenty, txtQty;
        Spinner qtySpinner;
        ImageView imgProduct;
        FrameLayout actionBuy, actionRemove;
        MaterialCardView actionQty;

        public WishListViewHolder(View view) {
            super(view);
            productName = view.findViewById(R.id.productName);
            productColor = view.findViewById(R.id.productColor);
            imgProduct = view.findViewById(R.id.imgProduct);
            txtRating = view.findViewById(R.id.txtRating);
            txtPrice = view.findViewById(R.id.txtProductPrice);
            txtOrgPrice = view.findViewById(R.id.txtProductPriceOrg);
            txtOff = view.findViewById(R.id.txtOff);
            txtWarrenty = view.findViewById(R.id.txtWarrenty);
            qtySpinner = view.findViewById(R.id.qtySpinner);
            txtQty = view.findViewById(R.id.txtQty);
            actionQty = view.findViewById(R.id.actionQty);
            actionBuy = view.findViewById(R.id.actionBuy);
            actionRemove = view.findViewById(R.id.actionRemove);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final WishListViewHolder holder, int position) {
        holder.actionQty.setVisibility(View.GONE);

        final Product product = list.get(position);
        imageFetcher.loadImage(product.getImgList().get(0), holder.imgProduct);
        holder.productName.setText(product.getName());
        if (!TextUtils.isEmpty(product.getColor())) {
            holder.productColor.setText(String.format(context.getString(R.string.prodColor), product.getColor()));
        }
        holder.txtPrice.setText(String.format(context.getString(R.string.currency), String.valueOf(Utils.getDiscountValue(product.getAmount(), product.getDisCount()))));
        holder.txtOrgPrice.setText(String.valueOf(product.getAmount()));
        holder.txtOrgPrice.setPaintFlags(holder.txtOrgPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.txtOff.setText(String.format(context.getString(R.string.offText), String.valueOf(product.getDisCount())));
        holder.txtWarrenty.setText(String.format(context.getString(R.string.warrenty), product.getWarrenty()));

        holder.actionBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener != null){
                    listener.onClick(holder.actionBuy ,holder.getAdapterPosition() ,list.get(holder.getAdapterPosition()));
                }
            }
        });

        holder.actionRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener != null){
                    listener.onClick(holder.actionRemove ,holder.getAdapterPosition() ,list.get(holder.getAdapterPosition()));
                }
            }
        });
    }

    @NonNull
    @Override
    public WishListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_cart, parent, false);
        return new WishListViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
