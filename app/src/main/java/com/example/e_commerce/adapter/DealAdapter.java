package com.example.e_commerce.adapter;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.e_commerce.R;
import com.example.e_commerce.common.imageUtils.ImageFetcher;
import com.example.e_commerce.model.entity.TrendsDeal;
import com.example.e_commerce.ui.common.ClickListener;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class DealAdapter extends RecyclerView.Adapter<DealAdapter.DealViewHolder> {

    private Context context;
    private List<TrendsDeal> list;
    private ImageFetcher imageFetcher;
    private ClickListener listener;

    public DealAdapter(Context context, ImageFetcher imageFetcher, List<TrendsDeal> trendsDeals, ClickListener listener) {
        this.context = context;
        this.list = trendsDeals;
        this.imageFetcher = imageFetcher;
        this.listener = listener;
    }

    public static class DealViewHolder extends RecyclerView.ViewHolder {
        ImageView lstImage;
        TextView txtTitle, txtDiscount;

        public DealViewHolder(View view) {
            super(view);
            lstImage = view.findViewById(R.id.lstImage);
            txtTitle = view.findViewById(R.id.txtTitle);
            txtDiscount = view.findViewById(R.id.txtDiscount);
        }
    }

    @NonNull
    @Override
    public DealViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_deal, parent, false);
        return new DealViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final DealViewHolder holder, int position) {
        final TrendsDeal trendsDeal = list.get(position);
        holder.txtTitle.setText(trendsDeal.getName());
        imageFetcher.loadImage(trendsDeal.getUrl() ,holder.lstImage);
        holder.txtDiscount.setText(String.format(context.getString((trendsDeal.getOfferProduct().equals("C"))?R.string.upto:R.string.fromOff) ,String.valueOf(trendsDeal.getDisCount())));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener != null){
                    listener.onItemClick(list.get(holder.getAdapterPosition()));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
