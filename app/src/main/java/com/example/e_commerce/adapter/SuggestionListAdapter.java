package com.example.e_commerce.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.e_commerce.R;
import com.example.e_commerce.common.imageUtils.ImageFetcher;
import com.example.e_commerce.model.entity.Category;
import com.example.e_commerce.ui.product.productList.ProductListActivity;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SuggestionListAdapter extends RecyclerView.Adapter<SuggestionListAdapter.SuggestionListViewHolder> {

    private List<Category> categoryList;
    private Context context;
    private ImageFetcher imageFetcher;
    private boolean isFromSuggestion;
    public SuggestionListAdapter(Context context, boolean isFromSuggestion, List<Category> categoryList , ImageFetcher imageFetcher) {
        this.context = context;
        this.categoryList = categoryList;
        this.isFromSuggestion = isFromSuggestion;
        this.imageFetcher = imageFetcher;
    }

    public static class SuggestionListViewHolder extends RecyclerView.ViewHolder {
        ImageView imageview;
        TextView txtCategory;
        CheckBox checkBox;
        SuggestionListViewHolder(View view) {
            super(view);
            imageview = view.findViewById(R.id.lstImage);
            txtCategory = view.findViewById(R.id.txtCategory);
            checkBox = view.findViewById(R.id.checkbox);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final SuggestionListViewHolder holder, int position) {

        final Category category = categoryList.get(position);
        imageFetcher.loadImage(category.getUrl(),holder.imageview);
        imageFetcher.setImageFadeIn(false);
        holder.checkBox.setChecked(category.isChecked());
        holder.txtCategory.setText(category.getName());

        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.checkBox.setChecked(!category.isChecked());
                category.setChecked(holder.checkBox.isChecked());
            }
        });

        holder.checkBox.setVisibility(isFromSuggestion?View.VISIBLE:View.GONE);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isFromSuggestion){
                    Intent intent = new Intent(context , ProductListActivity.class);
                    intent.putExtra("isFromSearch" ,false);
                    intent.putExtra("cid" ,category.getcId());
                    context.startActivity(intent);
                }
            }
        });
    }

    @NonNull
    @Override
    public SuggestionListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_suggestion, parent, false);
        return new SuggestionListViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public void setList(List<Category> list){
        categoryList.clear();
        categoryList.addAll(list);
        notifyDataSetChanged();
    }
}
