package com.example.e_commerce.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.e_commerce.R;
import com.example.e_commerce.common.imageUtils.ImageFetcher;
import com.example.e_commerce.common.imageUtils.RoundedCornerImageView;
import com.example.e_commerce.model.entity.GeneralCategory;
import com.example.e_commerce.ui.common.ClickListener;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class GeneralCategoryAdapter extends RecyclerView.Adapter<GeneralCategoryAdapter.GeneralCategoryViewHolder> {

    private Context context;
    private List<GeneralCategory> generalCategoryList;
    private ImageFetcher imageFetcher;
    private ClickListener listener;

    public GeneralCategoryAdapter(Context context, ImageFetcher imageFetcher, List<GeneralCategory> generalCategoryList, ClickListener listener) {
        this.context = context;
        this.generalCategoryList = generalCategoryList;
        this.imageFetcher = imageFetcher;
        this.listener = listener;
    }

    public static class GeneralCategoryViewHolder extends RecyclerView.ViewHolder {
        TextView txtName;
        RoundedCornerImageView lstImage;

        public GeneralCategoryViewHolder(View view) {
            super(view);
            lstImage = view.findViewById(R.id.lstImage);
            txtName = view.findViewById(R.id.txtGeneralCategory);
        }
    }

    @NonNull
    @Override
    public GeneralCategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_general_category, parent, false);
        return new GeneralCategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final GeneralCategoryViewHolder holder, int position) {
        GeneralCategory generalCategory = generalCategoryList.get(position);
        holder.txtName.setText(generalCategory.getName());
        imageFetcher.loadImage(generalCategory.getUrl(), holder.lstImage);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onItemClick(generalCategoryList.get(holder.getAdapterPosition()));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return generalCategoryList.size();
    }
}
