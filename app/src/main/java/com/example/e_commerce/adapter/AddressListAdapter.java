package com.example.e_commerce.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.example.e_commerce.R;
import com.example.e_commerce.common.PreferenceManager;
import com.example.e_commerce.model.entity.Address;
import com.example.e_commerce.model.entity.User;
import com.example.e_commerce.ui.common.ClickListener;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class AddressListAdapter extends RecyclerView.Adapter<AddressListAdapter.AddressListViewHolder> {

    private List<Address> list;
    private Context context;
    private int pos;
    private User user;
    private ClickListener listener;
    public AddressListAdapter(Context context , List<Address> list , ClickListener listener){
        this.list = list;
        this.context = context;
        this.listener = listener;
        this.user = PreferenceManager.getUserInfo(context);
    }
    public static class AddressListViewHolder extends RecyclerView.ViewHolder{
        RadioButton radioButton;
        FrameLayout actionChange;
        TextView txtName;
        TextView txtAddress;
        AddressListViewHolder(View view){
            super(view);
            radioButton = view.findViewById(R.id.rdAddress);
            actionChange = view.findViewById(R.id.actionChange);
            txtName = view.findViewById(R.id.txtName);
            txtAddress = view.findViewById(R.id.txtAddress);
        }

    }

    @NonNull
    @Override
    public AddressListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_address, parent, false);
        return new AddressListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final AddressListViewHolder holder, final int position) {
        final Address address = list.get(position);
        holder.txtName.setVisibility(View.VISIBLE);
        holder.txtAddress.setVisibility(View.VISIBLE);
        holder.radioButton.setVisibility(View.VISIBLE);
        holder.txtName.setText(String.format(context.getString(R.string.concatinate),address.getFirstName() ,address.getLastName()));
        String fullAddress = String.format(context.getString(R.string.formAddress) ,address.getDoorNo(),address.getArea(),address.getCity(),address.getState(),address.getState(),address.getPinCode(),address.getAltNo());
        holder.txtAddress.setText(fullAddress);

        if(address.isSelected()){
            pos = position;
            holder.radioButton.setChecked(true);
        }
        else {
            holder.radioButton.setChecked(false);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                list.get(pos).setSelected(false);
                list.get(holder.getAdapterPosition()).setSelected(true);
                notifyDataSetChanged();
            }
        });

        holder.actionChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(address);
            }
        });
    }

    public String getSelectedId(){
        return list.get(pos).getAddressId();
    }
    @Override
    public int getItemCount() {
        return list.size();
    }
}
