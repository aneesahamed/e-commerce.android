package com.example.e_commerce.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.e_commerce.R;
import com.example.e_commerce.common.imageUtils.ImageFetcher;

import java.util.List;

import androidx.viewpager.widget.PagerAdapter;

public class CustomProductSlider extends PagerAdapter {

    public Context context;
    Context mContext;
    LayoutInflater mLayoutInflater;
    private List<String> list;
    private ImageFetcher imageFetcher;


    public CustomProductSlider(Context context, List<String> list, ImageFetcher imageFetcher) {
        mContext = context;
        this.list = list;
        this.imageFetcher = imageFetcher;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.list_item_product_slider, container, false);
        ImageView img = itemView.findViewById(R.id.imgItem);
        imageFetcher.loadImage(list.get(position), img);
        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

}
