package com.example.e_commerce.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.e_commerce.R;
import com.example.e_commerce.common.Utils;
import com.example.e_commerce.common.imageUtils.ImageFetcher;
import com.example.e_commerce.model.entity.Product;
import com.example.e_commerce.ui.common.ClickListener;
import com.example.e_commerce.ui.common.OnLoadMoreListener;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class RelatedProductAdapter extends RecyclerView.Adapter<RelatedProductAdapter.RelatedProductViewHolder> {
    private List<Product> productList;
    ImageFetcher imageFetcher;
    Context context;
    private ClickListener listener;
    private OnLoadMoreListener onLoadMoreListener;
    private int totalItemCount = 0, lastVisibleItem = 0;
    private int visibleThreshold = 5;
    public boolean loading;
    public RelatedProductAdapter(Context context, ImageFetcher imageFetcher, List<Product> list ,ClickListener listener ,RecyclerView recyclerView) {
        this.productList = list;
        this.context = context;
        this.imageFetcher = imageFetcher;
        this.listener = listener;
        if (recyclerView.getLayoutManager() instanceof GridLayoutManager) {
            final GridLayoutManager linearLayoutManager = (GridLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView,
                                       int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        // End has been reached
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }
    }

    public static class RelatedProductViewHolder extends RecyclerView.ViewHolder {
        ImageView lstImage;
        TextView txtDiscount, txtProductPrice, txtRating, txtReviewCount, txtProductTitle;

        public RelatedProductViewHolder(View view) {
            super(view);
            lstImage = view.findViewById(R.id.lstImage);
            txtProductTitle = view.findViewById(R.id.txtProductTitle);
            txtRating = view.findViewById(R.id.txtRating);
            txtReviewCount = view.findViewById(R.id.txtReviewCount);
            txtProductPrice = view.findViewById(R.id.txtProductPrice);
            txtDiscount = view.findViewById(R.id.txtDiscount);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RelatedProductViewHolder holder, int position) {
        final Product product = productList.get(position);
        imageFetcher.loadImage(product.getUrl(), holder.lstImage);
        holder.txtProductTitle.setText(product.getName());
        if (product.getReviewCount() > 0) {
            holder.txtRating.setVisibility(View.VISIBLE);
            holder.txtReviewCount.setVisibility(View.VISIBLE);
            holder.txtReviewCount.setText(String.valueOf(product.getReviewCount()));
            holder.txtRating.setText(String.valueOf((int)Math.round(product.getRating())));
        } else {
            holder.txtRating.setVisibility(View.GONE);
            holder.txtReviewCount.setVisibility(View.GONE);
        }
        holder.txtProductPrice.setText(String.format(context.getString(R.string.currency), String.valueOf(Utils.getDiscountValue(product.getAmount(), product.getDisCount()))));
        holder.txtDiscount.setText(String.format(context.getString(R.string.offText), String.valueOf(product.getDisCount())));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener != null){
                    listener.onItemClick(product);
                }
            }
        });

    }

    @NonNull
    @Override
    public RelatedProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_related_product, parent, false);
        return new RelatedProductViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public void setLoaded(){
        loading = false;
    }
    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }
}
