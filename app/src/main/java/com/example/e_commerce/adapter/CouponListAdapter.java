package com.example.e_commerce.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.e_commerce.R;
import com.example.e_commerce.common.imageUtils.ImageFetcher;
import com.example.e_commerce.model.entity.Coupon;
import com.example.e_commerce.ui.common.ClickListener;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class CouponListAdapter extends RecyclerView.Adapter<CouponListAdapter.CouponListViewHolder> {

    private Context context;
    private List<Coupon> list;
    private ClickListener clickListener;
    private ImageFetcher imageFetcher;
    private boolean isFromNav;

    public CouponListAdapter(Context context, boolean isFromNav ,ImageFetcher imageFetcher , List<Coupon> list, ClickListener clickListener) {
        this.context = context;
        this.imageFetcher = imageFetcher;
        this.clickListener = clickListener;
        this.list = list;
        this.isFromNav = isFromNav;
    }

    public static class CouponListViewHolder extends RecyclerView.ViewHolder {
        ImageView lstImage;
        TextView txtCouponTitle,txtCouponDesc ,txtRedeem;
        FrameLayout actionRedeem;
        public CouponListViewHolder(View view) {
            super(view);
            lstImage = view.findViewById(R.id.lstImage);
            txtRedeem = view.findViewById(R.id.txtActionChange);
            txtCouponTitle = view.findViewById(R.id.txtCouponTitle);
            txtCouponDesc = view.findViewById(R.id.txtCouponDesc);
            actionRedeem = view.findViewById(R.id.actionRedeem);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull CouponListViewHolder holder, int position) {
        final Coupon coupon = list.get(position);
        imageFetcher.loadImage(coupon.getUrl() ,holder.lstImage);
        holder.txtCouponTitle.setText(list.get(position).getCouponTitle());
        holder.txtCouponDesc.setText(String.format(context.getString(R.string.getOff),String.valueOf(coupon.getDiscount())));

        holder.actionRedeem.setVisibility(isFromNav?View.GONE:View.VISIBLE);
        holder.actionRedeem.setBackgroundResource(coupon.getStatus().equals("1")?R.drawable.bg_button_red_outline:R.drawable.bg_button_outline);
        holder.actionRedeem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(clickListener != null){
                    if(!coupon.getStatus().equals("1")){
                        clickListener.onItemClick(coupon);
                    }
                }
            }
        });
    }

    @NonNull
    @Override
    public CouponListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_coupon, parent, false);
        return new CouponListViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
