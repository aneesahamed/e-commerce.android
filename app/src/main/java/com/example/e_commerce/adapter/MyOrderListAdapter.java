package com.example.e_commerce.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.e_commerce.R;
import com.example.e_commerce.common.PreferenceManager;
import com.example.e_commerce.common.Utils;
import com.example.e_commerce.common.imageUtils.ImageFetcher;
import com.example.e_commerce.model.entity.Order;
import com.example.e_commerce.model.entity.Product;
import com.example.e_commerce.model.entity.Review;
import com.example.e_commerce.model.entity.User;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MyOrderListAdapter extends RecyclerView.Adapter<MyOrderListAdapter.MyOrderListViewHolder> {

    private ImageFetcher imageFetcher;
    private Context context;
    private List<Order> orderList;
    private User user;
    public interface Listener{
        void onClick(View view ,Order order);
    }
    private Listener listener;

    public MyOrderListAdapter(Context context, ImageFetcher imageFetcher, List<Order> list ,Listener listener) {
        this.context = context;
        this.imageFetcher = imageFetcher;
        this.orderList = list;
        this.listener = listener;
        this.user = PreferenceManager.getUserInfo(context);
    }

    public static class MyOrderListViewHolder extends RecyclerView.ViewHolder {
        private TextView productName, productColor, txtPrice, txtOrgPrice, txtQty,actionReview,txtOrderId ,txtDeliveryInfo;
        ImageView imgProduct;
        LinearLayout productInfo;

        MyOrderListViewHolder(View view) {
            super(view);
            productName = view.findViewById(R.id.productName);
            productColor = view.findViewById(R.id.productColor);
            imgProduct = view.findViewById(R.id.imgProduct);
            txtPrice = view.findViewById(R.id.txtProductPrice);
            actionReview = view.findViewById(R.id.actionReview);
            txtOrgPrice = view.findViewById(R.id.txtProductPriceOrg);
            txtQty = view.findViewById(R.id.txtQty);
            productInfo = view.findViewById(R.id.productInfo);
            txtOrderId = view.findViewById(R.id.txtOrderId);
            txtDeliveryInfo = view.findViewById(R.id.txtDeliveryInfo);
        }
    }

    @NonNull
    @Override
    public MyOrderListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_myorder, parent, false);
        return new MyOrderListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyOrderListViewHolder holder, int position) {
        final Order order = orderList.get(position);
        Product product = order.getProduct();
        imageFetcher.loadImage(product.getImgList().get(0), holder.imgProduct);
        holder.productName.setText(product.getName());
        if (!TextUtils.isEmpty(product.getColor())) {
            holder.productColor.setText(String.format(context.getString(R.string.prodColor), product.getColor()));
        }
        holder.txtPrice.setText(String.format(context.getString(R.string.currency), String.valueOf(Utils.getDiscountValue(product.getAmount(), product.getDisCount()))));
        holder.txtOrgPrice.setText(String.valueOf(product.getAmount()));
        holder.txtOrgPrice.setPaintFlags(holder.txtOrgPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.txtQty.setText(String.format(context.getString(R.string.qty), String.valueOf(product.getQty())));
        holder.txtOrderId.setText(String.format(context.getString(R.string.orderId),order.getOrderId().substring(0,8)));
        holder.txtDeliveryInfo.setText(Utils.getDayDiff(context ,order.getExpectedTime()));
        if(product.getReviewList() != null && product.getReviewList().size() >0){
            for(Review review :product.getReviewList()){
                if(review.getUid().equals(user.getUid())){
                    holder.actionReview.setVisibility(View.GONE);
                }
                else {
                    holder.actionReview.setVisibility(View.VISIBLE);
                }
            }
        }
        else {
            holder.actionReview.setVisibility(View.VISIBLE);
        }
        holder.actionReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener!= null){
                    listener.onClick(holder.actionReview ,order);
                }
            }
        });

        holder.productInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener!= null){
                    listener.onClick(holder.productInfo ,order);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }
}
