package com.example.e_commerce.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.e_commerce.R;
import com.example.e_commerce.common.Utils;
import com.example.e_commerce.common.imageUtils.ImageFetcher;
import com.example.e_commerce.model.entity.Product;
import com.example.e_commerce.ui.common.ClickListener;
import com.example.e_commerce.ui.common.OnLoadMoreListener;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ProductListViewHolder> {

    private ClickListener listener;
    private List<Product> productList;
    private Context context;
    private ImageFetcher imageFetcher;
    private OnLoadMoreListener onLoadMoreListener;
    private int totalItemCount = 0, lastVisibleItem = 0;
    private int visibleThreshold = 5;
    public boolean loading;

    public ProductListAdapter(Context context, ImageFetcher imageFetcher, List<Product> productList, RecyclerView recyclerView ,ClickListener listener) {
        this.listener = listener;
        this.context = context;
        this.productList = productList;
        this.imageFetcher = imageFetcher;
        /*if (recyclerView.getLayoutManager() instanceof GridLayoutManager) {
            final GridLayoutManager linearLayoutManager = (GridLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView,
                                       int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        // End has been reached
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }*/
    }

    public static class ProductListViewHolder extends RecyclerView.ViewHolder {
        ImageView lstImage;
        TextView brand, price, originalPrice, discount ,txtProductTitle;

        public ProductListViewHolder(View view) {
            super(view);
            lstImage = view.findViewById(R.id.lstImage);
            txtProductTitle = view.findViewById(R.id.txtProductTitle);
            brand = view.findViewById(R.id.txtProductBrand);
            price = view.findViewById(R.id.txtProductPrice);
            originalPrice = view.findViewById(R.id.txtProductPriceOrg);
            discount = view.findViewById(R.id.txtProductOff);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final ProductListViewHolder holder, int position) {
        Product product = productList.get(position);
        imageFetcher.loadImage(product.getUrl() ,holder.lstImage);
        holder.txtProductTitle.setText(product.getName());
        holder.price.setText(String.format(context.getString(R.string.currency) ,String.valueOf(Utils.getDiscountValue(product.getAmount() ,product.getDisCount()))));
        holder.originalPrice.setText(String.valueOf(product.getAmount()));
        holder.originalPrice.setPaintFlags(holder.originalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.discount.setText(String.format(context.getString(R.string.offText) ,String.valueOf(product.getDisCount())));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener != null){
                    listener.onItemClick(productList.get(holder.getAdapterPosition()));
                }
            }
        });
    }

    @NonNull
    @Override
    public ProductListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_product, parent, false);
        return new ProductListViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return this.productList.size();
    }

    public void setLoaded(){
        loading = false;
    }
    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }
}
