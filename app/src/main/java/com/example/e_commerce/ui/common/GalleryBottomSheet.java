package com.example.e_commerce.ui.common;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.e_commerce.R;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import static android.app.Activity.RESULT_OK;

public class GalleryBottomSheet extends BottomSheetDialogFragment {

    public GalleryBottomSheet(){
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView =  inflater.inflate(R.layout.dialog_gallery_chooser, container, false);

        LinearLayout camera = rootView.findViewById(R.id.actionCamera);
        LinearLayout gallery = rootView.findViewById(R.id.actionGallery);

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getTargetFragment().onActivityResult(getTargetRequestCode(), RESULT_OK,
                        getActivity().getIntent().putExtra("selection" ,false));
                dismiss();
            }
        });

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getTargetFragment().onActivityResult(getTargetRequestCode(), RESULT_OK,
                        getActivity().getIntent().putExtra("selection" ,true));
                dismiss();

            }
        });



        return rootView;
    }

    @Override
    public void dismiss() {
        super.dismiss();
        setRetainInstance(false);
    }

    @Override
    public void onDestroyView() {
        if (getDialog() != null && getRetainInstance())
            getDialog().setDismissMessage(null);
        super.onDestroyView();

    }

    @Override
    public void onCancel(@NonNull DialogInterface dialog) {
        super.onCancel(dialog);
        setRetainInstance(false);
    }
}
