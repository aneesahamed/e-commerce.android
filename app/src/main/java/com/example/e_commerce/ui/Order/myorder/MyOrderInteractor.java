package com.example.e_commerce.ui.Order.myorder;

import com.example.e_commerce.model.entity.Order;
import com.example.e_commerce.ui.BaseInteractor;

import java.util.List;

public interface MyOrderInteractor extends BaseInteractor {
    @Override
    void noInternet();

    @Override
    void showLoader();

    @Override
    void hideLoader();

    void onResult(List<Order> list);
}
