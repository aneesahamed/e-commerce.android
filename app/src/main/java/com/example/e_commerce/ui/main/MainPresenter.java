package com.example.e_commerce.ui.main;

import android.content.Context;
import android.os.AsyncTask;

import com.example.e_commerce.common.AppController;
import com.example.e_commerce.model.entity.HomeFeed;
import com.example.e_commerce.model.entity.User;


public class MainPresenter {
    MainInteractor interactor;

    MainPresenter(MainInteractor interactor) {
        this.interactor = interactor;
    }

    public void getHomeFeed(int pagination , User user ,boolean isLoadMore){
        new GetHomeFeed(interactor ,user ,isLoadMore).execute(pagination);
    }

    public static class GetHomeFeed extends AsyncTask<Integer ,Void , HomeFeed> {
        private MainInteractor interactor;
        private User user;
        private boolean isLoadMore;
        GetHomeFeed(MainInteractor interactor ,User user ,boolean isLoadMore){
            this.interactor = interactor;
            this.user = user;
            this.isLoadMore = isLoadMore;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected HomeFeed doInBackground(Integer... page) {
            return AppController.getInstance().getDataBaseHelper().getHomeFeed(user ,page[0]);
        }

        @Override
        protected void onPostExecute(HomeFeed homeFeed) {
           if(homeFeed != null){
               if(interactor != null){
                   interactor.updateUI(homeFeed ,isLoadMore);
               }
           }
           else {
               if(interactor != null){
                   interactor.showError();
               }
           }
        }
    }
}
