package com.example.e_commerce.ui.common;

public interface OnLoadMoreListener {

    void onLoadMore();
}
