package com.example.e_commerce.ui.product.coupons;

import android.content.Context;
import android.database.Cursor;

import com.example.e_commerce.common.imageUtils.AsyncTask;
import com.example.e_commerce.model.entity.Coupon;
import com.example.e_commerce.model.entity.CouponsContract;

import java.util.ArrayList;
import java.util.List;

import static com.example.e_commerce.model.DBConstant.COUPON_ID;
import static com.example.e_commerce.model.DBConstant.COUPON_TITLE;
import static com.example.e_commerce.model.DBConstant.DISCOUNT;
import static com.example.e_commerce.model.DBConstant.REDEEMED_STATE;
import static com.example.e_commerce.model.DBConstant.URL;

public class CouponPresenter {
    private CouponInteractor interactor;

    public CouponPresenter(CouponInteractor interactor) {
        this.interactor = interactor;
    }

    public void getCouponList(Context context ,String uid) {
        new GetCouponList(context ,interactor).execute(uid);
    }

    public static class GetCouponList extends AsyncTask<String, Void, List<Coupon>> {
        private CouponInteractor interactor;
        private Context context;

        GetCouponList(Context context, CouponInteractor interactor) {
            this.interactor = interactor;
            this.context = context;
        }

        @Override
        protected List<Coupon> doInBackground(String... id) {
            Cursor cursor = context.getContentResolver().query(CouponsContract.CouponsEntry.getAddressUri(id[0]), null, null, null, null);
            List<Coupon> list = new ArrayList<>();
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    Coupon coupon = new Coupon();
                    coupon.setCouponId(cursor.getString(cursor.getColumnIndex(COUPON_ID)));
                    coupon.setCouponTitle(cursor.getString(cursor.getColumnIndex(COUPON_TITLE)));
                    coupon.setDiscount(cursor.getInt(cursor.getColumnIndex(DISCOUNT)));
                    coupon.setStatus(cursor.getString(cursor.getColumnIndex(REDEEMED_STATE)));
                    coupon.setUrl(cursor.getString(cursor.getColumnIndex(URL)));
                    list.add(coupon);
                } while (cursor.moveToNext());
                cursor.close();
            }
            return list;
        }

        @Override
        protected void onPostExecute(List<Coupon> list) {
            if (interactor != null) {
                interactor.hideLoader();
                interactor.onResult(list);
            }
        }
    }
}
