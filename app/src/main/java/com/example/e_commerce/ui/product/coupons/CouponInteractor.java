package com.example.e_commerce.ui.product.coupons;

import com.example.e_commerce.model.entity.Coupon;
import com.example.e_commerce.ui.BaseInteractor;

import java.util.List;

public interface CouponInteractor extends BaseInteractor {
    @Override
    void showLoader();

    @Override
    void hideLoader();

    @Override
    void noInternet();

    void onResult(List<Coupon> list);
}
