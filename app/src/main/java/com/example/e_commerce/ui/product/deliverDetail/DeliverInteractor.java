package com.example.e_commerce.ui.product.deliverDetail;

import com.example.e_commerce.model.entity.Address;
import com.example.e_commerce.ui.BaseInteractor;

public interface DeliverInteractor extends BaseInteractor {
    @Override
    void showLoader();

    @Override
    void hideLoader();

    @Override
    void noInternet();

    void updateAddress(Address address);

}
