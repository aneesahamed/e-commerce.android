package com.example.e_commerce.ui.product.productList;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.e_commerce.R;
import com.example.e_commerce.adapter.SearchProductList;
import com.example.e_commerce.common.AppConstant;
import com.example.e_commerce.common.Utils;
import com.example.e_commerce.common.imageUtils.ImageFetcher;
import com.example.e_commerce.model.entity.Product;
import com.example.e_commerce.ui.BaseActivity;
import com.example.e_commerce.ui.common.ClickListener;
import com.example.e_commerce.ui.common.FilterBottomSheet;
import com.example.e_commerce.ui.common.OnLoadMoreListener;
import com.example.e_commerce.ui.common.SortingBottomsheet;
import com.example.e_commerce.ui.product.ProductActivity;
import com.example.e_commerce.ui.suggestion.SuggestionListActivity;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ProductListActivity extends BaseActivity implements ProductHeadlessFragment.FragListener, SortingBottomsheet.CallBack {

    private ProductHeadlessFragment headlessFrag;
    private int pagination = 0;
    private boolean isPaginationEnd;
    private List<Product> productList = new ArrayList<>();
    private SearchProductList adapter;
    private RecyclerView productListRecycler;
    private ProgressBar progressBar;
    private String keyWord;
    private RelativeLayout rlFilter;
    private FrameLayout txtSort, txtFilter;
    private boolean isFromSearch;
    private boolean isFromFilter;
    private Product product;
    private String sort;
    SearchView searchView;
    private String searchKeyWord = AppConstant.EMPTY;
    boolean isSearchViewCollapsed = true;
    private ImageFetcher imageFetcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.search));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        imageFetcher = Utils.getImageFetcher(this, true);
        productListRecycler = findViewById(R.id.productListRecycler);
        txtSort = findViewById(R.id.txtSort);
        rlFilter = findViewById(R.id.rlFilter);
        txtFilter = findViewById(R.id.txtFilter);
        productListRecycler.setLayoutManager(new GridLayoutManager(this, 2));
        progressBar = findViewById(R.id.progressBar);
        adapter = new SearchProductList(this, imageFetcher, productList, new ClickListener() {
            @Override
            public void onItemClick(Object object) {
                Product product = (Product) object;
                if (product != null) {
                    Intent intent = new Intent(ProductListActivity.this, ProductActivity.class);
                    intent.putExtra("id", product.getpId());
                    startActivity(intent);
                }
            }
        }, productListRecycler);
        productListRecycler.setAdapter(adapter);
        adapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (!isPaginationEnd) {
                    if (!isFromFilter) {
                        headlessFrag.getData(isFromSearch, keyWord, sort, pagination, true);
                    } else {
                        headlessFrag.getData(product, sort, pagination, true);
                    }
                }
            }
        });

        txtSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SortingBottomsheet galleryBottomSheet = new SortingBottomsheet();
                galleryBottomSheet.setListener(ProductListActivity.this);
                galleryBottomSheet.show(getSupportFragmentManager(), SortingBottomsheet.class.getSimpleName());
            }
        });

        txtFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FilterBottomSheet filterBottomSheet = new FilterBottomSheet();
                Bundle args = new Bundle();
                args.putString("cid", productList.get(0).getcId());
                filterBottomSheet.setArguments(args);
                filterBottomSheet.setListener(ProductListActivity.this);
                filterBottomSheet.show(getSupportFragmentManager(), FilterBottomSheet.class.getSimpleName());
            }
        });
        headlessFrag = (ProductHeadlessFragment) getSupportFragmentManager().findFragmentByTag(ProductHeadlessFragment.class.getSimpleName());
        if (headlessFrag == null) {
            headlessFrag = new ProductHeadlessFragment();
            getSupportFragmentManager().beginTransaction().add(headlessFrag, ProductHeadlessFragment.class.getSimpleName()).commit();
        }

        if (savedInstanceState != null) {
            sort = savedInstanceState.getString("sort");
            isFromFilter = savedInstanceState.getBoolean("isFromFilter");
            isFromSearch = savedInstanceState.getBoolean("isFromSearch");
            keyWord = savedInstanceState.getString("key");
            pagination = savedInstanceState.getInt("page");
            isPaginationEnd = savedInstanceState.getBoolean("pageEnd");
            productList.addAll(headlessFrag.getData());
            adapter.notifyDataSetChanged();
            isSearchViewCollapsed = savedInstanceState.getBoolean("isSearchCollapsed");
            searchKeyWord = savedInstanceState.getString("searchedKeyword");
            if (!isSearchViewCollapsed) {
                invalidateOptionsMenu();
            }
        } else {
            isFromSearch = getIntent().getBooleanExtra("isFromSearch", false);
            if (getIntent().hasExtra("cid")) {
                keyWord = getIntent().getStringExtra("cid");
                pagination = 0;
                isPaginationEnd = false;
                headlessFrag.getData(isFromSearch, keyWord, null, pagination, false);
            }
        }

        rlFilter.setVisibility(TextUtils.isEmpty(keyWord) ? View.GONE : View.VISIBLE);
    }

    @Override
    public void onResult(Product product) {
        this.isFromFilter = true;
        this.product = product;
        pagination = 0;
        isPaginationEnd = false;
        headlessFrag.getData(product, null, pagination, false);
    }

    @Override
    public void onResult(String sort) {
        this.sort = sort.equals("L") ? "ASC" : "DESC";
        pagination = 0;
        isPaginationEnd = false;
        if (isFromFilter) {
            headlessFrag.getData(product, null, pagination, false);
        } else {
            headlessFrag.getData(isFromSearch, keyWord, this.sort, pagination, false);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_items, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.action_cart).setVisible(false);
        menu.findItem(R.id.action_wish_list).setVisible(false);
        menu.findItem(R.id.action_search).setVisible(false);
        MenuItem menuItem = menu.findItem(R.id.action_searchClass);
        menuItem.setVisible(true);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_searchClass).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconified(true);
        searchView.setQueryRefinementEnabled(true);
        if (getIntent().getBooleanExtra("isFromSearch", false)) {
            menuItem.expandActionView();
            isSearchViewCollapsed = false;
        }
        if (!isSearchViewCollapsed) {
            menuItem.expandActionView();
            searchView.setQuery(searchKeyWord, true);
        }

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                isSearchViewCollapsed = true;
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                searchKeyWord = s;
                return false;
            }
        });
        menuItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                boolean isSearchViewCollapsed = false;
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                boolean isSearchViewCollapsed = true;
                return true;
            }
        });
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        this.isFromSearch = true;
        this.isFromFilter = false;
        intent.putExtra("isFromSearch", true);
        setIntent(intent);
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            doQuery(query, false);

        }
        if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            Uri uri = intent.getData();
            doQuery(uri.getLastPathSegment(), false);

        }

    }

    private void doQuery(String keyWord, boolean isLoadMore) {
        this.keyWord = keyWord;
        if (headlessFrag != null) {
            pagination = 0;
            isPaginationEnd = false;
            headlessFrag.getData(this.isFromSearch, keyWord, null, pagination, isLoadMore);
        }
    }

    @Override
    public void noInternet() {

    }

    @Override
    public void hideLoader() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showLoader() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("key", keyWord);
        outState.putInt("page", pagination);
        outState.putBoolean("pageEnd", isPaginationEnd);
        outState.putBoolean("isFromSearch", isFromSearch);
        outState.putBoolean("isFromFilter", isFromFilter);
        outState.putString("sort", sort);
        outState.putBoolean("isSearchCollapsed", isSearchViewCollapsed);
        if (!isSearchViewCollapsed) {
            outState.putString("searchedKeyword", searchKeyWord);
        }

    }

    @Override
    public void onResult(boolean isLoadMore, List<Product> list) {
        if (list.size() > 0) {
            rlFilter.setVisibility(View.VISIBLE);
            if (!isLoadMore) {
                pagination = 1;
                productList.clear();
                productList.addAll(list);
                adapter.notifyDataSetChanged();
            } else {
                pagination = pagination + 1;
                productList.addAll(list);
                adapter.notifyDataSetChanged();
                adapter.setLoaded();
            }
        } else {
            if (!isLoadMore) {
                rlFilter.setVisibility(View.GONE);
            } else {
                isPaginationEnd = true;
            }
        }
    }
}
