package com.example.e_commerce.ui.Order.review;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.e_commerce.R;
import com.example.e_commerce.common.PreferenceManager;
import com.example.e_commerce.common.Utils;
import com.example.e_commerce.common.imageUtils.AsyncTask;
import com.example.e_commerce.common.imageUtils.ImageFetcher;
import com.example.e_commerce.model.entity.Order;
import com.example.e_commerce.model.entity.Product;
import com.example.e_commerce.model.entity.User;
import com.example.e_commerce.ui.common.GalleryBottomSheet;
import com.example.e_commerce.ui.profile.ProfileFragment;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReviewFragment extends Fragment implements  ReviewInteractor {


    LinearLayout imgViewList;
    RatingBar ratingBar;
    ImageFetcher imageFetcher;
    TextView productName, productColor, txtPrice, txtOrgPrice;
    ImageView imgProduct;
    Order order;
    EditText edReview;
    float rating;
    User user;
    File tempFile;
    ReviewPresenter presenter;
    static final int RESULT_OPEN_GALLERY = 0;
    static final int RESULT_OPEN_CAMERA = 1;
    public static final int REQ_GALLERY_CHOOSER = 100;

    List<String> imgList = new ArrayList<>();
    public ReviewFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_review, container, false);
        imgViewList = view.findViewById(R.id.imgListView);
        ratingBar = view.findViewById(R.id.ratingBar);
        imageFetcher = Utils.getImageFetcher(getActivity(), true);
        productName = view.findViewById(R.id.productName);
        productColor = view.findViewById(R.id.productColor);
        imgProduct = view.findViewById(R.id.imgProduct);
        txtPrice = view.findViewById(R.id.txtProductPrice);
        txtOrgPrice = view.findViewById(R.id.txtProductPriceOrg);
        edReview = view.findViewById(R.id.edReview);
        user = PreferenceManager.getUserInfo(getActivity());
        presenter = new ReviewPresenter(this);
        if (savedInstanceState == null) {
            order = getArguments().getParcelable("order");
            populateProductInfo(order.getProduct());
        } else {
            rating = savedInstanceState.getFloat("rating");
            edReview.setText(savedInstanceState.getString("review"));
            addImage(imgList);

        }
        return view;
    }

    private void populateProductInfo(Product product) {
        imageFetcher.loadImage(product.getImgList().get(0), imgProduct);
        productName.setText(product.getName());
        if (!TextUtils.isEmpty(product.getColor())) {
            productColor.setText(String.format(getString(R.string.prodColor), product.getColor()));
        }
        txtPrice.setText(String.format(getString(R.string.currency), String.valueOf(Utils.getDiscountValue(product.getAmount(), product.getDisCount()))));
        txtOrgPrice.setText(String.valueOf(product.getAmount()));
        txtOrgPrice.setPaintFlags(txtOrgPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

    }

    public void addImage(final List<String> list) {
        imgViewList.removeAllViews();
        for (int i = 0; i < list.size(); i++) {
            final int pos = i;
            final View infatedView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_selected_img, null, false);
            RelativeLayout close = infatedView.findViewById(R.id.close);
            ImageView imageView = infatedView.findViewById(R.id.lstImage);
            imageView.setImageBitmap(Utils.decodeSampledBitmap(list.get(i),70,70));
            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    list.remove(pos);
                    addImage(list);
                }
            });
            imgViewList.addView(infatedView);
        }
    }


    public void openDialogChooser() {
        if (!(imgList.size() > 4)) {
            GalleryBottomSheet galleryBottomSheet = new GalleryBottomSheet();
            galleryBottomSheet.setTargetFragment(ReviewFragment.this, REQ_GALLERY_CHOOSER);
            galleryBottomSheet.show(getFragmentManager(), GalleryBottomSheet.class.getSimpleName());
        }
    }

    public void updateReview() {
        String rating = String.valueOf(ratingBar.getRating());
        presenter.addReview(user ,rating ,edReview.getText().toString(),imgList,order);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == REQ_GALLERY_CHOOSER && data != null) {
            openChooserDialog(data.getExtras().getBoolean("selection"));
        } else if (resultCode != RESULT_CANCELED && requestCode == RESULT_OPEN_CAMERA && resultCode == RESULT_OK) {
            Uri capturedUri = FileProvider.getUriForFile(getActivity(), getContext().getPackageName() + ".fileProvider", tempFile);
            imgList.add(tempFile.getAbsolutePath());
            addImage(imgList);


        } else if (resultCode != RESULT_CANCELED && requestCode == RESULT_OPEN_GALLERY && resultCode == RESULT_OK && data != null) {
            Uri uri = data.getData();
            imgList.add(Utils.FilePath.getPath(getActivity(), uri));
            addImage(imgList);

        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                Log.v("FileChoose", "Permission: " + permissions[0] + "was " + grantResults[0]);
                openCamera();
            } else {
                Toast.makeText(getContext(), R.string.app_permission_camera, Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == 2) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                Log.v("FileChoose", "Permission: " + permissions[0] + "was " + grantResults[0]);
                Log.v("FileChoose", "Permission: " + permissions[1] + "was " + grantResults[1]);
                openGallery();
            } else {
                Toast.makeText(getContext(), R.string.app_permission_gallery, Toast.LENGTH_LONG).show();
            }
        }
    }

    public void openChooserDialog(boolean isGallery) {
        if (isGallery) {
            if (Build.VERSION.SDK_INT >= 23) {
                if (Utils.isCameraPermissionGiven(getActivity())) {
                    Log.v("Permission", "Permission is granted");
                    openGallery();
                } else {
                    Log.v("Permission", "Permission is not granted");
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
                }
            } else {
                openGallery();
            }
        } else {

            if (Build.VERSION.SDK_INT >= 23) {
                if (Utils.isCameraPermissionGiven(getActivity())) {
                    Log.v("Permission", "Permission is granted");
                    openCamera();
                } else {
                    Log.v("Permission", "Permission is not granted");
                    requestPermissions(new String[]{android.Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                }
            } else {
                openCamera();
            }

        }
    }

    public void openGallery() {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        i.setType("image/*");
        startActivityForResult(i, RESULT_OPEN_GALLERY);
    }

    public void openCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
            tempFile = Utils.createTempFile(getActivity());
            if (tempFile != null) {
                Uri contentUri = FileProvider.getUriForFile(getActivity(), getActivity().getPackageName() + ".fileProvider", tempFile);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, contentUri);
                startActivityForResult(intent, RESULT_OPEN_CAMERA);

            }
        }
    }

    @Override
    public void noInternet() {

    }

    @Override
    public void showFieldError() {

    }

    @Override
    public void showLoader() {
        Toast.makeText(getActivity() ,getString(R.string.requiredContent),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void hideLoader() {

    }

    @Override
    public void onResult() {
        Toast.makeText(getActivity() ,getString(R.string.reviewAdded),Toast.LENGTH_SHORT).show();
        if(getActivity() != null){
            setRetainInstance(false);
            getActivity().getSupportFragmentManager().popBackStack();
        }
    }

    public static class GetBitmaps extends AsyncTask<List<String> ,Void,List<Bitmap>>{
        List<Bitmap> bitmapList = new ArrayList<>();
        public interface Listener{
            void onResult(List<Bitmap> list);
        }
        private Listener listener;
        @Override
        protected List<Bitmap> doInBackground(List<String>... lists) {
           for(String path : lists[0]){
               Bitmap bitmap = Utils.decodeSampledBitmap(path, 70, 70);
               bitmapList.add(bitmap);
           }
           return bitmapList;
        }

        @Override
        protected void onPostExecute(List<Bitmap> bitmaps) {
            listener.onResult(bitmapList);
        }
    }
}
