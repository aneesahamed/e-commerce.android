package com.example.e_commerce.ui.home;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.core.view.MenuItemCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.e_commerce.R;
import com.example.e_commerce.common.AppConstant;
import com.example.e_commerce.common.AppController;
import com.example.e_commerce.common.PreferenceManager;
import com.example.e_commerce.common.Utils;
import com.example.e_commerce.common.imageUtils.RoundedCornerImageView;
import com.example.e_commerce.model.entity.Coupon;
import com.example.e_commerce.model.entity.Product;
import com.example.e_commerce.model.entity.User;
import com.example.e_commerce.ui.BaseActivity;
import com.example.e_commerce.ui.Order.OrderActivity;
import com.example.e_commerce.ui.Order.myorder.MyOrderList;
import com.example.e_commerce.ui.Order.orderDetail.OrderDetailFragment;
import com.example.e_commerce.ui.Order.review.ReviewFragment;
import com.example.e_commerce.ui.SearchDialogActivity;
import com.example.e_commerce.ui.cart.CartListFragment;
import com.example.e_commerce.ui.common.HomeFragListener;
import com.example.e_commerce.ui.login.LoginActivity;
import com.example.e_commerce.ui.main.MainFragment;
import com.example.e_commerce.ui.product.ProductActivity;
import com.example.e_commerce.ui.product.ProductHomeListener;
import com.example.e_commerce.ui.product.SuccessPage.SuccessFragment;
import com.example.e_commerce.ui.product.address.AddressChangeFragment;
import com.example.e_commerce.ui.product.address.ChooseAddressFragment;
import com.example.e_commerce.ui.product.coupons.CouponsListFrag;
import com.example.e_commerce.ui.product.deliverDetail.DeliverFragment;
import com.example.e_commerce.ui.product.payment.PaymentFragment;
import com.example.e_commerce.ui.product.paymentMode.PaymentModeFrag;
import com.example.e_commerce.ui.product.productDetail.ProductDetailFragment;
import com.example.e_commerce.ui.product.productList.ProductHeadlessFragment;
import com.example.e_commerce.ui.product.productList.ProductListActivity;
import com.example.e_commerce.ui.product.wishList.WishListFragment;
import com.example.e_commerce.ui.profile.ProfileFragment;

import java.io.File;

public class HomeActivity extends BaseActivity implements ProductHomeListener, ProfileFragment.profileListener {

    DrawerLayout drawer;
    int selectedNavItem;
    private boolean isHomeFragment = true;
    TextView toolbarTitle;
    String tag;
    Product product;
    Toolbar toolbar;
    boolean isHome;
    User user;
    RoundedCornerImageView imgProfile;
    TextView txtName, txtEmail;
    ActionBarDrawerToggle toggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        imgProfile = findViewById(R.id.imgProfile);
        txtName = findViewById(R.id.txtName);
        txtEmail = findViewById(R.id.txtEmail);
        user = PreferenceManager.getUserInfo(this);
        drawer = findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.setDrawerIndicatorEnabled(false);
        toggle.setHomeAsUpIndicator(R.drawable.ic_menu_button);
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(Gravity.LEFT);
            }
        });
        toggle.syncState();

        drawer.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {

            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });
        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigate(ProfileFragment.class.getSimpleName(), null);
            }
        });

        if (savedInstanceState == null) {
            MainFragment mainFragment = (MainFragment) getSupportFragmentManager().findFragmentByTag(MainFragment.class.getSimpleName());
            if (mainFragment == null) {
                isHome = true;
                tag = MainFragment.class.getSimpleName();
                getSupportFragmentManager().beginTransaction().add(R.id.fragContainer, new MainFragment(), MainFragment.class.getSimpleName()).commit();

            }
        } else {
            tag = savedInstanceState.getString("tag");
            updateNavIcon(tag);
            setTitle(tag);
            invalidateOptionsMenu();
        }

        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {

               /* Fragment productFrag = getSupportFragmentManager().findFragmentByTag(ProductDetailFragment.class.getSimpleName());
                Fragment deliverFrag = getSupportFragmentManager().findFragmentByTag(DeliverFragment.class.getSimpleName());
                Fragment addressChangeFrag = getSupportFragmentManager().findFragmentByTag(AddressChangeFragment.class.getSimpleName());
                Fragment chooseFrag = getSupportFragmentManager().findFragmentByTag(ChooseAddressFragment.class.getSimpleName());
                Fragment paymentFrag = getSupportFragmentManager().findFragmentByTag(PaymentFragment.class.getSimpleName());
                if(productFrag!=null && productFrag.isVisible()){
                    tag = ProductDetailFragment.class.getSimpleName();
                }
                else if(deliverFrag != null && deliverFrag.isVisible()) {
                    tag = DeliverFragment.class.getSimpleName();
                }
                else if(addressChangeFrag != null && addressChangeFrag.isVisible()) {
                    tag = AddressChangeFragment.class.getSimpleName();
                }
                else if(chooseFrag != null && chooseFrag.isVisible()){
                    tag = ChooseAddressFragment.class.getSimpleName();
                }
                else if(paymentFrag != null && paymentFrag.isVisible()){
                    tag = PaymentFragment.class.getSimpleName();
                }
                else {
                    tag = AppConstant.EMPTY;
                }*/
                for (Fragment fragment : getSupportFragmentManager().getFragments()) {
                    if (fragment != null && fragment.isVisible()) {
                        tag = fragment.getTag();
                    }
                }
                setTitle(tag);
                updateNavIcon(tag);
                invalidateOptionsMenu();
            }
        });

        updateProfileStatus();
    }

    @Override
    public void updateNav() {
        user = PreferenceManager.getUserInfo(this);
        updateProfileStatus();
    }

    private void updateProfileStatus() {
        txtName.setText(user.getName());
        txtEmail.setText(user.getEmail());
        if (!TextUtils.isEmpty(user.getProfileUri())) {
            File file = new File(user.getProfileUri());
            if (file.exists()) {
                imgProfile.setImageBitmap(Utils.decodeSampledBitmap(file.getAbsolutePath(), 70, 70));
            }
        } else {
            imgProfile.setImageDrawable(getResources().getDrawable(R.drawable.placeholder));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        invalidateOptionsMenu();
    }

    public void navItemsClick(View view) {
        switch (view.getId()) {
            case R.id.llMyOrder:
                navigate(MyOrderList.class.getSimpleName(), null);
                break;
            case R.id.llMyCart:
                navigate(CartListFragment.class.getSimpleName(), null);
                break;
            case R.id.llMyWishList:
                navigate(WishListFragment.class.getSimpleName(), null);
                break;
            case R.id.llMyCoupon:
                Bundle args = new Bundle();
                args.putBoolean("isFromNav" ,true);
                navigate(CouponsListFrag.class.getSimpleName(), args);
                break;
            case R.id.llMyProfile:
                navigate(ProfileFragment.class.getSimpleName(), null);
                break;
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("tag", tag);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_items, menu);
        MenuItem cartItem = menu.findItem(R.id.action_cart);
        return true;

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem cartItem = menu.findItem(R.id.action_cart);
        cartItem.setVisible(false);
        menu.findItem(R.id.action_wish_list).setVisible(false);
        menu.findItem(R.id.action_search).setVisible(false);
        menu.findItem(R.id.action_save).setVisible(false);
        menu.findItem(R.id.action_logout).setVisible(false);
        if (tag.equals(MainFragment.class.getSimpleName())) {
            menu.findItem(R.id.action_cart).setVisible(true);
            menu.findItem(R.id.action_wish_list).setVisible(true);
            menu.findItem(R.id.action_search).setVisible(true);
            View view = cartItem.getActionView();
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    navigate(CartListFragment.class.getSimpleName(), null);
                }
            });
            int count = AppController.getInstance().getDataBaseHelper().getMyCartCount(user.getUid());
            TextView txtCount = view.findViewById(R.id.txtCount);
            txtCount.setText(String.valueOf(count));
            txtCount.setVisibility(count > 0 ? View.VISIBLE : View.INVISIBLE);
        } else if (tag.equals(ProfileFragment.class.getSimpleName())) {
            menu.findItem(R.id.action_logout).setVisible(true);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_search) {
            Intent intent = new Intent(this, ProductListActivity.class);
            intent.putExtra("isFromSearch", true);
            startActivity(intent);
        } else if (item.getItemId() == R.id.action_cart) {
            navigate(CartListFragment.class.getSimpleName(), null);
        } else if (item.getItemId() == R.id.action_wish_list) {
            navigate(WishListFragment.class.getSimpleName(), null);
        } else if (item.getItemId() == R.id.action_logout) {
            PreferenceManager.clearUserInfo(this);
            Intent intent = new Intent(this, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }


    private void updateNavIcon(String tag) {
        if (tag.equals(MainFragment.class.getSimpleName())) {

            toggle.setDrawerIndicatorEnabled(false);
            toggle.setDrawerSlideAnimationEnabled(true);
            toggle.setToolbarNavigationClickListener(toggle.getToolbarNavigationClickListener());
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            toggle.setHomeAsUpIndicator(R.drawable.ic_menu_button);
            toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    drawer.openDrawer(Gravity.LEFT);
                }
            });
        } else {

            toggle.setDrawerIndicatorEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                    invalidateOptionsMenu();
                }
            });
        }
    }

    @Override
    public void setProduct(Product product) {

    }

    @Override
    public void openDialogVariant() {

    }

    @Override
    public void updateCoupons(Coupon coupon) {
        removeRetainedFragment();
        getSupportFragmentManager().popBackStackImmediate();
        PaymentFragment paymentfrag = (PaymentFragment) getSupportFragmentManager().findFragmentByTag(PaymentFragment.class.getSimpleName());
        if (paymentfrag != null) {
            paymentfrag.applyCoupons(coupon);
        }
    }

    @Override
    public void navigate(String tag, Object object) {
        this.isHome = false;
        if (!tag.equals(MyOrderList.class.getSimpleName())) {
            this.tag = tag;
        }
        if (tag.equals(WishListFragment.class.getSimpleName())) {
            WishListFragment wishListFragment = new WishListFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.fragContainer, wishListFragment, WishListFragment.class.getSimpleName()).addToBackStack(null).commit();


        } else if (tag.equals(CartListFragment.class.getSimpleName())) {
            CartListFragment cartListFragment = new CartListFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.fragContainer, cartListFragment, CartListFragment.class.getSimpleName()).addToBackStack(null).commit();

        } else if (tag.equals(CouponsListFrag.class.getSimpleName()) && object != null) {
            CouponsListFrag couponsListFrag = new CouponsListFrag();
            couponsListFrag.setArguments((Bundle)object);
            getSupportFragmentManager().beginTransaction().replace(R.id.fragContainer, couponsListFrag, CouponsListFrag.class.getSimpleName()).addToBackStack(null).commit();

        } else if (tag.equals(ProductDetailFragment.class.getSimpleName())) {
            Product product = (Product) object;
            if (product != null) {
                Intent intent = new Intent(this, ProductActivity.class);
                intent.putExtra("id", product.getpId());
                startActivity(intent);
            }
        } else if (tag.equals(PaymentFragment.class.getSimpleName())) {
            PaymentFragment paymentFragment = new PaymentFragment();
            Bundle args = (Bundle) object;
            paymentFragment.setArguments(args);
            getSupportFragmentManager().beginTransaction().replace(R.id.fragContainer, paymentFragment, PaymentFragment.class.getSimpleName()).addToBackStack(null).commit();

        } else if (tag.equals(ProfileFragment.class.getSimpleName())) {
            ProfileFragment profileFragment = new ProfileFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.fragContainer, profileFragment, ProfileFragment.class.getSimpleName()).addToBackStack(null).commit();

        } else if (tag.equals(CouponsListFrag.class.getSimpleName())) {
            CouponsListFrag couponsListFrag = new CouponsListFrag();
            getSupportFragmentManager().beginTransaction().replace(R.id.fragContainer, couponsListFrag, CouponsListFrag.class.getSimpleName()).addToBackStack(null).commit();

        } else if (tag.equals(PaymentModeFrag.class.getSimpleName())) {
            PaymentModeFrag PaymentModeFrag = new PaymentModeFrag();
            Bundle args = (Bundle) object;
            PaymentModeFrag.setArguments(args);
            getSupportFragmentManager().beginTransaction().replace(R.id.fragContainer, PaymentModeFrag, PaymentModeFrag.class.getSimpleName()).addToBackStack(null).commit();

        } else if (tag.equals(SuccessFragment.class.getSimpleName())) {
            SuccessFragment successFragment = new SuccessFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.fragContainer, successFragment, SuccessFragment.class.getSimpleName()).addToBackStack(null).commit();

        } else if (tag.equals(MyOrderList.class.getSimpleName())) {
            drawer.closeDrawers();
            startActivity(new Intent(this, OrderActivity.class));
        }
    }

    @Override
    public void showLoader() {
        showProgressDialog();
    }

    @Override
    public void hideLoader() {
        hideProgressDialog();
    }

    @Override
    public void noInternet() {

    }


    public void removeRetainedFragment() {
        Fragment paymentfrag = getSupportFragmentManager().findFragmentByTag(PaymentFragment.class.getSimpleName());
        Fragment couponsFrag = getSupportFragmentManager().findFragmentByTag(CouponsListFrag.class.getSimpleName());
        Fragment cartListFrag = getSupportFragmentManager().findFragmentByTag(CartListFragment.class.getSimpleName());
        Fragment profileFrag = getSupportFragmentManager().findFragmentByTag(ProfileFragment.class.getSimpleName());
        Fragment payModeFrag = getSupportFragmentManager().findFragmentByTag(PaymentModeFrag.class.getSimpleName());
        Fragment successFrag = getSupportFragmentManager().findFragmentByTag(SuccessFragment.class.getSimpleName());
        Fragment myOrder = getSupportFragmentManager().findFragmentByTag(MyOrderList.class.getSimpleName());
        Fragment reviewFrag = getSupportFragmentManager().findFragmentByTag(ReviewFragment.class.getSimpleName());
        Fragment orderDetail = getSupportFragmentManager().findFragmentByTag(OrderDetailFragment.class.getSimpleName());
        if (paymentfrag != null && paymentfrag.isVisible()) {
            paymentfrag.setRetainInstance(false);
        } else if (couponsFrag != null && couponsFrag.isVisible()) {
            couponsFrag.setRetainInstance(false);
        } else if (cartListFrag != null && cartListFrag.isVisible()) {
            cartListFrag.setRetainInstance(false);
        } else if (profileFrag != null && profileFrag.isVisible()) {
            profileFrag.setRetainInstance(false);
        } else if (payModeFrag != null && payModeFrag.isVisible()) {
            payModeFrag.setRetainInstance(false);
        } else if (successFrag != null && successFrag.isVisible()) {
            successFrag.setRetainInstance(false);
        } else if (myOrder != null && myOrder.isVisible()) {
            myOrder.setRetainInstance(false);
        } else if (orderDetail != null && orderDetail.isVisible()) {
            orderDetail.setRetainInstance(false);
        } else if (reviewFrag != null && reviewFrag.isVisible()) {
            reviewFrag.setRetainInstance(false);
        }
    }

    public void setTitle(String tag) {
        String title = AppConstant.EMPTY;
        if (tag.equals(DeliverFragment.class.getSimpleName())) {
            title = getString(R.string.delivery);
        } else if (tag.equals(AddressChangeFragment.class.getSimpleName())) {
            title = getString(R.string.address);
        } else if (tag.equals(ChooseAddressFragment.class.getSimpleName())) {
            title = getString(R.string.selectAddress);
        } else if (tag.equals(PaymentFragment.class.getSimpleName())) {
            title = getString(R.string.payment);
        } else if (tag.equals(CouponsListFrag.class.getSimpleName())) {
            title = getString(R.string.coupons);
        } else if (tag.equals(CartListFragment.class.getSimpleName())) {
            title = getString(R.string.myCart);
        } else if (tag.equals(WishListFragment.class.getSimpleName())) {
            title = getString(R.string.myWishList);
        } else if (tag.equals(MainFragment.class.getSimpleName())) {
            title = getString(R.string.app_name);
        } else if (tag.equals(ProfileFragment.class.getSimpleName())) {
            title = getString(R.string.myProfile);
        } else if (tag.equals(PaymentModeFrag.class.getSimpleName())) {
            title = getString(R.string.payMode);
        } else if (tag.equals(SuccessFragment.class.getSimpleName())) {
            title = getString(R.string.orderPlaced);
        } else if (tag.equals(MyOrderList.class.getSimpleName())) {
            title = getString(R.string.myOrder);
        } else if (tag.equals(OrderDetailFragment.class.getSimpleName())) {
            title = getString(R.string.orders);
        } else if (tag.equals(ReviewFragment.class.getSimpleName())) {
            title = getString(R.string.review);
        }
        if (toolbar != null) {
            toolbar.setTitle(title);
        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (tag.equals(SuccessFragment.class.getSimpleName())) {
                finish();
                overridePendingTransition(0, 0);
                startActivity(getIntent());
                overridePendingTransition(0, 0);
            } else {
                removeRetainedFragment();
                super.onBackPressed();
            }
        }


    }
}
