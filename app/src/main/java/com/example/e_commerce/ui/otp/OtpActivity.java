package com.example.e_commerce.ui.otp;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.e_commerce.R;
import com.example.e_commerce.common.AppController;
import com.example.e_commerce.common.KeyListener;
import com.example.e_commerce.common.PreferenceManager;
import com.example.e_commerce.common.Utils;
import com.example.e_commerce.common.ValidatorTextWatcher;
import com.example.e_commerce.model.entity.User;
import com.example.e_commerce.ui.BaseActivity;
import com.example.e_commerce.ui.home.HomeActivity;
import com.example.e_commerce.ui.suggestion.SuggestionListActivity;
import com.google.android.material.button.MaterialButton;

import java.util.UUID;

public class OtpActivity extends BaseActivity implements OtpInteractor {

    EditText inputOne,inputTwo,inputThree,inputFour,inputFive,inputSix;
    OtpPresenter presenter;
    Bundle userBundle;
    MaterialButton btnVerify;
    boolean isFromLogin;
    String secretOtp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        inputOne = findViewById(R.id.inputOne);
        inputTwo = findViewById(R.id.inputTwo);
        inputThree = findViewById(R.id.inputThree);
        inputFour = findViewById(R.id.inputFour);
        inputFive = findViewById(R.id.inputFive);
        inputSix = findViewById(R.id.inputSix);
        btnVerify = findViewById(R.id.btnVerify);
        setListener();
        presenter = new OtpPresenter(this);
        if(savedInstanceState != null){
            inputOne.setText(savedInstanceState.getString("one"));
            inputTwo.setText(savedInstanceState.getString("two"));
            inputThree.setText(savedInstanceState.getString("three"));
            inputFour.setText(savedInstanceState.getString("four"));
            inputFive.setText(savedInstanceState.getString("five"));
            inputSix.setText(savedInstanceState.getString("six"));
            secretOtp = savedInstanceState.getString("secretCode");
            userBundle = savedInstanceState.getBundle("user");

        }
        else {
            secretOtp = Utils.getOtp();
            userBundle = getIntent().getExtras();
            presenter.triggerOtp(this ,secretOtp);
        }

        btnVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.hideKeyBoard(OtpActivity.this);
                String code = inputOne.getText().toString().trim()
                        + inputTwo.getText().toString().trim()
                        + inputThree.getText().toString().trim()
                        + inputFour.getText().toString().trim()
                        + inputFive.getText().toString().trim()
                        + inputSix.getText().toString().trim();
                presenter.validateCode(code,secretOtp);
            }
        });

    }

    private void setListener() {
        inputOne.addTextChangedListener(new ValidatorTextWatcher(this, inputOne, inputTwo));
        inputTwo.addTextChangedListener(new ValidatorTextWatcher(this, inputTwo, inputThree));
        inputThree.addTextChangedListener(new ValidatorTextWatcher(this, inputThree, inputFour));
        inputFour.addTextChangedListener(new ValidatorTextWatcher(this, inputFour, inputFive));
        inputFive.addTextChangedListener(new ValidatorTextWatcher(this, inputFive, inputSix));
        inputSix.addTextChangedListener(new ValidatorTextWatcher(this, inputSix, null));

        inputOne.setOnKeyListener(new KeyListener(this, null, inputOne));
        inputTwo.setOnKeyListener(new KeyListener(this, inputOne, inputTwo));
        inputThree.setOnKeyListener(new KeyListener(this, inputTwo, inputThree));
        inputFour.setOnKeyListener(new KeyListener(this, inputThree, inputFour));
        inputFive.setOnKeyListener(new KeyListener(this, inputFour, inputFive));
        inputSix.setOnKeyListener(new KeyListener(this, inputFive, inputSix));
        Utils.requestFocus(this, inputOne);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("one" ,inputOne.getText().toString());
        outState.putString("two" ,inputTwo.getText().toString());
        outState.putString("three" ,inputThree.getText().toString());
        outState.putString("four" ,inputFour.getText().toString());
        outState.putString("five" ,inputFive.getText().toString());
        outState.putString("six" ,inputSix.getText().toString());
        outState.putString("secretCode" ,secretOtp);
        outState.putBundle("user" ,getIntent().getExtras());
    }

    @Override
    public void showLoader() {
        showProgressDialog();
    }

    @Override
    public void hideLoader() {
        hideProgressDialog();
    }

    @Override
    public void noInternet() {
        Toast.makeText(this, getString(R.string.noInternet), Toast.LENGTH_LONG).show();
    }

    @Override
    public void launchHome() {
        hideProgressDialog();
        if(userBundle != null){
            User user = userBundle.getParcelable("user");
            if(user != null){
                Intent intent = null;
                    if(userBundle.getBoolean("isFromLogin" ,true)){
                    PreferenceManager.setUserInfo(user ,this);
                    intent = new Intent(OtpActivity.this , HomeActivity.class);
                }
                else {
                    user.setUid(UUID.randomUUID().toString());
                    if(AppController.getInstance().getDataBaseHelper().createUser(user)){
                        PreferenceManager.setUserInfo(user ,this);
                    }
                        intent = new Intent(OtpActivity.this , SuggestionListActivity.class);
                }
                intent.putExtra("isFromSuggestion",true);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        }
    }

    @Override
    public void showInvalidOtp() {
        Toast.makeText(this ,getString(R.string.invalidOtpNumber) ,Toast.LENGTH_SHORT).show();
    }
}

