package com.example.e_commerce.ui.product.paymentMode;

import com.example.e_commerce.ui.BaseInteractor;

public interface PaymentModeInteractor extends BaseInteractor {
    @Override
    void showLoader();

    @Override
    void hideLoader();

    @Override
    void noInternet();

    void launchNextScreen();

    void showFieldError(int code);
}
