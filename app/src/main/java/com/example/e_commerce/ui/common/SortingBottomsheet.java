package com.example.e_commerce.ui.common;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.e_commerce.R;
import com.example.e_commerce.model.entity.Product;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import static android.app.Activity.RESULT_OK;

public class SortingBottomsheet extends BottomSheetDialogFragment {

    ImageView actionClose;
    RadioGroup radioGroup;
    RadioButton rdLowHigh,rdHighLow;
    FrameLayout actionApply;
    String sort;
    public CallBack callBack;
    public interface CallBack{
        void onResult(String sort);
        void onResult(Product product);
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_sort, container, false);
        actionClose = rootView.findViewById(R.id.actionClose);
        radioGroup = rootView.findViewById(R.id.radioGroup);
        rdLowHigh = rootView.findViewById(R.id.rd_lowHigh);
        rdHighLow = rootView.findViewById(R.id.rd_highLow);
        actionApply = rootView.findViewById(R.id.actionApply);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(checkedId == R.id.rd_lowHigh){
                    rdLowHigh.setChecked(true);
                    sort = "L";
                }
                else {
                    rdHighLow.setChecked(true);
                    sort = "H";
                }
            }
        });
        rdLowHigh.setChecked(true);

        actionApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBack.onResult(sort);
                dismiss();
            }
        });
        actionClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        return rootView;
    }

    public void setListener(CallBack callBack){
        this.callBack = callBack;
    }
    @Override
    public void dismiss() {
        super.dismiss();
        setRetainInstance(false);
    }

    @Override
    public void onDestroyView() {
        if (getDialog() != null && getRetainInstance())
            getDialog().setDismissMessage(null);
        super.onDestroyView();

    }

    @Override
    public void onCancel(@NonNull DialogInterface dialog) {
        super.onCancel(dialog);
        setRetainInstance(false);
    }
}
