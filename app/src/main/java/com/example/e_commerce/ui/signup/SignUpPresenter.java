package com.example.e_commerce.ui.signup;

import android.content.Context;
import android.text.TextUtils;

import com.example.e_commerce.common.Utils;

public class SignUpPresenter {
    private SignUpInteractor interactor;
    public SignUpHelper signUpHelper;
    SignUpPresenter(SignUpInteractor interactor) {
        this.interactor = interactor;
    }

    public void userSignUp(Context context ,String mobile, String name , String email){
        if(isValid(mobile ,name ,email)){
            if(Utils.isConnectedToInternet(context)){
                interactor.showLoader();
                signUpHelper.checkUser("+91"+mobile);
            }
            else {
                interactor.noInternet();
            }
        }
    }

    public void setSignUpHelper(SignUpHelper signUpHelper){
        this.signUpHelper = signUpHelper;
    }
    private boolean isValid(String mobile,String name ,String email){
        boolean isValid = true;
        if(TextUtils.isEmpty(mobile)){
            isValid = false;
            interactor.showMobileFieldError(true);
        }
        else if(mobile.length()!=10){
            isValid = false;
            interactor.showMobileFieldError(false);
        }
        else if(TextUtils.isEmpty(name)){
            isValid = false;
            interactor.showNameFieldError();
        }
        else if(TextUtils.isEmpty(email)){
            isValid = false;
            interactor.showEmailFieldError(true);
        }
        else if(!Utils.validateEmail(email)){
            isValid = false;
            interactor.showEmailFieldError(false);
        }

        return isValid;
    }
    public void onDestroy() {
        interactor = null;
    }
}
