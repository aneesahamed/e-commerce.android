package com.example.e_commerce.ui.productVariant;

import com.example.e_commerce.model.entity.Product;

public interface VariantInteractor {
    void updateUI(Product product);
}
