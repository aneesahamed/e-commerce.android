package com.example.e_commerce.ui.product.wishList;

import android.os.AsyncTask;

import com.example.e_commerce.common.AppController;
import com.example.e_commerce.model.entity.Product;

import java.util.List;

public class WishListPresenter {
    private WishListInteractor interactor;
    WishListPresenter(WishListInteractor interactor){
        this.interactor = interactor;
    }

    public void getWishList(String id){
        new GetWishList(interactor).execute(id);
    }

    public static class GetWishList extends AsyncTask<String, Void, List<Product>> {
        private WishListInteractor interactor;

        GetWishList(WishListInteractor interactor) {
            this.interactor = interactor;
        }

        @Override
        protected List<Product> doInBackground(String... id) {
            return AppController.getInstance().getDataBaseHelper().getWishList(id[0]);
        }

        @Override
        protected void onPostExecute(List<Product> list) {
            if(interactor != null){
                interactor.onResult(list);
            }
        }
    }

    public void onDestroy(){
        interactor = null;
    }
}
