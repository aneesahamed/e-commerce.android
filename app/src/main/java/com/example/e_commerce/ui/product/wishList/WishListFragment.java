package com.example.e_commerce.ui.product.wishList;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.e_commerce.R;
import com.example.e_commerce.adapter.CartListAdapter;
import com.example.e_commerce.adapter.WishListAdapter;
import com.example.e_commerce.common.AppController;
import com.example.e_commerce.common.PreferenceManager;
import com.example.e_commerce.common.Utils;
import com.example.e_commerce.common.imageUtils.ImageFetcher;
import com.example.e_commerce.model.entity.Product;
import com.example.e_commerce.model.entity.User;
import com.example.e_commerce.ui.product.ProductHomeListener;
import com.example.e_commerce.ui.product.productDetail.ProductDetailFragment;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class WishListFragment extends Fragment implements WishListInteractor {


    private ProgressBar progressBar;
    private RecyclerView wishListRecycler;
    private List<Product> wishList = new ArrayList<>();
    private WishListPresenter presenter;
    private WishListAdapter adapter;
    TextView txtEmptyState;
    private ProductHomeListener mListener;
    User user;
    private ImageFetcher imageFetcher;
    public WishListFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_wish_list, container, false);
        progressBar = view.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        txtEmptyState = view.findViewById(R.id.txtEmptyState);
        wishListRecycler = view.findViewById(R.id.wishListRecycler);
        presenter = new WishListPresenter(this);
        user = PreferenceManager.getUserInfo(getActivity());
        imageFetcher = Utils.getImageFetcher(getActivity() ,true);
        wishListRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new WishListAdapter(wishList, getActivity(), imageFetcher, new CartListAdapter.CartListener() {
            @Override
            public void onClick(View view, int position, Object object) {
                Product product = (Product) object;
                if (view.getId() == R.id.actionRemove) {
                    if (product != null) {
                        boolean success = AppController.getInstance().getDataBaseHelper().updateWishList(user.getUid() ,product.getpId() ,1,false);
                        if (success) {
                            wishList.remove(position);
                            adapter.notifyItemRemoved(position);
                            adapter.notifyItemRangeChanged(position, wishList.size() - position);
                            txtEmptyState.setVisibility(wishList.size() > 0 ? View.GONE : View.VISIBLE);
                        }
                    }
                } else if (view.getId() == R.id.actionBuy) {
                    if (product != null) {
                        mListener.navigate(ProductDetailFragment.class.getSimpleName(), product);
                    }
                }
            }
        });

        wishListRecycler.setAdapter(adapter);
        presenter.getWishList(user.getUid());
        return view;
    }

    @Override
    public void showLoader() {

    }

    @Override
    public void hideLoader() {

    }

    @Override
    public void noInternet() {

    }

    @Override
    public void onResult(List<Product> list) {
        if (list != null && list.size() > 0) {
            txtEmptyState.setVisibility(View.INVISIBLE);
            wishList.clear();
            wishList.addAll(list);
            adapter.notifyDataSetChanged();
        } else {
            txtEmptyState.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ProductHomeListener) {
            mListener = (ProductHomeListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


}
