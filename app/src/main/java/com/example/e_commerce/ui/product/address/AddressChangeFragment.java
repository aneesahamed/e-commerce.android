package com.example.e_commerce.ui.product.address;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.e_commerce.R;
import com.example.e_commerce.common.AppConstant;
import com.example.e_commerce.model.entity.Address;
import com.example.e_commerce.ui.product.ProductHomeListener;

import java.util.List;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddressChangeFragment extends Fragment implements AddressChangeInteractor {


    EditText edFirst,edLast,edDoorNo,edArea,edCity,edState,edAlt ,edPincode;
    RadioGroup radioGroup;
    RadioButton rdHome,rdWork;
    Address address;
    boolean isTaskRunning;
    String addressType;
    boolean isForAdd;
    public ProductHomeListener mListener;
    private AddressChangePresenter presenter;
    public AddressChangeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_address_change, container, false);
        edFirst = view.findViewById(R.id.edFirst);
        edLast = view.findViewById(R.id.edLast);
        edDoorNo = view.findViewById(R.id.edDoorNo);
        edArea = view.findViewById(R.id.edArea);
        edCity = view.findViewById(R.id.edCity);
        edState = view.findViewById(R.id.edState);
        edAlt = view.findViewById(R.id.edAlt);
        edPincode = view.findViewById(R.id.edPinCode);
        radioGroup = view.findViewById(R.id.radioGroups);
        rdHome = view.findViewById(R.id.rdHome);
        rdWork = view.findViewById(R.id.rdWork);
        presenter = new AddressChangePresenter(this);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int id) {
                switch (id){
                    case R.id.rdHome:
                        addressType = "H";
                        break;
                    case R.id.rdWork:
                        addressType = "W";
                        break;
                }
            }
        });

        if(savedInstanceState == null){
            if(getArguments().containsKey("isForAdd")){
                isForAdd = getArguments().getBoolean("isForAdd");
                if(!getArguments().getBoolean("isForAdd")){
                    address = getArguments().getParcelable("address");
                    if(address != null){
                        populateData(address);
                    }
                }
                else {
                    rdHome.setChecked(true);
                }
            }
        }
        return view;
    }

    public void saveAddress(){
        isTaskRunning = true;
        if(presenter != null){
            presenter.saveAddress(getContext(),
                    isForAdd,isForAdd? AppConstant.EMPTY:address.getAddressId(),
                    edFirst.getText().toString(),
                    edLast.getText().toString(),
                    edDoorNo.getText().toString(),
                    edArea.getText().toString(),
                    edCity.getText().toString(),
                    edState.getText().toString(),
                    edAlt.getText().toString(),
                    edPincode.getText().toString(),
                    addressType);
        }

    }
    private void populateData(Address address){
        edFirst.setText(address.getFirstName());
        edLast.setText(address.getLastName());
        edDoorNo.setText(address.getDoorNo());
        edArea.setText(address.getArea());
        edCity.setText(address.getCity());
        edState.setText(address.getState());
        edAlt.setText(address.getAltNo());
        edPincode.setText(address.getPinCode());

        if(address.getAddressType().equals("H")){
          rdHome.setChecked(true);
          rdWork.setChecked(false);
        }
        else {
            rdHome.setChecked(false);
            rdWork.setChecked(true);
        }
    }

    @Override
    public void showLoader() {

    }

    @Override
    public void hideLoader() {

    }

    @Override
    public void noInternet() {

    }

    @Override
    public void showAddressList(List<Address> list) {

    }

    @Override
    public void onSaveFinished() {
        setRetainInstance(false);
        getActivity().getSupportFragmentManager().popBackStack();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ProductHomeListener) {
            mListener = (ProductHomeListener) context;
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(getActivity() != null){
            getActivity().setTitle(getString(R.string.address));
        }
        if (mListener != null) {
            if (isTaskRunning) {
                mListener.showLoader();
            }
        }
    }

    @Override
    public void showFieldError(int error) {
        switch (error){
            case 0:
                edFirst.setError(getString(R.string.required));
                break;
            case 1:
                edLast.setError(getString(R.string.required));
                break;
            case 2:
                edDoorNo.setError(getString(R.string.required));
                break;
            case 3:
                edArea.setError(getString(R.string.required));
                break;
            case 4:
                edCity.setError(getString(R.string.required));
                break;

            case 5:
                edState.setError(getString(R.string.required));
                break;
            case 6:
                edAlt.setError(getString(R.string.required));
                break;
            case 7:
                edPincode.setError(getString(R.string.required));
                break;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener.hideLoader();
        mListener = null;
    }

}
