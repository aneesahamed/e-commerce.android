package com.example.e_commerce.ui.common;

import com.example.e_commerce.ui.BaseInteractor;

public interface HomeFragListener extends BaseInteractor {
    @Override
    void hideLoader();

    @Override
    void showLoader();

    @Override
    void noInternet();
}
