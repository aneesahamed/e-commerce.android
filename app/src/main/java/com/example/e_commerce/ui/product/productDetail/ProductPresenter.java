package com.example.e_commerce.ui.product.productDetail;

import com.example.e_commerce.common.AppController;
import com.example.e_commerce.common.imageUtils.AsyncTask;
import com.example.e_commerce.model.entity.Product;

import java.util.List;

public class ProductPresenter {
    private ProductInteractor interactor;

    ProductPresenter(ProductInteractor interactor) {
        this.interactor = interactor;
    }

    public void getProductDetail(String productId) {
        new GetProductDetail(interactor).execute(productId);
    }

    public void getRelatedProduct(int pagination ,String id ,boolean isLoadMore){
        new GetRelatedProduct(interactor ,isLoadMore ,id).execute(pagination);
    }

    private static class GetRelatedProduct extends AsyncTask<Integer, Void, List<Product>> {
        private ProductInteractor interactor;
        private boolean isLoadMore;
        private String pId;
        GetRelatedProduct(ProductInteractor interactor ,boolean isLoadMore ,String pId) {
            this.interactor = interactor;
            this.isLoadMore = isLoadMore;
            this.pId = pId;
        }

        @Override
        protected List<Product> doInBackground(Integer... id) {
            return AppController.getInstance().getDataBaseHelper().getRelatedProduct( pId,id[0]);
        }

        @Override
        protected void onPostExecute(List<Product> list) {
            if (interactor != null) {
                interactor.updateRelatedProducts(list ,isLoadMore);
            }
        }
    }

    private static class GetProductDetail extends AsyncTask<String, Void, Product> {
        private ProductInteractor interactor;

        GetProductDetail(ProductInteractor interactor) {
            this.interactor = interactor;
        }

        @Override
        protected Product doInBackground(String... id) {
            return AppController.getInstance().getDataBaseHelper().getProductInfo(id[0]);
        }

        @Override
        protected void onPostExecute(Product product) {
            if (interactor != null) {
                if (product != null) {
                    interactor.updateUI(product);
                } else {
                    interactor.showError();
                }
            }
        }
    }
}
