package com.example.e_commerce.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import android.app.ProgressDialog;
import android.os.Bundle;

import com.example.e_commerce.R;
import com.example.e_commerce.common.AppConstant;
import com.example.e_commerce.common.AppController;
import com.example.e_commerce.ui.Order.myorder.MyOrderList;
import com.example.e_commerce.ui.cart.CartListFragment;
import com.example.e_commerce.ui.main.MainFragment;
import com.example.e_commerce.ui.product.SuccessPage.SuccessFragment;
import com.example.e_commerce.ui.product.address.AddressChangeFragment;
import com.example.e_commerce.ui.product.address.ChooseAddressFragment;
import com.example.e_commerce.ui.product.coupons.CouponsListFrag;
import com.example.e_commerce.ui.product.deliverDetail.DeliverFragment;
import com.example.e_commerce.ui.product.payment.PaymentFragment;
import com.example.e_commerce.ui.product.paymentMode.PaymentModeFrag;
import com.example.e_commerce.ui.product.wishList.WishListFragment;
import com.example.e_commerce.ui.profile.ProfileFragment;

public class BaseActivity extends AppCompatActivity {

    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void showProgressDialog(){
        if(progressDialog == null){
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(getString(R.string.pleaseWait));
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
    }

    public void hideProgressDialog(){
        if(progressDialog != null){
            progressDialog.hide();
            progressDialog.dismiss();
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(progressDialog != null){
            progressDialog.hide();
            progressDialog.dismiss();
        }
        //AppController.getInstance().getDataBaseHelper().closeConnection();
    }
}
