package com.example.e_commerce.ui.Order;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.e_commerce.R;
import com.example.e_commerce.common.AppConstant;
import com.example.e_commerce.model.entity.Coupon;
import com.example.e_commerce.model.entity.Order;
import com.example.e_commerce.model.entity.Product;
import com.example.e_commerce.model.entity.Review;
import com.example.e_commerce.ui.BaseActivity;
import com.example.e_commerce.ui.Order.myorder.MyOrderList;
import com.example.e_commerce.ui.Order.orderDetail.OrderDetailFragment;
import com.example.e_commerce.ui.Order.review.ReviewFragment;
import com.example.e_commerce.ui.cart.CartListFragment;
import com.example.e_commerce.ui.main.MainFragment;
import com.example.e_commerce.ui.product.ProductHomeListener;
import com.example.e_commerce.ui.product.SuccessPage.SuccessFragment;
import com.example.e_commerce.ui.product.address.AddressChangeFragment;
import com.example.e_commerce.ui.product.address.ChooseAddressFragment;
import com.example.e_commerce.ui.product.coupons.CouponsListFrag;
import com.example.e_commerce.ui.product.deliverDetail.DeliverFragment;
import com.example.e_commerce.ui.product.payment.PaymentFragment;
import com.example.e_commerce.ui.product.paymentMode.PaymentModeFrag;
import com.example.e_commerce.ui.product.wishList.WishListFragment;
import com.example.e_commerce.ui.profile.ProfileFragment;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

public class OrderActivity extends BaseActivity implements ProductHomeListener {

    Toolbar toolbar;
    private String tag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        if(savedInstanceState == null){
            setInitialFragment();
        }
        else {
            tag = savedInstanceState.getString("tag");
            setTitle(tag);
            invalidateOptionsMenu();
        }
        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {

                for (Fragment fragment : getSupportFragmentManager().getFragments()) {
                    if (fragment != null && fragment.isVisible()) {
                        tag = fragment.getTag();
                    }
                }
                setTitle(tag);
                invalidateOptionsMenu();
            }
        });

    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("tag" ,tag);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_items, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.action_attachment).setVisible(false);
        menu.findItem(R.id.action_review_done).setVisible(false);
        if (tag.equals(ReviewFragment.class.getSimpleName())) {
            menu.findItem(R.id.action_attachment).setVisible(true);
            menu.findItem(R.id.action_review_done).setVisible(true);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        ReviewFragment reviewFragment = (ReviewFragment) getSupportFragmentManager().findFragmentByTag(ReviewFragment.class.getSimpleName());
        if (item.getItemId() == R.id.action_attachment) {
            if (reviewFragment != null && reviewFragment.isVisible()) {
                reviewFragment.openDialogChooser();
            }
        } else if (item.getItemId() == R.id.action_review_done) {
            if (reviewFragment != null && reviewFragment.isVisible()) {
                reviewFragment.updateReview();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void navigate(String tag, Object object) {
        this.tag = tag;
        if(tag.equals(ReviewFragment.class.getSimpleName())){
            ReviewFragment reviewFragment = new ReviewFragment();
            Bundle args = new Bundle();
            Order order = (Order)object;
            args.putParcelable("order" ,order);
            reviewFragment.setArguments(args);
            getSupportFragmentManager().beginTransaction().replace(R.id.fragContainer ,reviewFragment ,ReviewFragment.class.getSimpleName()).addToBackStack(null).commit();
        }
        else if(tag.equals(OrderDetailFragment.class.getSimpleName())){
            OrderDetailFragment orderDetailFragment = new OrderDetailFragment();
            Bundle args = new Bundle();
            Order order = (Order)object;
            args.putParcelable("order" ,order);
            orderDetailFragment.setArguments(args);
            getSupportFragmentManager().beginTransaction().replace(R.id.fragContainer ,orderDetailFragment ,OrderDetailFragment.class.getSimpleName()).addToBackStack(null).commit();

        }
    }

    @Override
    public void updateCoupons(Coupon coupon) {

    }

    @Override
    public void showLoader() {

    }

    @Override
    public void hideLoader() {

    }


    @Override
    public void setProduct(Product product) {

    }

    @Override
    public void noInternet() {

    }

    @Override
    public void openDialogVariant() {

    }

    private void setInitialFragment() {
        MyOrderList orderList = (MyOrderList) getSupportFragmentManager().findFragmentByTag(MyOrderList.class.getSimpleName());
        if (orderList == null) {
            orderList = new MyOrderList();
            getSupportFragmentManager().beginTransaction().add(R.id.fragContainer, orderList, MyOrderList.class.getSimpleName()).commit();
            tag = MyOrderList.class.getSimpleName();
        }
    }

    public void removeRetainedFragment() {
        Fragment myOrder = getSupportFragmentManager().findFragmentByTag(MyOrderList.class.getSimpleName());
        Fragment reviewFrag = getSupportFragmentManager().findFragmentByTag(ReviewFragment.class.getSimpleName());
        Fragment orderDetail = getSupportFragmentManager().findFragmentByTag(OrderDetailFragment.class.getSimpleName());
         if (myOrder != null && myOrder.isVisible()) {
            myOrder.setRetainInstance(false);
        }
        else if (orderDetail != null && orderDetail.isVisible()) {
            orderDetail.setRetainInstance(false);
        }
         else if (reviewFrag != null && reviewFrag.isVisible()) {
             reviewFrag.setRetainInstance(false);
         }
    }

    public void setTitle(String tag) {
        String title = AppConstant.EMPTY;
        if (tag.equals(MyOrderList.class.getSimpleName())) {
            title = getString(R.string.myOrder);
        }
        else if (tag.equals(OrderDetailFragment.class.getSimpleName())) {
            title = getString(R.string.orders);
        }
        else if (tag.equals(ReviewFragment.class.getSimpleName())) {
            title = getString(R.string.review);
        }
        if (toolbar != null) {
            toolbar.setTitle(title);
        }
    }
    @Override
    public void onBackPressed() {
        removeRetainedFragment();
        super.onBackPressed();
    }
}
