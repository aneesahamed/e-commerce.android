package com.example.e_commerce.ui.main;

import com.example.e_commerce.model.entity.HomeFeed;

public interface MainInteractor {

    void noInternet();

    void updateUI(HomeFeed homeFeed ,boolean isLoadMore);

    void showError();
}
