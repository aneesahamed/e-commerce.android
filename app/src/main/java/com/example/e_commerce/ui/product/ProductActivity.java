package com.example.e_commerce.ui.product;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.e_commerce.R;
import com.example.e_commerce.common.AppConstant;
import com.example.e_commerce.common.AppController;
import com.example.e_commerce.common.PreferenceManager;
import com.example.e_commerce.model.entity.Coupon;
import com.example.e_commerce.model.entity.Product;
import com.example.e_commerce.model.entity.User;
import com.example.e_commerce.ui.BaseActivity;
import com.example.e_commerce.ui.cart.CartListFragment;
import com.example.e_commerce.ui.product.SuccessPage.SuccessFragment;
import com.example.e_commerce.ui.product.address.AddressChangeFragment;
import com.example.e_commerce.ui.product.address.ChooseAddressFragment;
import com.example.e_commerce.ui.product.coupons.CouponsListFrag;
import com.example.e_commerce.ui.product.deliverDetail.DeliverFragment;
import com.example.e_commerce.ui.product.payment.PaymentFragment;
import com.example.e_commerce.ui.product.paymentMode.PaymentModeFrag;
import com.example.e_commerce.ui.product.productDetail.ProductDetailFragment;
import com.example.e_commerce.ui.productVariant.DialogVariant;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

public class ProductActivity extends BaseActivity implements ProductHomeListener {

    private Product product;
    private String tag = AppConstant.EMPTY;
    public Toolbar toolbar;
    private boolean isProductInWishList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        if (savedInstanceState != null) {
            isProductInWishList = savedInstanceState.getBoolean("isProductInWishList");
            tag = savedInstanceState.getString("tag");
            invalidateOptionsMenu();
        } else {
            setInitialFrag(getIntent().getStringExtra("id"));
        }

        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {

               /* Fragment productFrag = getSupportFragmentManager().findFragmentByTag(ProductDetailFragment.class.getSimpleName());
                Fragment deliverFrag = getSupportFragmentManager().findFragmentByTag(DeliverFragment.class.getSimpleName());
                Fragment addressChangeFrag = getSupportFragmentManager().findFragmentByTag(AddressChangeFragment.class.getSimpleName());
                Fragment chooseFrag = getSupportFragmentManager().findFragmentByTag(ChooseAddressFragment.class.getSimpleName());
                Fragment paymentFrag = getSupportFragmentManager().findFragmentByTag(PaymentFragment.class.getSimpleName());
                if(productFrag!=null && productFrag.isVisible()){
                    tag = ProductDetailFragment.class.getSimpleName();
                }
                else if(deliverFrag != null && deliverFrag.isVisible()) {
                    tag = DeliverFragment.class.getSimpleName();
                }
                else if(addressChangeFrag != null && addressChangeFrag.isVisible()) {
                    tag = AddressChangeFragment.class.getSimpleName();
                }
                else if(chooseFrag != null && chooseFrag.isVisible()){
                    tag = ChooseAddressFragment.class.getSimpleName();
                }
                else if(paymentFrag != null && paymentFrag.isVisible()){
                    tag = PaymentFragment.class.getSimpleName();
                }
                else {
                    tag = AppConstant.EMPTY;
                }*/
                for (Fragment fragment : getSupportFragmentManager().getFragments()) {
                    if (fragment != null && fragment.isVisible()) {
                        tag = fragment.getTag();
                    }
                }
                setTitle(tag);
                invalidateOptionsMenu();
            }
        });
    }

    private void setInitialFrag(String id) {
        Bundle bundle = new Bundle();
        bundle.putString("id", id);
        ProductDetailFragment productDetailFragment = new ProductDetailFragment();
        productDetailFragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().add(R.id.fragContainer, productDetailFragment, ProductDetailFragment.class.getSimpleName()).commit();
        tag = ProductDetailFragment.class.getSimpleName();
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("isProductInWishList", isProductInWishList);
        outState.putString("tag", tag);
    }

    @Override
    public void showLoader() {
        showProgressDialog();
    }

    @Override
    public void hideLoader() {
        hideProgressDialog();
    }

    @Override
    public void noInternet() {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_items, menu);
        return true;

    }

    @Override
    public void openDialogVariant() {
        DialogVariant dialogVariant = (DialogVariant) getSupportFragmentManager().findFragmentByTag(DialogVariant.class.getSimpleName());
        if (dialogVariant == null) {
            dialogVariant = new DialogVariant();
            Bundle bundle = new Bundle();
            bundle.putString("pId", product.getpId());
            dialogVariant.setArguments(bundle);
            dialogVariant.show(getSupportFragmentManager(), DialogVariant.class.getSimpleName());
        } else {
            dialogVariant.show(getSupportFragmentManager(), DialogVariant.class.getSimpleName());
        }
    }

    @Override
    public void navigate(String tag, Object object) {
        this.tag = tag;
        if (tag.equals(DeliverFragment.class.getSimpleName())) {
            DeliverFragment deliverFragment = new DeliverFragment();
            Bundle args = new Bundle();
            args.putParcelable("product", product);
            deliverFragment.setArguments(args);
            getSupportFragmentManager().beginTransaction().replace(R.id.fragContainer, deliverFragment, DeliverFragment.class.getSimpleName()).addToBackStack(null).commit();
        } else if (tag.equals(AddressChangeFragment.class.getSimpleName())) {
            AddressChangeFragment addressFrag = new AddressChangeFragment();
            Bundle args = (Bundle) object;
            addressFrag.setArguments(args);
            getSupportFragmentManager().beginTransaction().replace(R.id.fragContainer, addressFrag, AddressChangeFragment.class.getSimpleName()).addToBackStack(null).commit();

        } else if (tag.equals(ChooseAddressFragment.class.getSimpleName())) {
            ChooseAddressFragment chooseAddressFragment = new ChooseAddressFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.fragContainer, chooseAddressFragment, ChooseAddressFragment.class.getSimpleName()).addToBackStack(null).commit();


        } else if (tag.equals(PaymentFragment.class.getSimpleName())) {
            PaymentFragment paymentFragment = new PaymentFragment();
            Bundle args = (Bundle) object;
            paymentFragment.setArguments(args);
            getSupportFragmentManager().beginTransaction().replace(R.id.fragContainer, paymentFragment, PaymentFragment.class.getSimpleName()).addToBackStack(null).commit();

        } else if (tag.equals(CouponsListFrag.class.getSimpleName())) {
            CouponsListFrag couponsListFrag = new CouponsListFrag();
            getSupportFragmentManager().beginTransaction().replace(R.id.fragContainer, couponsListFrag, CouponsListFrag.class.getSimpleName()).addToBackStack(null).commit();


        } else if (tag.equals(CartListFragment.class.getSimpleName())) {

            CartListFragment cartListFragment = new CartListFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.fragContainer, cartListFragment, CartListFragment.class.getSimpleName()).addToBackStack(null).commit();

        } else if (tag.equals(ProductDetailFragment.class.getSimpleName())) {
            Product product = (Product)object;
            CartListFragment cartListFragment = (CartListFragment) getSupportFragmentManager().findFragmentByTag(CartListFragment.class.getSimpleName());
            if(cartListFragment != null){
                cartListFragment.setRetainInstance(false);
                getSupportFragmentManager().popBackStackImmediate();
            }
            ProductDetailFragment productDetailFragment = (ProductDetailFragment)getSupportFragmentManager().findFragmentByTag(ProductDetailFragment.class.getSimpleName());
            if(productDetailFragment != null && productDetailFragment.isVisible()){
             productDetailFragment.setProduct(product.getpId());
            }
           /* Product product = (Product) object;
            Bundle bundle = new Bundle();
            bundle.putString("id", product.getpId());
            ProductDetailFragment productDetailFragment = new ProductDetailFragment();
            productDetailFragment.setArguments(bundle);
            getSupportFragmentManager().beginTransaction().add(R.id.fragContainer, productDetailFragment, ProductDetailFragment.class.getSimpleName()).addToBackStack(null).commit();
*/

        }
        else if (tag.equals(PaymentModeFrag.class.getSimpleName())) {
            PaymentModeFrag PaymentModeFrag = new PaymentModeFrag();
            Bundle args = (Bundle) object;
            PaymentModeFrag.setArguments(args);
            getSupportFragmentManager().beginTransaction().replace(R.id.fragContainer, PaymentModeFrag, PaymentModeFrag.class.getSimpleName()).addToBackStack(null).commit();

        }
        else if (tag.equals(SuccessFragment.class.getSimpleName())) {
            SuccessFragment successFragment = new SuccessFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.fragContainer, successFragment, SuccessFragment.class.getSimpleName()).addToBackStack(null).commit();

        }
        setTitle(tag);
        invalidateOptionsMenu();
    }

    @Override
    public void updateCoupons(Coupon coupon) {
        removeRetainedFragment();
        getSupportFragmentManager().popBackStackImmediate();
        PaymentFragment paymentfrag = (PaymentFragment) getSupportFragmentManager().findFragmentByTag(PaymentFragment.class.getSimpleName());
        if (paymentfrag != null) {
            paymentfrag.applyCoupons(coupon);
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (tag.equals(ProductDetailFragment.class.getSimpleName())) {
            if (isProductInWishList) {
                menu.findItem(R.id.action_wish_list).setVisible(false);
                menu.findItem(R.id.action_wish_list_done).setVisible(true);
            } else {
                menu.findItem(R.id.action_wish_list).setVisible(true);
                menu.findItem(R.id.action_wish_list_done).setVisible(false);
            }
            menu.findItem(R.id.action_share).setVisible(true);
        } else if (tag.equals(AddressChangeFragment.class.getSimpleName())) {
            menu.findItem(R.id.action_wish_list).setVisible(false);
            menu.findItem(R.id.action_wish_list_done).setVisible(false);
            menu.findItem(R.id.action_share).setVisible(false);
            menu.findItem(R.id.action_save).setVisible(true);
        } else {
            menu.findItem(R.id.action_wish_list).setVisible(false);
            menu.findItem(R.id.action_wish_list_done).setVisible(false);
            menu.findItem(R.id.action_share).setVisible(false);
            menu.findItem(R.id.action_save).setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_wish_list_done) {
            updateWishList(false);
        } else if (item.getItemId() == R.id.action_wish_list) {
            updateWishList(true);
        } else if (item.getItemId() == R.id.action_save) {
            AddressChangeFragment fragment = (AddressChangeFragment) getSupportFragmentManager().findFragmentByTag(AddressChangeFragment.class.getSimpleName());
            if (fragment != null && fragment.isVisible()) {
                fragment.saveAddress();
            }
        }
        else if(item.getItemId() == R.id.action_share){
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "");
            startActivity(Intent.createChooser(sharingIntent ,getString(R.string.shareItem)));
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateWishList(boolean isForAdd) {
        if (this.product != null) {
            User user = PreferenceManager.getUserInfo(this);
            AppController.getInstance().getDataBaseHelper().updateWishList(user.getUid(), product.getpId(), 1, isForAdd);
            isProductInWishList = isForAdd;
            invalidateOptionsMenu();
        }
        /*ProductDetailFragment fragment = (ProductDetailFragment) getSupportFragmentManager().findFragmentByTag(ProductDetailFragment.class.getSimpleName());
        if (fragment != null && fragment.isVisible()) {
            Product product = fragment.getProductForWish();

        }*/
    }

    @Override
    public void setProduct(Product product) {
        this.product = product;
        setTitle(tag);
        isProductInWishList = AppController.getInstance().getDataBaseHelper().isProductInWishList(PreferenceManager.getUserInfo(this).getUid(), product.getpId());
        invalidateOptionsMenu();
    }

    @Override
    public void onBackPressed() {
        removeRetainedFragment();
        if(tag.equals(SuccessFragment.class.getSimpleName())){
            finish();
        }
        else {
            super.onBackPressed();
        }
    }

    private void removeRetainedFragment() {
        Fragment productFrag = getSupportFragmentManager().findFragmentByTag(ProductDetailFragment.class.getSimpleName());
        Fragment deliverFrag = getSupportFragmentManager().findFragmentByTag(DeliverFragment.class.getSimpleName());
        Fragment addressChangeFrag = getSupportFragmentManager().findFragmentByTag(AddressChangeFragment.class.getSimpleName());
        Fragment chooseAddressFrag = getSupportFragmentManager().findFragmentByTag(ChooseAddressFragment.class.getSimpleName());
        Fragment paymentfrag = getSupportFragmentManager().findFragmentByTag(PaymentFragment.class.getSimpleName());
        Fragment couponsFrag = getSupportFragmentManager().findFragmentByTag(CouponsListFrag.class.getSimpleName());
        Fragment cartListFrag = getSupportFragmentManager().findFragmentByTag(CartListFragment.class.getSimpleName());
        Fragment paymentModeFrag = getSupportFragmentManager().findFragmentByTag(PaymentModeFrag.class.getSimpleName());
        Fragment successFrag = getSupportFragmentManager().findFragmentByTag(SuccessFragment.class.getSimpleName());
        if (chooseAddressFrag != null && chooseAddressFrag.isVisible()) {
            chooseAddressFrag.setRetainInstance(false);
        } else if (productFrag != null && productFrag.isVisible()) {
            productFrag.setRetainInstance(false);
        } else if (deliverFrag != null && deliverFrag.isVisible()) {
            deliverFrag.setRetainInstance(false);
        } else if (addressChangeFrag != null && addressChangeFrag.isVisible()) {
            addressChangeFrag.setRetainInstance(false);
        } else if (paymentfrag != null && paymentfrag.isVisible()) {
            paymentfrag.setRetainInstance(false);
        } else if (couponsFrag != null && couponsFrag.isVisible()) {
            couponsFrag.setRetainInstance(false);
        } else if (cartListFrag != null && cartListFrag.isVisible()) {
            cartListFrag.setRetainInstance(false);
        }
        else if (paymentModeFrag != null && paymentModeFrag.isVisible()) {
            paymentModeFrag.setRetainInstance(false);
        }
        else if (successFrag != null && successFrag.isVisible()) {
            successFrag.setRetainInstance(false);
        }
    }

    private void setTitle(String tag) {
        String title = AppConstant.EMPTY;
        if (tag.equals(ProductDetailFragment.class.getSimpleName())) {
            title = product.getName();
        } else if (tag.equals(DeliverFragment.class.getSimpleName())) {
            title = getString(R.string.delivery);
        } else if (tag.equals(AddressChangeFragment.class.getSimpleName())) {
            title = getString(R.string.address);
        } else if (tag.equals(ChooseAddressFragment.class.getSimpleName())) {
            title = getString(R.string.selectAddress);
        } else if (tag.equals(PaymentFragment.class.getSimpleName())) {
            title = getString(R.string.payment);
        } else if (tag.equals(CouponsListFrag.class.getSimpleName())) {
            title = getString(R.string.coupons);
        } else if (tag.equals(CartListFragment.class.getSimpleName())) {
            title = getString(R.string.myCart);
        }
        else if (tag.equals(PaymentModeFrag.class.getSimpleName())) {
            title = getString(R.string.payMode);
        }
        else if (tag.equals(SuccessFragment.class.getSimpleName())) {
            title = getString(R.string.orderPlaced);
        }
        if (toolbar != null) {
            toolbar.setTitle(title);
        }
    }
}
