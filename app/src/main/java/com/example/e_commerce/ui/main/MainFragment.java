package com.example.e_commerce.ui.main;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.e_commerce.R;
import com.example.e_commerce.adapter.DealAdapter;
import com.example.e_commerce.adapter.GeneralCategoryAdapter;
import com.example.e_commerce.adapter.ProductListAdapter;
import com.example.e_commerce.common.AppController;
import com.example.e_commerce.common.PreferenceManager;
import com.example.e_commerce.common.Utils;
import com.example.e_commerce.common.imageUtils.ImageFetcher;
import com.example.e_commerce.model.entity.GeneralCategory;
import com.example.e_commerce.model.entity.HomeFeed;
import com.example.e_commerce.model.entity.Product;
import com.example.e_commerce.model.entity.TrendsDeal;
import com.example.e_commerce.model.entity.User;
import com.example.e_commerce.ui.common.ClickListener;
import com.example.e_commerce.ui.common.CustomPagerAdapter;
import com.example.e_commerce.ui.common.HomeFragListener;
import com.example.e_commerce.ui.common.OnLoadMoreListener;
import com.example.e_commerce.ui.common.ViewPagerScroller;
import com.example.e_commerce.ui.product.ProductActivity;
import com.example.e_commerce.ui.product.ProductHomeListener;
import com.example.e_commerce.ui.product.coupons.CouponsListFrag;
import com.example.e_commerce.ui.product.payment.PaymentFragment;
import com.example.e_commerce.ui.product.productList.ProductHeadlessFragment;
import com.example.e_commerce.ui.product.productList.ProductListActivity;
import com.example.e_commerce.ui.suggestion.SuggestionListActivity;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import static com.example.e_commerce.model.DBConstant.TAG;

/**
 * A simple {@link Fragment} subclass.hom
 */
public class MainFragment extends Fragment implements MainInteractor {


    private ViewPager viewPager;
    private LinearLayout llDots;
    private boolean isTaskRunning;
    private MainPresenter presenter;
    private User user;
    private HomeFeed homeFeed;
    private ProductHomeListener mListener;
    private CustomPagerAdapter pagerAdapter;
    private GeneralCategoryAdapter generalCategoryAdapter;
    private DealAdapter dealAdapter;
    private ProductListAdapter productListAdapter;
    private ImageFetcher imageFetcher;
    private int currentPage;
    private int pagination;
    private boolean isPaginationEnd;
    private TextView[] dots;
    private RecyclerView generalCategoryRecycler, dealRecycler, productRecyclerView;
    private List<TrendsDeal> trendList = new ArrayList<>();
    private List<TrendsDeal> dealList = new ArrayList<>();
    private List<GeneralCategory> generalCategoryList = new ArrayList<>();
    private List<Product> productList = new ArrayList<>();
    private Handler handler;
    private Runnable runnable;
    private ProgressBar progressBar;
    private NestedScrollView nestedScrollView;
    private Timer timer;
    private final Object lock = new Object();

    public MainFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        nestedScrollView = view.findViewById(R.id.nestedScrollView);
        viewPager = view.findViewById(R.id.viewPager);
        changePagerScroller();
        generalCategoryRecycler = view.findViewById(R.id.generalCategoryRecycler);
        dealRecycler = view.findViewById(R.id.dealRecycler);
        productRecyclerView = view.findViewById(R.id.productRecyclerView);
        llDots = view.findViewById(R.id.llDots);
        progressBar = view.findViewById(R.id.progressBar);
        user = PreferenceManager.getUserInfo(getActivity());
        imageFetcher = Utils.getImageFetcher(getActivity(), true);
        pagerAdapter = new CustomPagerAdapter(getActivity(), trendList, imageFetcher);
        pagerAdapter.setListener(new CustomPagerAdapter.SliderListener() {
            @Override
            public void onClick(TrendsDeal trendsDeal) {
                if (trendsDeal != null) {
                    if (trendsDeal.getOfferProduct().equals("P")) {
                        Intent intent = new Intent(getContext(), ProductActivity.class);
                        intent.putExtra("id", trendsDeal.getpId());
                        startActivity(intent);
                    }
                    else {
                        Intent intent = new Intent(getContext() , ProductListActivity.class);
                        intent.putExtra("isFromSearch" ,false);
                        intent.putExtra("cid" ,trendsDeal.getcId());
                        startActivity(intent);
                    }
                }
            }
        });
        viewPager.setAdapter(pagerAdapter);
        user.setSelecedIds(AppController.getInstance().getDataBaseHelper().getUserInterest(user.getUid()));
        presenter = new MainPresenter(this);
        generalCategoryAdapter = new GeneralCategoryAdapter(getActivity(), imageFetcher, generalCategoryList, new ClickListener() {
            @Override
            public void onItemClick(Object object) {
                GeneralCategory generalCategory = (GeneralCategory)object;
                if(generalCategory != null){
                    Intent intent = new Intent(getContext() , SuggestionListActivity.class);
                    intent.putExtra("gcId" ,generalCategory.getGcId());
                    startActivity(intent);
                }
            }
        });
        generalCategoryRecycler.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        generalCategoryRecycler.setAdapter(generalCategoryAdapter);
        dealAdapter = new DealAdapter(getActivity(), imageFetcher, dealList, new ClickListener() {
            @Override
            public void onItemClick(Object object) {
                TrendsDeal trendsDeal = (TrendsDeal) object;
                if (trendsDeal != null) {
                    if (trendsDeal.getOfferProduct().equals("P")) {
                        Intent intent = new Intent(getContext(), ProductActivity.class);
                        intent.putExtra("id", trendsDeal.getpId());
                        startActivity(intent);
                    }
                    else {
                        Intent intent = new Intent(getContext() , ProductListActivity.class);
                        intent.putExtra("isFromSearch" ,false);
                        intent.putExtra("cid" ,trendsDeal.getcId());
                        startActivity(intent);
                    }
                }
            }
        });
        dealRecycler.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        dealRecycler.setAdapter(dealAdapter);

        productRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        productListAdapter = new ProductListAdapter(getActivity(), imageFetcher, productList, productRecyclerView, new ClickListener() {
            @Override
            public void onItemClick(Object object) {
                Product product = (Product) object;
                if (product != null) {
                    Intent intent = new Intent(getContext(), ProductActivity.class);
                    intent.putExtra("id", product.getpId());
                    startActivity(intent);
                }
            }
        });

        /*productListAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (!isPaginationEnd) {
                    initData(true);
                }
            }
        });*/
        productRecyclerView.setAdapter(productListAdapter);
        if (savedInstanceState == null) {
            progressBar.setVisibility(View.VISIBLE);
            nestedScrollView.setVisibility(View.GONE);
            initData(false);
        } else {
            progressBar.setVisibility(View.GONE);
            currentPage = savedInstanceState.getInt("currentViewPager");
            isTaskRunning = savedInstanceState.getBoolean("isTaskRunning");
            isPaginationEnd = savedInstanceState.getBoolean("isPaginationEnd");
            addBottomDots(currentPage, pagerAdapter.getCount());
            removeHandlerCallback();
            setAutoScrollPager();
            final int[] position = savedInstanceState.getIntArray("SCROLL_POSITION");
            if (position != null)
                nestedScrollView.post(new Runnable() {
                    public void run() {
                        nestedScrollView.scrollTo(position[0], position[1]);
                    }
                });
        }
        nestedScrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                View view = (View) nestedScrollView.getChildAt(nestedScrollView.getChildCount() - 1);

                int diff = (view.getBottom() - (nestedScrollView.getHeight() + nestedScrollView
                        .getScrollY()));

                if (diff == 0) {
                    // your pagination code
                    if (!isTaskRunning) {
                        if (!isPaginationEnd) {
                            isTaskRunning = true;
                            presenter.getHomeFeed(pagination, user, true);
                        }
                    }
                }
            }
        });
        return view;
    }

    private void addBottomDots(int currentPage, int length) {
       if(getActivity() != null){
           dots = new TextView[length];
           llDots.removeAllViews();
           for (int i = 0; i < dots.length; i++) {
               dots[i] = new TextView(getActivity());
               dots[i].setText(Html.fromHtml("&#8226;"));
               dots[i].setTextSize(35);
               dots[i].setTextColor(getResources().getColor(R.color.dot_inactive));
               llDots.addView(dots[i]);
           }

           if (dots.length > 0)
               dots[currentPage].setTextColor(getResources().getColor(R.color.dot_active));
       }
    }


    private void initData(boolean isLoadMore) {
        isTaskRunning = true;
        mListener.showLoader();
        pagination = 0;
        isPaginationEnd = false;
        presenter.getHomeFeed(pagination, user, isLoadMore);
    }

    @Override
    public void updateUI(HomeFeed homeFeed, boolean isLoadMore) {
        this.homeFeed = homeFeed;
        isTaskRunning = false;
        mListener.hideLoader();
        progressBar.setVisibility(View.GONE);
        nestedScrollView.setVisibility(View.VISIBLE);
        if (!isLoadMore) {
            setPagerAdapter(homeFeed.getTrendsDealList());
            setGeneralCategory(homeFeed.getGeneralCategoryList());
        }
        setProductList(homeFeed.getProductList(), isLoadMore);
    }

    private void setDeals(List<TrendsDeal> list) {
        dealList.clear();
        dealList.addAll(list);
        dealAdapter.notifyDataSetChanged();
    }

    private void setGeneralCategory(List<GeneralCategory> list) {
        generalCategoryList.addAll(list);
        generalCategoryAdapter.notifyDataSetChanged();
    }

    private void setPagerAdapter(List<TrendsDeal> list) {
        trendList.clear();
        trendList.addAll(list.size() > 3 ? list.subList(0, 3) : list);
        //pagerAdapter.setList(list.size()>3?list.subList(0 ,3):list);
        pagerAdapter.notifyDataSetChanged();
        currentPage = 0;
        addBottomDots(0, pagerAdapter.getCount());
        setAutoScrollPager();
        setDeals(list);

    }

    private void setAutoScrollPager() {
        viewPager.addOnPageChangeListener(pageChangeListener);
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                if (currentPage == pagerAdapter.getCount() - 1) {
                    currentPage = -1;
                }
                viewPager.setCurrentItem((currentPage == -1) ? 0 : currentPage + 1, true);
            }
        };
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(runnable);
            }
        }, 2500, 2500);

    }

    private void setProductList(List<Product> productList, boolean isLoadMore) {
        synchronized (lock) {
            if (productList != null && productList.size() > 0) {
                if (!isLoadMore) {
                    pagination = 1;
                    this.productList.clear();
                    this.productList.addAll(productList);
                    productListAdapter.notifyDataSetChanged();
                } else {
                    pagination = pagination + 1;
                    this.productList.addAll(productList);
                    productListAdapter.notifyDataSetChanged();
                    //productListAdapter.setLoaded();
                }
            } else {
                if (!isLoadMore) {

                } else {
                    isPaginationEnd = true;
                }
            }
        }

    }

    ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position, pagerAdapter.getCount());
            currentPage = position;
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    @Override
    public void showError() {

    }

    @Override
    public void noInternet() {

    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("currentViewPager", currentPage);
        outState.putIntArray("SCROLL_POSITION", new int[]{nestedScrollView.getScrollX(), nestedScrollView.getScrollY()});
        outState.putBoolean("isTaskRunning", isTaskRunning);
        outState.putBoolean("isPaginationEnd", isPaginationEnd);

    }

    private void removeHandlerCallback() {
        if (handler != null && timer != null) {
            handler.removeCallbacks(runnable);
            timer.cancel();
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (mListener != null) {
            if (isTaskRunning) {
                mListener.showLoader();
            }
        }
    }

    private void changePagerScroller() {
        try {
            Field mScroller = null;
            mScroller = ViewPager.class.getDeclaredField("mScroller");
            mScroller.setAccessible(true);
            ViewPagerScroller scroller = new ViewPagerScroller(viewPager.getContext());
            mScroller.set(viewPager, scroller);
        } catch (Exception e) {
            Log.e(TAG, "error of change scroller ", e);
        }
    }

   /* @Override
    public void onAttachFragment(@NonNull Fragment childFragment) {
        if (fragment instanceof Main) {
            HeadlinesFragment headlinesFragment = (HeadlinesFragment) fragment;
            headlinesFragment.setOnHeadlineSelectedListener(this);
        }
    }*/

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ProductHomeListener) {
            mListener = (ProductHomeListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener.hideLoader();
        mListener = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        removeHandlerCallback();
    }
}
