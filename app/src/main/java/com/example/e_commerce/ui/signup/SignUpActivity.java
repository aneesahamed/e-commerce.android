package com.example.e_commerce.ui.signup;

import android.content.Intent;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.e_commerce.BuildConfig;
import com.example.e_commerce.R;
import com.example.e_commerce.common.Utils;
import com.example.e_commerce.model.entity.User;
import com.example.e_commerce.ui.BaseActivity;
import com.example.e_commerce.ui.common.CustomEditText;
import com.example.e_commerce.ui.otp.OtpActivity;
import com.google.android.material.button.MaterialButton;

import androidx.appcompat.widget.Toolbar;

public class SignUpActivity extends BaseActivity implements SignUpInteractor, SignUpHelper.Callback {


    CustomEditText edMobile;
    SignUpPresenter signUpPresenter;
    EditText edEmail, edName;
    MaterialButton btnSignUp;
    SignUpHelper signUpHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        edMobile = findViewById(R.id.edMobileNumber);
        edName = findViewById(R.id.edName);
        edEmail = findViewById(R.id.edEmail);
        btnSignUp = findViewById(R.id.btnSignUp);
        TextView txtLogin = findViewById(R.id.txtLogin);
        Spannable text = new SpannableString(getString(R.string.havingAcctAlready));
        text.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorPrimary)), 27, 33, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        txtLogin.setText(text);
        signUpPresenter = new SignUpPresenter(this);
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.hideKeyBoard(SignUpActivity.this);
                signUpPresenter.userSignUp(SignUpActivity.this,
                        edMobile.getText().toString().trim(),
                        edName.getText().toString().trim(),
                        edEmail.getText().toString().trim());
            }
        });

        txtLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if (savedInstanceState != null) {
            edMobile.setText(savedInstanceState.getString("mobileNo"));
            edName.setText(savedInstanceState.getString("name"));
            edEmail.setText(savedInstanceState.getString("email"));
        }

        signUpHelper = (SignUpHelper) getSupportFragmentManager().findFragmentByTag(SignUpHelper.class.getSimpleName());
        if (signUpHelper == null) {
            signUpHelper = new SignUpHelper();
            signUpPresenter.setSignUpHelper(signUpHelper);
            getSupportFragmentManager().beginTransaction().add(signUpHelper, SignUpHelper.class.getSimpleName()).commit();

        } else {
            signUpPresenter.setSignUpHelper(signUpHelper);
        }
    }

    @Override
    public void result(User user) {
        hideProgressDialog();
        if (user == null) {
            user = new User();
            user.setName(edName.getText().toString().trim());
            user.setEmail(edEmail.getText().toString().trim());
            user.setMobileNo("+91" + edMobile.getText().toString().trim());
            Intent intent = new Intent(SignUpActivity.this, OtpActivity.class);
            intent.putExtra("isFromLogin", false);
            intent.putExtra("user", user);
            startActivity(intent);
        } else {
            Toast.makeText(this, getString(R.string.mobileNoExist), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("mobileNo", edMobile.getText().toString());
        outState.putString("name", edName.getText().toString());
        outState.putString("email", edEmail.getText().toString());
    }

    @Override
    public void showLoader() {
        showProgressDialog();
    }

    @Override
    public void hideLoader() {
        hideProgressDialog();
    }

    @Override
    public void showEmailFieldError(boolean isEmpty) {
        if (isEmpty) {
            edEmail.setError(getString(R.string.required));
            edEmail.setFocusable(true);
        } else {
            edEmail.setError(getString(R.string.invalidEmail));
            edEmail.setFocusable(true);
        }
    }

    @Override
    public void showMobileFieldError(boolean isEmpty) {
        if (isEmpty) {
            edMobile.setError(getString(R.string.required));
            edMobile.setFocusable(true);
        } else {
            edMobile.setError(getString(R.string.invalidNumber));
            edMobile.setFocusable(true);
        }
    }

    @Override
    public void showNameFieldError() {
        edName.setError(getString(R.string.required));
    }

    @Override
    public void launchNextScreen() {

    }

    @Override
    public void noInternet() {
        Toast.makeText(this, getString(R.string.noInternet), Toast.LENGTH_LONG).show();
    }


}
