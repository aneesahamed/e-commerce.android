package com.example.e_commerce.ui.common;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.e_commerce.R;
import com.example.e_commerce.common.AppController;
import com.example.e_commerce.model.entity.Product;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class FilterBottomSheet extends BottomSheetDialogFragment {

    SeekBar seekBar;
    Spinner colorSpinner, ramSpinner, storageSpinner, hertzSpinner;
    TextView titleRam, titleColor, titleStorage, titleHertz ,txtMin,txtMax;
    Product product;
    FrameLayout actionApply;
    ImageView actionClose;
    String cid;
    public SortingBottomsheet.CallBack callBack;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_filter_bottom_sheet, container, false);
        colorSpinner = rootView.findViewById(R.id.colorSpinner);
        actionClose = rootView.findViewById(R.id.actionClose);
        ramSpinner = rootView.findViewById(R.id.ramSpinner);
        storageSpinner = rootView.findViewById(R.id.storageSpinner);
        hertzSpinner = rootView.findViewById(R.id.hertzSpinner);
        txtMin = rootView.findViewById(R.id.txtMin);
        txtMax = rootView.findViewById(R.id.txtMax);
        actionApply = rootView.findViewById(R.id.actionApply);
        seekBar = rootView.findViewById(R.id.seekBar);
        titleColor = rootView.findViewById(R.id.titleColor);
        titleHertz = rootView.findViewById(R.id.titleHertz);
        titleRam = rootView.findViewById(R.id.titleRam);
        titleStorage = rootView.findViewById(R.id.titleStorage);

        actionClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                txtMin.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        if(savedInstanceState == null){
            cid = getArguments().getString("cid");
            new GetVariant(new GetVariant.Listener() {
                @Override
                public void onResult(Product product) {
                    setVariant(product);
                }
            }).execute(getArguments().getString("cid"));
        }
        else {
            setVariant(product);
            colorSpinner.setSelection(savedInstanceState.getInt("color"));
            ramSpinner.setSelection(savedInstanceState.getInt("ram"));
            storageSpinner.setSelection(savedInstanceState.getInt("storage"));
            hertzSpinner.setSelection(savedInstanceState.getInt("hertz"));

        }

        actionApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Product result = new Product();
                result.setcId(cid);
                if(product.getRamList() != null && product.getRamList().size() > 0){
                    result.setRam(product.getRamList().get(ramSpinner.getSelectedItemPosition()));
                }
                if(product.getColorList() != null && product.getColorList().size() > 0){
                    result.setColor(product.getColorList().get(colorSpinner.getSelectedItemPosition()));
                }
                if(product.getMemList() != null && product.getMemList().size() > 0){
                    result.setMemory(product.getMemList().get(storageSpinner.getSelectedItemPosition()));
                }
                if(product.getHertzList() != null && product.getHertzList().size() > 0){
                    result.setHertz(product.getHertzList().get(hertzSpinner.getSelectedItemPosition()));
                }

                product.setAmount(seekBar.getProgress());
                callBack.onResult(result);
                dismiss();
            }
        });
        return rootView;
    }

    public void setListener(SortingBottomsheet.CallBack callBack){
        this.callBack = callBack;
    }

    private void setVariant(Product product) {
        this.product = product;
        seekBar.setProgress(product.getMin());
        seekBar.setMax(product.getMax());
        txtMax.setText(String.valueOf(product.getMax()));
        txtMin.setText(String.valueOf(product.getMin()));
        if (product.getColorList() != null && product.getColorList().size()>0) {
            titleColor.setVisibility(View.VISIBLE);
            colorSpinner.setVisibility(View.VISIBLE);
        } else {
            titleColor.setVisibility(View.GONE);
            colorSpinner.setVisibility(View.GONE);
        }

        if (product.getRamList() != null && product.getRamList().size() > 0) {
            titleRam.setVisibility(View.VISIBLE);
            ramSpinner.setVisibility(View.VISIBLE);
        } else {
            titleRam.setVisibility(View.GONE);
            ramSpinner.setVisibility(View.GONE);
        }

        if (product.getMemList() != null && product.getMemList().size() > 0) {
            titleStorage.setVisibility(View.VISIBLE);
            storageSpinner.setVisibility(View.VISIBLE);
        } else {
            titleStorage.setVisibility(View.GONE);
            storageSpinner.setVisibility(View.GONE);
        }

        if (product.getHertzList() != null && product.getHertzList().size() > 0) {
            titleHertz.setVisibility(View.VISIBLE);
            hertzSpinner.setVisibility(View.VISIBLE);
        } else {
            titleHertz.setVisibility(View.GONE);
            hertzSpinner.setVisibility(View.GONE);

        }

        setColorList(product);
        setRamList(product);
        setStorageList(product);
        setHertzList(product);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("color" ,colorSpinner.getSelectedItemPosition());
        outState.putInt("ram" ,ramSpinner.getSelectedItemPosition());
        outState.putInt("storage" ,storageSpinner.getSelectedItemPosition());
        outState.putInt("hertz" ,hertzSpinner.getSelectedItemPosition());
    }

    private void setColorList(Product product) {
        if (product.getColorList() != null && product.getColorList().size() > 0) {
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, product.getColorList());
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            colorSpinner.setAdapter(adapter);
            colorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            colorSpinner.setSelection(0);
        }
    }

    private void setRamList(Product product) {
        if (product.getRamList() != null && product.getRamList().size() > 0) {
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, product.getRamList());
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            ramSpinner.setAdapter(adapter);
            ramSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            ramSpinner.setSelection(0);
        }
    }

    private void setStorageList(Product product) {

        if (product.getMemList() != null && product.getMemList().size() > 0) {
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, product.getMemList());
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            storageSpinner.setAdapter(adapter);
            storageSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            storageSpinner.setSelection(0);
        }
    }

    private void setHertzList(Product product) {
        if (product.getHertzList() != null && product.getHertzList().size() > 0) {
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, product.getHertzList());
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            hertzSpinner.setAdapter(adapter);
            hertzSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            hertzSpinner.setSelection(0);
        }
    }

    public static class GetVariant extends AsyncTask<String, Void, Product> {

        private Listener listener;

        public interface Listener {
            void onResult(Product product);
        }

        GetVariant(Listener listener) {
            this.listener = listener;
        }

        @Override
        protected Product doInBackground(String... strings) {
            return AppController.getInstance().getDataBaseHelper().getSpecificationByProduct(strings[0]);
        }

        @Override
        protected void onPostExecute(Product product) {
            if(listener != null){
                listener.onResult(product);
            }
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();
        setRetainInstance(false);
    }

    @Override
    public void onDestroyView() {
        if (getDialog() != null && getRetainInstance())
            getDialog().setDismissMessage(null);
        super.onDestroyView();

    }

    @Override
    public void onCancel(@NonNull DialogInterface dialog) {
        super.onCancel(dialog);
        setRetainInstance(false);
    }
}
