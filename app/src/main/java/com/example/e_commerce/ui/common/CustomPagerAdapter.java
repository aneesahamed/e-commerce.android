package com.example.e_commerce.ui.common;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.e_commerce.R;
import com.example.e_commerce.common.imageUtils.ImageFetcher;
import com.example.e_commerce.model.entity.TrendsDeal;

import java.util.List;

import androidx.viewpager.widget.PagerAdapter;

public class CustomPagerAdapter extends PagerAdapter {
    public Context context;
    Context mContext;
    LayoutInflater mLayoutInflater;
    private List<TrendsDeal> list;
    private ImageFetcher imageFetcher;
    private SliderListener listener;
    public interface SliderListener {
        void onClick(TrendsDeal trendsDeal);
    }

    public CustomPagerAdapter(Context context, List<TrendsDeal> list, ImageFetcher imageFetcher) {
        mContext = context;
        this.list = list;
        this.imageFetcher = imageFetcher;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setListener(SliderListener listener) {
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.list_item_imageview, container, false);
        final TrendsDeal trendsDeal = list.get(position);
        ImageView img = itemView.findViewById(R.id.imgItem);
        imageFetcher.loadImage(trendsDeal.getUrl(), img);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener != null){
                    listener.onClick(trendsDeal);
                }
            }
        });
        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

    public void setList(List<TrendsDeal> list){
        this.list = list;
    }
}
