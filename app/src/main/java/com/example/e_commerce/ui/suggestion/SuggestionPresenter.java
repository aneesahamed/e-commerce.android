package com.example.e_commerce.ui.suggestion;

import android.content.Context;

import com.example.e_commerce.common.AppController;
import com.example.e_commerce.common.PreferenceManager;
import com.example.e_commerce.common.imageUtils.AsyncTask;
import com.example.e_commerce.model.entity.Category;

import java.util.ArrayList;
import java.util.List;

public class SuggestionPresenter {

    public SuggestionInteractor interactor;

    SuggestionPresenter(SuggestionInteractor interactor) {
        this.interactor = interactor;
    }

    public void registerUserInterest(List<Category> list, Context context) {
        this.interactor.showLoader();
        List<Category> mList = new ArrayList<>();
        for(Category category : list){
            if(category.isChecked()){
                mList.add(category);
            }
        }
        if(mList.isEmpty()){
            mList.add(list.get(0));
        }
        new RegisterInterest(context ,interactor,mList).execute();
    }

    private static class RegisterInterest extends AsyncTask<Void, Void, Boolean> {
        private Context context;
        private List<Category> list;
        private SuggestionInteractor interactor;
        public RegisterInterest(Context context ,SuggestionInteractor interactor ,List<Category> list) {
            this.context = context;
            this.list = list;
            this.interactor = interactor;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            return AppController.getInstance().getDataBaseHelper().registerUserInterest(list, PreferenceManager.getUserInfo(context));
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if(interactor != null){
                interactor.hideLoader();
                interactor.launchNextScreen();
            }
        }
    }

    public void onDestroy(){
        interactor = null;
    }
}
