package com.example.e_commerce.ui.Order.review;

import com.example.e_commerce.ui.BaseInteractor;

public interface ReviewInteractor extends BaseInteractor {
    @Override
    void noInternet();

    @Override
    void showLoader();

    @Override
    void hideLoader();

    void onResult();

    void showFieldError();
}
