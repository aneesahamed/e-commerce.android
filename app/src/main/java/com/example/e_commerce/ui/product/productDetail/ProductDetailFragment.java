package com.example.e_commerce.ui.product.productDetail;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.e_commerce.R;
import com.example.e_commerce.adapter.CustomProductSlider;
import com.example.e_commerce.adapter.RelatedProductAdapter;
import com.example.e_commerce.adapter.ReviewAdapter;
import com.example.e_commerce.adapter.SpecificationAdapter;
import com.example.e_commerce.common.AppController;
import com.example.e_commerce.common.PreferenceManager;
import com.example.e_commerce.common.Utils;
import com.example.e_commerce.common.imageUtils.ImageFetcher;
import com.example.e_commerce.model.entity.Product;
import com.example.e_commerce.model.entity.Review;
import com.example.e_commerce.model.entity.Specification;
import com.example.e_commerce.model.entity.User;
import com.example.e_commerce.ui.cart.CartListFragment;
import com.example.e_commerce.ui.common.ClickListener;
import com.example.e_commerce.ui.common.ViewPagerScroller;
import com.example.e_commerce.ui.product.ProductHomeListener;
import com.example.e_commerce.ui.product.deliverDetail.DeliverFragment;
import com.example.e_commerce.ui.productVariant.DialogVariant;

import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import static com.example.e_commerce.model.DBConstant.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductDetailFragment extends Fragment implements ProductInteractor {


    private ViewPager viewPager;
    private TextView txtProductBrand, txtProductName, txtRating, txtProductPrice, txtProductOrg, txtOffer, txtColor, txtStorage, txtRam, txtHertz, txtProductInfo, txtShowLess, txtWarrenty, txtColorCount, txtRamCount, txtStorageCount, txtProductColor;
    private TextView txtHertzCount, txtReviews, txtBuy, txtcart;
    private LinearLayout llColorCount, llStorageCount, llRamCount, llHertz;
    private RelativeLayout specVariantLayout;
    private ProductPresenter productPresenter;
    public ProductHomeListener mListener;
    private boolean isTaskRunning;
    private ImageFetcher imageFetcher;
    private LinearLayout llDots;
    private TextView[] dots;
    private CustomProductSlider pagerAdapter;
    private Handler handler;
    private Runnable runnable;
    private NestedScrollView nestedScrollView;
    private int currentPage;
    private Timer timer;
    private List<String> imgList = new ArrayList<>();
    private List<Specification> specificationList = new ArrayList<>();
    private SpecificationAdapter specificationAdapter;
    private RecyclerView specRecyclerView, reviewRecycler;
    private FrameLayout actionBuy, actionCart;
    private RelatedProductAdapter relatedProductAdapter;
    private RecyclerView relatedProductList;
    private List<Product> relatedList = new ArrayList<>();
    private final Object lock = new Object();
    private int pagination;
    private boolean isPaginationEnd;
    private Product product;
    private boolean isProductInCart;
    private User user;
    private ReviewAdapter reviewAdapter;
    List<Review> reviewList = new ArrayList<>();
    public static int REQUEST_CODE_COUPONS = 110;
    public ProductDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_product_detail, container, false);
        relatedProductList = view.findViewById(R.id.relatedProductList);
        txtProductBrand = view.findViewById(R.id.productBrand);
        txtProductName = view.findViewById(R.id.productName);
        txtProductColor = view.findViewById(R.id.productColor);
        txtRating = view.findViewById(R.id.txtRating);
        txtBuy = view.findViewById(R.id.txtBuy);
        txtcart = view.findViewById(R.id.txtCart);
        reviewRecycler = view.findViewById(R.id.reviewList);
        txtOffer = view.findViewById(R.id.txtOff);
        txtReviews = view.findViewById(R.id.txtReviews);
        txtProductOrg = view.findViewById(R.id.txtProductPriceOrg);
        txtProductPrice = view.findViewById(R.id.txtProductPrice);
        txtColor = view.findViewById(R.id.txtColor);
        txtStorage = view.findViewById(R.id.txtStorage);
        txtRam = view.findViewById(R.id.txtRam);
        txtHertz = view.findViewById(R.id.txtHertz);
        txtProductInfo = view.findViewById(R.id.txtProductInfo);
        txtShowLess = view.findViewById(R.id.txtShowLess);
        txtWarrenty = view.findViewById(R.id.txtWarrenty);
        txtColorCount = view.findViewById(R.id.txtColorCount);
        txtStorageCount = view.findViewById(R.id.txtStorageCount);
        txtRamCount = view.findViewById(R.id.txtRamCount);
        llColorCount = view.findViewById(R.id.llColor);
        llStorageCount = view.findViewById(R.id.llStorage);
        llRamCount = view.findViewById(R.id.llRam);
        llHertz = view.findViewById(R.id.llHertz);
        txtHertzCount = view.findViewById(R.id.txtHertzCount);
        specVariantLayout = view.findViewById(R.id.specVariantLayout);
        nestedScrollView = view.findViewById(R.id.nestedScrollView);
        viewPager = view.findViewById(R.id.viewPager);
        llDots = view.findViewById(R.id.llDots);
        actionBuy = view.findViewById(R.id.actionBuy);
        actionCart = view.findViewById(R.id.actionCart);
        specRecyclerView = view.findViewById(R.id.specRecyclerView);
        specRecyclerView.setNestedScrollingEnabled(false);
        imageFetcher = Utils.getImageFetcher(getActivity() ,false);
        productPresenter = new ProductPresenter(this);
        pagerAdapter = new CustomProductSlider(getActivity(), imgList, imageFetcher);
        specificationAdapter = new SpecificationAdapter(specificationList);
        specRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        specRecyclerView.setAdapter(specificationAdapter);
        viewPager.setAdapter(pagerAdapter);
        changePagerScroller();
        user = PreferenceManager.getUserInfo(getContext());
        reviewRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        reviewAdapter = new ReviewAdapter(getActivity() ,reviewList);
        reviewRecycler.setAdapter(reviewAdapter);
        relatedProductList.setLayoutManager(new LinearLayoutManager(getActivity()));
        relatedProductList.setNestedScrollingEnabled(false);
        relatedProductAdapter = new RelatedProductAdapter(getActivity(), imageFetcher, relatedList, new ClickListener() {
            @Override
            public void onItemClick(Object object) {
                Product product = (Product) object;
                if (product != null) {
                   setProduct(product.getpId());
                }
            }
        } ,relatedProductList);
        relatedProductList.setAdapter(relatedProductAdapter);
        if (savedInstanceState == null) {
            if (getArguments() != null) {
                getProduct(getArguments().getString("id"));
            }
        } else {
            currentPage = savedInstanceState.getInt("currentViewPager");
            isTaskRunning = savedInstanceState.getBoolean("isTaskRunning");
            isPaginationEnd = savedInstanceState.getBoolean("isPaginationEnd");
            isProductInCart = savedInstanceState.getBoolean("isProductInCart");
            getActivity().setTitle(product.getName());
            setVariant(this.product);
            populateData(this.product);
            addBottomDots(currentPage, pagerAdapter.getCount());
            removeHandlerCallback();
            setAutoScrollPager();
            setCartAction(this.product.getpId());
            final int[] position = savedInstanceState.getIntArray("SCROLL_POSITION");
            if (position != null)
                nestedScrollView.post(new Runnable() {
                    public void run() {
                        nestedScrollView.scrollTo(position[0], position[1]);
                    }
                });
            if (mListener != null) {
                mListener.setProduct(this.product);
            }

        }

        nestedScrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                View view = (View) nestedScrollView.getChildAt(nestedScrollView.getChildCount() - 1);

                int diff = (view.getBottom() - (nestedScrollView.getHeight() + nestedScrollView
                        .getScrollY()));

                if (diff == 0) {
                    // your pagination code
                    if (product != null) {
                        if (!isTaskRunning) {
                            if (!isPaginationEnd) {
                                isTaskRunning = true;
                                productPresenter.getRelatedProduct(pagination, product.getcId(), true);
                            }
                        }
                    }
                }
            }
        });

        specVariantLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogVariant dialogVariant = new DialogVariant();
                Bundle bundle = new Bundle();
                bundle.putString("pId" ,product.getpId());
                dialogVariant.setTargetFragment(ProductDetailFragment.this ,REQUEST_CODE_COUPONS);
                dialogVariant.setArguments(bundle);
                dialogVariant.show(getActivity().getSupportFragmentManager() , DialogVariant.class.getSimpleName());
                //mListener.openDialogVariant();
            }
        });

        actionCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isProductInCart) {
                    mListener.navigate(CartListFragment.class.getSimpleName() ,null);
                } else {
                    AppController.getInstance().getDataBaseHelper().addToCart(user.getUid() ,product.getpId(),product.getQty(),true);
                    isProductInCart = true;
                }
                txtcart.setText(getString(isProductInCart ? R.string.goToCart : R.string.addToCart));
            }
        });

        actionBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.navigate(DeliverFragment.class.getSimpleName(),product);
            }
        });
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(REQUEST_CODE_COUPONS == requestCode && resultCode == Activity.RESULT_OK){
            Bundle args = new Bundle();
            args.putString("id" ,data.getStringExtra("pid"));
            setArguments(args);
            getProduct(data.getStringExtra("pid"));
        }
    }

    public void setProduct(String id){
        Bundle args = new Bundle();
        args.putString("id" ,id);
        setArguments(args);
        getProduct(id);
    }
    private void getProduct(String id) {
        if (productPresenter != null) {
            mListener.showLoader();
            isTaskRunning = true;
            productPresenter.getProductDetail(id);
        }
    }

    @Override
    public void updateUI(final Product product) {
        nestedScrollView.fullScroll(View.FOCUS_UP);
        nestedScrollView.scrollTo(0, 0);
        this.product = product;
        txtRating.setText(String.valueOf(product.getRating()));
        mListener.setProduct(product);
        mListener.hideLoader();
        isTaskRunning = true;
        setPagerAdapter(product.getImgList());
        setVariant(product);
        setReviewAdapter(product);
        populateData(product);
        pagination = 0;
        isPaginationEnd = false;
        productPresenter.getRelatedProduct(pagination, product.getcId(), false);
        setCartAction(product.getpId());
    }

    private void setCartAction(String pid) {
        User user = PreferenceManager.getUserInfo(getActivity());
        int qty = AppController.getInstance().getDataBaseHelper().isProductInCart(user.getUid(), pid);
        product.setQty(qty==0?1:qty);
        isProductInCart = qty!=0;
        txtcart.setText(getString(isProductInCart ? R.string.goToCart : R.string.addToCart));
    }

    @Override
    public void showError() {

    }

    private void setReviewAdapter(Product product) {
        if (product.getReviewList() != null && product.getReviewList().size() > 0) {
            txtReviews.setVisibility(View.VISIBLE);
            reviewRecycler.setVisibility(View.VISIBLE);
            reviewList.clear();
            reviewList.addAll(product.getReviewList());
            reviewAdapter.notifyDataSetChanged();
        } else {
            txtReviews.setVisibility(View.GONE);
            reviewRecycler.setVisibility(View.GONE);
        }
    }

    private void populateData(Product product) {
        txtProductName.setText(product.getName());
        if (!TextUtils.isEmpty(product.getColor())) {
            txtProductColor.setText(String.format(getString(R.string.prodColor), product.getColor()));
        }
        txtRating.setVisibility(product.getRating() > 0 ? View.VISIBLE : View.GONE);
        DecimalFormat value = new DecimalFormat("#.#");
        value.format(product.getRating());
        txtRating.setText(value.format(product.getRating()));
        txtProductBrand.setText(product.getProductBrand());
        txtProductPrice.setText(String.format(getString(R.string.currency), String.valueOf(Utils.getDiscountValue(product.getAmount(), product.getDisCount()))));
        txtProductOrg.setText(String.valueOf(product.getAmount()));
        txtProductOrg.setPaintFlags(txtProductOrg.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        txtOffer.setText(String.format(getString(R.string.offText), String.valueOf(product.getDisCount())));
        txtWarrenty.setText(String.format(getString(R.string.warrenty), product.getWarrenty()));
        txtProductInfo.setText(product.getProductDesc());
        setVariant(product);
        setSpecList(product.getSpecificationList());
    }

    private void setVariant(Product product) {
        boolean isAnyAvailable = false;
        if (!TextUtils.isEmpty(product.getColor())) {
            isAnyAvailable = true;
            LinearLayout.LayoutParams wallParams = (LinearLayout.LayoutParams) llColorCount.getLayoutParams();
            wallParams.weight = 2.5f;
            llColorCount.setLayoutParams(wallParams);
            txtColor.setText(product.getColor());
            txtColorCount.setText(String.format(getString(R.string.vColor), String.valueOf(product.getColorCount())));

        } else {
            llColorCount.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(product.getRam())) {
            isAnyAvailable = true;
            LinearLayout.LayoutParams wallParams = (LinearLayout.LayoutParams) llRamCount.getLayoutParams();
            wallParams.weight = 2.5f;
            llRamCount.setLayoutParams(wallParams);
            txtRam.setText(product.getRam());
            txtRamCount.setText(String.format(getString(R.string.vRam), String.valueOf(product.getRamCount())));
        } else {
            llRamCount.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(product.getMemory())) {
            isAnyAvailable = true;
            LinearLayout.LayoutParams wallParams = (LinearLayout.LayoutParams) llStorageCount.getLayoutParams();
            wallParams.weight = 2.5f;
            llStorageCount.setLayoutParams(wallParams);
            txtStorage.setText(product.getMemory());
            txtStorageCount.setText(String.format(getString(R.string.vStorage), String.valueOf(product.getStorageCount())));
        } else {
            llStorageCount.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(product.getHertz())) {
            isAnyAvailable = true;
            LinearLayout.LayoutParams wallParams = (LinearLayout.LayoutParams) llHertz.getLayoutParams();
            wallParams.weight = 2.5f;
            llHertz.setLayoutParams(wallParams);
            txtHertz.setText(product.getHertz());
            txtHertzCount.setText(String.format(getString(R.string.vHertz), String.valueOf(product.getHertzCount())));
        } else {
            llHertz.setVisibility(View.GONE);
        }

        specVariantLayout.setVisibility(isAnyAvailable ? View.VISIBLE : View.GONE);
    }

    private void setPagerAdapter(List<String> list) {
        imgList.clear();
        imgList.addAll(list);
        //pagerAdapter.setList(list.size()>3?list.subList(0 ,3):list);
        pagerAdapter.notifyDataSetChanged();
        currentPage = 0;
        addBottomDots(0, pagerAdapter.getCount());
        setAutoScrollPager();
    }

    private void setSpecList(List<Specification> list) {
        specificationList.clear();
        specificationList.addAll(list);
        specificationAdapter.notifyDataSetChanged();

    }

    private void addBottomDots(int currentPage, int length) {
        if (getActivity() != null) {
            dots = new TextView[length];
            llDots.removeAllViews();
            for (int i = 0; i < dots.length; i++) {
                dots[i] = new TextView(getActivity());
                dots[i].setText(Html.fromHtml("&#8226;"));
                dots[i].setTextSize(35);
                dots[i].setTextColor(getResources().getColor(R.color.dot_inactive));
                llDots.addView(dots[i]);
            }

            if (dots.length > 0)
                dots[currentPage].setTextColor(getResources().getColor(R.color.dot_active));
        }
    }

    private void setAutoScrollPager() {
        viewPager.addOnPageChangeListener(pageChangeListener);
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                if (currentPage == pagerAdapter.getCount() - 1) {
                    currentPage = -1;
                }
                viewPager.setCurrentItem((currentPage == -1) ? 0 : currentPage + 1, true);
            }
        };
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(runnable);
            }
        }, 2500, 2500);

    }

    ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position, pagerAdapter.getCount());
            currentPage = position;
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ProductHomeListener) {
            mListener = (ProductHomeListener) context;
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("currentViewPager", currentPage);
        outState.putIntArray("SCROLL_POSITION", new int[]{nestedScrollView.getScrollX(), nestedScrollView.getScrollY()});
        outState.putBoolean("isTaskRunning", isTaskRunning);
        outState.putBoolean("isPaginationEnd", isPaginationEnd);
        outState.putBoolean("isProductInCart", isProductInCart);
    }

    @Override
    public void updateRelatedProducts(List<Product> list, boolean isLoadMore) {
        isTaskRunning = false;
        synchronized (lock) {
            if (list != null && list.size() > 0) {
                if (!isLoadMore) {
                    pagination = 1;
                    relatedList.clear();
                    relatedList.addAll(list);
                    relatedProductAdapter.notifyDataSetChanged();
                } else {
                    pagination = pagination + 1;
                    relatedList.addAll(list);
                    relatedProductAdapter.notifyDataSetChanged();

                }
            } else {
                if (!isLoadMore) {

                } else {
                    isPaginationEnd = true;
                }
            }
        }
    }

    private void changePagerScroller() {
        try {
            Field mScroller = null;
            mScroller = ViewPager.class.getDeclaredField("mScroller");
            mScroller.setAccessible(true);
            ViewPagerScroller scroller = new ViewPagerScroller(viewPager.getContext());
            mScroller.set(viewPager, scroller);
        } catch (Exception e) {
            Log.e(TAG, "error of change scroller ", e);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener.hideLoader();
        mListener = null;
    }

    private void removeHandlerCallback() {
        if (handler != null && timer != null) {
            handler.removeCallbacks(runnable);
            timer.cancel();
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (mListener != null) {
            if (isTaskRunning) {
                mListener.showLoader();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        removeHandlerCallback();
    }
}
