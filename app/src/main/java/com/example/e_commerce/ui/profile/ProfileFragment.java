package com.example.e_commerce.ui.profile;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.e_commerce.R;
import com.example.e_commerce.common.AppConstant;
import com.example.e_commerce.common.AppController;
import com.example.e_commerce.common.PreferenceManager;
import com.example.e_commerce.common.Utils;
import com.example.e_commerce.common.imageUtils.RoundedCornerImageView;
import com.example.e_commerce.model.entity.User;
import com.example.e_commerce.ui.common.GalleryBottomSheet;
import com.example.e_commerce.ui.home.HomeActivity;
import com.google.android.material.button.MaterialButton;

import java.io.File;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {

    public interface profileListener {
        void updateNav();
    }

    private profileListener listener;
    static final int RESULT_OPEN_GALLERY = 0;
    static final int RESULT_OPEN_CAMERA = 1;
    RoundedCornerImageView imgProfile;
    RelativeLayout editProfile;
    TextView txtOrders, txtWishList, txtCoupons;
    MaterialButton btnSave;
    EditText edName, edEmail, edMobile;
    public static final int REQ_GALLERY_CHOOSER = 100;
    String selectedPath = AppConstant.EMPTY;
    User user;
    int couponCount, wishListCount, orderCount;
    private File tempFile;

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        imgProfile = view.findViewById(R.id.imgProfile);
        editProfile = view.findViewById(R.id.editProfile);
        edName = view.findViewById(R.id.edName);
        edEmail = view.findViewById(R.id.edEmail);
        edMobile = view.findViewById(R.id.edMobileNumber);
        txtOrders = view.findViewById(R.id.txtOrder);
        txtWishList = view.findViewById(R.id.txtWishList);
        txtCoupons = view.findViewById(R.id.txtCoupons);
        btnSave = view.findViewById(R.id.btnSave);
        user = PreferenceManager.getUserInfo(getActivity());
        editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GalleryBottomSheet galleryBottomSheet = new GalleryBottomSheet();
                galleryBottomSheet.setTargetFragment(ProfileFragment.this, REQ_GALLERY_CHOOSER);
                galleryBottomSheet.show(getFragmentManager(), GalleryBottomSheet.class.getSimpleName());
            }
        });

        if (savedInstanceState == null) {
            orderCount = AppController.getInstance().getDataBaseHelper().getOrderCount(user.getUid());
            wishListCount = AppController.getInstance().getDataBaseHelper().getMyWishListCount(user.getUid());
            couponCount = AppController.getInstance().getDataBaseHelper().getCouponCount(user.getUid());
            populateData(user);
            setProfile();
        } else {
            edName.setText(savedInstanceState.getString("edName"));
            edEmail.setText(savedInstanceState.getString("edEmail"));
            edMobile.setText(savedInstanceState.getString("edMob"));
            selectedPath = savedInstanceState.getString("selectedPath");
            populateCount();
            setProfile();
        }


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity) getContext()).showProgressDialog();
                if (isValid(edName.getText().toString(), edEmail.getText().toString())) {
                    user.setName(edName.getText().toString().trim());
                    user.setEmail(edEmail.getText().toString().trim());
                    if (!TextUtils.isEmpty(selectedPath)) {
                        user.setProfileUri(selectedPath);
                    }
                    updateUserData(user);
                }
            }
        });
        return view;
    }

    private void updateUserData(User user) {
        int i = AppController.getInstance().getDataBaseHelper().updateUser(user);
        ((HomeActivity) getContext()).hideProgressDialog();
        if (i > 0) {
            PreferenceManager.setUserInfo(user, getActivity());
            Toast.makeText(getContext(), getString(R.string.profileUpdated), Toast.LENGTH_SHORT).show();
            listener.updateNav();
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof profileListener) {
            listener = (profileListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    private boolean isValid(String name, String email) {
        boolean isValid = true;
        if (TextUtils.isEmpty(name)) {
            isValid = false;
            edName.setError(getString(R.string.required));
        } else if (TextUtils.isEmpty(email)) {
            isValid = false;
            edEmail.setError(getString(R.string.required));
        }

        return isValid;
    }

    private void populateData(User user) {
        edName.setText(user.getName());
        edEmail.setText(user.getEmail());
        edMobile.setText(Utils.revertMobileNo(user.getMobileNo()));
        populateCount();
    }

    private void populateCount() {
        txtOrders.setText(String.valueOf(orderCount));
        txtWishList.setText(String.valueOf(wishListCount));
        txtCoupons.setText(String.valueOf(couponCount));
    }

    private void setProfile() {
        if (!TextUtils.isEmpty(selectedPath)) {
            File file = new File(selectedPath);
            if (file.exists()) {
                imgProfile.setImageBitmap(Utils.decodeSampledBitmap(file.getAbsolutePath(), 70, 70));
            }
        } else {
            if (!TextUtils.isEmpty(user.getProfileUri())) {
                File file = new File(user.getProfileUri());
                if (file.exists()) {
                    imgProfile.setImageBitmap(Utils.decodeSampledBitmap(file.getAbsolutePath(), 70, 70));
                }
            } else {
                imgProfile.setImageDrawable(getResources().getDrawable(R.drawable.placeholder));
            }
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("edName", edName.getText().toString());
        outState.putString("edEmail", edEmail.getText().toString());
        outState.putString("edMob", edMobile.getText().toString());
        outState.putString("selectedPath", selectedPath);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == REQ_GALLERY_CHOOSER && data != null) {
            openChooserDialog(data.getExtras().getBoolean("selection"));
        } else if (resultCode != RESULT_CANCELED && requestCode == RESULT_OPEN_CAMERA && resultCode == RESULT_OK) {
            Uri capturedUri = FileProvider.getUriForFile(getActivity(), getContext().getPackageName() + ".fileProvider", tempFile);
            selectedPath = tempFile.getAbsolutePath();
            setProfile();

        } else if (resultCode != RESULT_CANCELED && requestCode == RESULT_OPEN_GALLERY && resultCode == RESULT_OK && data != null) {
            Uri uri = data.getData();
            selectedPath = Utils.FilePath.getPath(getActivity(), uri);
            setProfile();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                Log.v("FileChoose", "Permission: " + permissions[0] + "was " + grantResults[0]);
                openCamera();
            } else {
                Toast.makeText(getContext(), R.string.app_permission_camera, Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == 2) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                Log.v("FileChoose", "Permission: " + permissions[0] + "was " + grantResults[0]);
                Log.v("FileChoose", "Permission: " + permissions[1] + "was " + grantResults[1]);
                openGallery();
            } else {
                Toast.makeText(getContext(), R.string.app_permission_gallery, Toast.LENGTH_LONG).show();
            }
        }
    }

    public void openChooserDialog(boolean isGallery) {
        if (isGallery) {
            if (Build.VERSION.SDK_INT >= 23) {
                if (Utils.isCameraPermissionGiven(getActivity())) {
                    Log.v("Permission", "Permission is granted");
                    openGallery();
                } else {
                    Log.v("Permission", "Permission is not granted");
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
                }
            } else {
                openGallery();
            }

        } else {
            if (Build.VERSION.SDK_INT >= 23) {
                if (Utils.isCameraPermissionGiven(getActivity())) {
                    Log.v("Permission", "Permission is granted");
                    openCamera();
                } else {
                    Log.v("Permission", "Permission is not granted");
                    requestPermissions(new String[]{android.Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                }
            } else {
                openCamera();
            }

        }
    }

    public void openGallery() {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        i.setType("image/*");
        startActivityForResult(i, RESULT_OPEN_GALLERY);
    }

    public void openCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
            tempFile = Utils.createTempFile(getActivity());
            if (tempFile != null) {
                Uri contentUri = FileProvider.getUriForFile(getActivity(), getActivity().getPackageName() + ".fileProvider", tempFile);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, contentUri);
                startActivityForResult(intent, RESULT_OPEN_CAMERA);

            }
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getActivity() != null) {
            getActivity().setTitle(getString(R.string.myProfile));
        }
    }
}
