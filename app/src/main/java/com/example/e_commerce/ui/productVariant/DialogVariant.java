package com.example.e_commerce.ui.productVariant;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.e_commerce.R;
import com.example.e_commerce.common.Utils;
import com.example.e_commerce.common.imageUtils.ImageFetcher;
import com.example.e_commerce.model.entity.Product;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import static android.app.Activity.RESULT_OK;

public class DialogVariant extends BottomSheetDialogFragment implements VariantInteractor {
    public Product mProduct;
    private Product product;
    ImageView actionClose;
    private TextView txtColor, txtRam, txtStorage, txthertz, productName, productColor, txtRating, txtPrice, txtOrgPrice, txtOff, txtWarrenty;
    private TextView txtHintColor, txtHintRam, txtHintStorage, txtHintHertz, notExist;
    private LinearLayout colorList, ramList, storageList, hertzList;
    private VariantPresenter presenter;
    private ImageFetcher imageFetcher;
    private FrameLayout actionApply;
    private CallBack callBack;
    private boolean isShowingAlready;
    public interface CallBack{
        void bottomSheetDetached(Product product);
        void attachBottomSheet();
        void onResult(Product product);
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        isShowingAlready = true;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_choose_variant, container, false);

        imageFetcher = Utils.getImageFetcher(getActivity() ,true);
        actionApply = rootView.findViewById(R.id.actionApply);
        actionClose = rootView.findViewById(R.id.actionClose);
        txtColor = rootView.findViewById(R.id.txtColor);
        txtRam = rootView.findViewById(R.id.txtRam);
        txtStorage = rootView.findViewById(R.id.txtStorage);
        notExist = rootView.findViewById(R.id.notExist);
        txthertz = rootView.findViewById(R.id.txtHertz);
        colorList = rootView.findViewById(R.id.colorList);
        ramList = rootView.findViewById(R.id.ramList);
        storageList = rootView.findViewById(R.id.storageList);
        hertzList = rootView.findViewById(R.id.hertzList);
        txtHintColor = rootView.findViewById(R.id.txtHintColor);
        txtHintRam = rootView.findViewById(R.id.txtHintRam);
        txtHintStorage = rootView.findViewById(R.id.txtHintStorage);
        txtHintHertz = rootView.findViewById(R.id.txtHintHertz);
        productName = rootView.findViewById(R.id.productName);
        productColor = rootView.findViewById(R.id.productColor);
        txtRating = rootView.findViewById(R.id.txtRating);
        txtPrice = rootView.findViewById(R.id.txtProductPrice);
        txtOrgPrice = rootView.findViewById(R.id.txtProductPriceOrg);
        txtOff = rootView.findViewById(R.id.txtOff);
        txtWarrenty = rootView.findViewById(R.id.txtWarrenty);
        presenter = new VariantPresenter(this);

        if(savedInstanceState == null){
            if (getArguments().containsKey("pId")) {
                presenter.getProductById(getArguments().getString("pId"));
            }
        }
        else {
            updateUI(product);
        }


        actionClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        actionApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(product != null){
                    Intent intent = new Intent();
                    intent.putExtra("pid" ,product.getpId());
                    getTargetFragment().onActivityResult(getTargetRequestCode(), RESULT_OK, intent);
                    dismiss();
                }
            }
        });
        return rootView;
    }

    public void setCallBack(CallBack callBack) {
        this.callBack = callBack;
    }

    @Override
    public void updateUI(Product product) {
        this.product = product;
        actionApply.setBackgroundResource((product==null)?R.drawable.bg_button_disable:R.drawable.bg_button_buy);
        if (product != null) {
            this.mProduct = product;
            notExist.setVisibility(View.GONE);
            productName.setText(product.getName());
            if (!TextUtils.isEmpty(product.getColor())) {
                productColor.setText(String.format(getString(R.string.prodColor), product.getColor()));
            }
            txtRating.setVisibility(product.getRating() > 0 ? View.VISIBLE : View.GONE);
            txtRating.setText(String.valueOf(product.getRating()));
            txtPrice.setText(String.format(getString(R.string.currency), String.valueOf(Utils.getDiscountValue(product.getAmount(), product.getDisCount()))));
            txtOrgPrice.setText(String.valueOf(product.getAmount()));
            txtOrgPrice.setPaintFlags(txtOrgPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            txtOff.setText(String.format(getString(R.string.offText), String.valueOf(product.getDisCount())));
            txtWarrenty.setText(String.format(getString(R.string.warrenty), product.getWarrenty()));
            setVariant(product);
        } else {
            notExist.setVisibility(View.VISIBLE);
            setVariant(mProduct);
        }
    }

    private void setVariant(Product product) {
        if (!TextUtils.isEmpty(product.getColor())) {
            txtColor.setVisibility(View.VISIBLE);
            txtHintColor.setVisibility(View.VISIBLE);
            colorList.setVisibility(View.VISIBLE);
            txtColor.setText(product.getColor());
        } else {
            txtColor.setVisibility(View.GONE);
            txtHintColor.setVisibility(View.GONE);
            colorList.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(product.getRam())) {
            txtRam.setVisibility(View.VISIBLE);
            txtHintRam.setVisibility(View.VISIBLE);
            ramList.setVisibility(View.VISIBLE);
            txtRam.setText(product.getRam());
        } else {
            txtRam.setVisibility(View.GONE);
            txtHintRam.setVisibility(View.GONE);
            ramList.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(product.getMemory())) {
            txtStorage.setVisibility(View.VISIBLE);
            txtHintStorage.setVisibility(View.VISIBLE);
            storageList.setVisibility(View.VISIBLE);
            txtStorage.setText(product.getMemory());
        } else {
            txtStorage.setVisibility(View.GONE);
            txtHintStorage.setVisibility(View.GONE);
            storageList.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(product.getHertz())) {
            txthertz.setVisibility(View.VISIBLE);
            txtHintHertz.setVisibility(View.VISIBLE);
            hertzList.setVisibility(View.VISIBLE);
            txthertz.setText(product.getHertz());
        } else {
            txthertz.setVisibility(View.GONE);
            txtHintHertz.setVisibility(View.GONE);
            hertzList.setVisibility(View.GONE);
        }

        setColorList(product);
        setRamList(product);
        setStorageList(product);
        setHertzList(product);
    }

    private void setColorList(final Product product) {

        if (product.getColorList() != null && product.getColorList().size() > 0) {
            colorList.removeAllViews();
            for (int i = 0; i < product.getColorList().size(); i++) {
                final int pos = i;
                final View infatedView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_variant, null, false);
                FrameLayout frameLayout = infatedView.findViewById(R.id.frLayout);
                ImageView lstImage = infatedView.findViewById(R.id.lstImage);
                TextView txtColorVariant = infatedView.findViewById(R.id.txtColorVariant);
                TextView txtVariant = infatedView.findViewById(R.id.txtVariant);
                txtVariant.setVisibility(View.GONE);
                txtColorVariant.setVisibility(View.VISIBLE);
                txtColorVariant.setText(product.getColorList().get(i));
                lstImage.setVisibility(View.VISIBLE);
                imageFetcher.loadImage(product.getImgList().get(0), lstImage);
                if (product.getColor().equals(product.getColorList().get(i))) {
                    frameLayout.setBackgroundResource(R.drawable.bg_variant);
                }
                frameLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (presenter != null) {
                            mProduct.setColor(product.getColorList().get(pos));
                            presenter.getProductBySpec(mProduct);
                        }
                    }
                });
                colorList.addView(infatedView);
            }
        }
    }

    private void setRamList(final Product product) {

        if (product.getRamList() != null && product.getRamList().size() > 0) {
            ramList.removeAllViews();
            for (int i = 0; i < product.getRamList().size(); i++) {
                final int pos = i;
                final View infatedView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_variant, null, false);
                FrameLayout frameLayout = infatedView.findViewById(R.id.frLayout);
                ImageView lstImage = infatedView.findViewById(R.id.lstImage);
                TextView txtColorVariant = infatedView.findViewById(R.id.txtColorVariant);
                TextView txtVariant = infatedView.findViewById(R.id.txtVariant);
                txtVariant.setVisibility(View.VISIBLE);
                txtVariant.setText(product.getRamList().get(i));
                txtColorVariant.setVisibility(View.GONE);
                lstImage.setVisibility(View.GONE);
                if (product.getRam().equals(product.getRamList().get(i))) {
                    frameLayout.setBackgroundResource(R.drawable.bg_variant);
                }
                frameLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (presenter != null) {
                            mProduct.setRam(product.getRamList().get(pos));
                            presenter.getProductBySpec(mProduct);
                        }
                    }
                });
                ramList.addView(infatedView);
            }
        }
    }

    private void setStorageList(final Product product) {

        if (product.getMemList() != null && product.getMemList().size() > 0) {
            storageList.removeAllViews();
            for (int i = 0; i < product.getMemList().size(); i++) {
                final int pos = i;
                final View infatedView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_variant, null, false);
                FrameLayout frameLayout = infatedView.findViewById(R.id.frLayout);
                ImageView lstImage = infatedView.findViewById(R.id.lstImage);
                TextView txtColorVariant = infatedView.findViewById(R.id.txtColorVariant);
                TextView txtVariant = infatedView.findViewById(R.id.txtVariant);
                txtVariant.setVisibility(View.VISIBLE);
                txtVariant.setText(product.getMemList().get(i));
                txtColorVariant.setVisibility(View.GONE);
                lstImage.setVisibility(View.GONE);
                if (product.getMemory().equals(product.getMemList().get(i))) {
                    frameLayout.setBackgroundResource(R.drawable.bg_variant);
                }
                frameLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (presenter != null) {
                            mProduct.setMemory(product.getMemList().get(pos));
                            presenter.getProductBySpec(mProduct);
                        }
                    }
                });
                storageList.addView(infatedView);
            }
        }
    }

    private void setHertzList(final Product product) {

        if (product.getHertzList() != null && product.getHertzList().size() > 0) {
            hertzList.removeAllViews();
            for (int i = 0; i < product.getHertzList().size(); i++) {
                final int pos = i;
                final View infatedView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_variant, null, false);
                FrameLayout frameLayout = infatedView.findViewById(R.id.frLayout);
                ImageView lstImage = infatedView.findViewById(R.id.lstImage);
                TextView txtColorVariant = infatedView.findViewById(R.id.txtColorVariant);
                TextView txtVariant = infatedView.findViewById(R.id.txtVariant);
                txtVariant.setVisibility(View.VISIBLE);
                txtVariant.setText(product.getHertzList().get(i));
                txtColorVariant.setVisibility(View.GONE);
                lstImage.setVisibility(View.GONE);
                if (product.getHertz().equals(product.getHertzList().get(i))) {
                    frameLayout.setBackgroundResource(R.drawable.bg_variant);
                }
                frameLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (presenter != null) {
                            mProduct.setHertz(product.getHertzList().get(pos));
                            presenter.getProductBySpec(mProduct);
                        }
                    }
                });
                hertzList.addView(infatedView);
            }
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();
        setRetainInstance(false);
        if(presenter != null){
            presenter.onDestroy();
        }
    }

    @Override
    public void onDestroyView() {
        if (getDialog() != null && getRetainInstance())
            getDialog().setDismissMessage(null);
        super.onDestroyView();

    }

    @Override
    public void onCancel(@NonNull DialogInterface dialog) {
        super.onCancel(dialog);
        setRetainInstance(false);
    }
}
