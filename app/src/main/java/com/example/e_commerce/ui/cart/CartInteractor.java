package com.example.e_commerce.ui.cart;

import com.example.e_commerce.model.entity.Product;
import com.example.e_commerce.ui.BaseInteractor;

import java.util.List;

public interface CartInteractor extends BaseInteractor {
    @Override
    void showLoader();

    @Override
    void hideLoader();

    @Override
    void noInternet();

    void onResult(List<Product> list);
}
