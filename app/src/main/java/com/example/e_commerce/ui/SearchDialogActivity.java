package com.example.e_commerce.ui;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.app.Activity;

import com.example.e_commerce.R;

import androidx.appcompat.widget.Toolbar;

public class SearchDialogActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_dialog);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        onSearchRequested();
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchManager.setOnDismissListener(new SearchManager.OnDismissListener() {
            @Override
            public void onDismiss() {
                String s ="";
            }
        });
    }

}
