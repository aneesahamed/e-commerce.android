package com.example.e_commerce.ui.cart;

import android.os.AsyncTask;

import com.example.e_commerce.common.AppController;
import com.example.e_commerce.model.entity.Product;

import java.util.List;

public class CartPresenter {
    private CartInteractor interactor;

    CartPresenter(CartInteractor interactor) {
        this.interactor = interactor;
    }

    public void getCartList(String id) {
        new GetCartList(interactor).execute(id);
    }

    public static class GetCartList extends AsyncTask<String, Void, List<Product>> {
        private CartInteractor interactor;

        GetCartList(CartInteractor interactor) {
            this.interactor = interactor;
        }

        @Override
        protected List<Product> doInBackground(String... id) {
            return AppController.getInstance().getDataBaseHelper().getCartList(id[0]);
        }

        @Override
        protected void onPostExecute(List<Product> list) {
            if(interactor != null){
                interactor.onResult(list);
            }
        }
    }

    public void onDestroy(){
        interactor = null;
    }
}
