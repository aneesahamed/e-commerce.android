package com.example.e_commerce.ui.Order.review;

import android.text.TextUtils;

import com.example.e_commerce.common.AppController;
import com.example.e_commerce.common.imageUtils.AsyncTask;
import com.example.e_commerce.model.entity.Order;
import com.example.e_commerce.model.entity.User;

import java.util.List;

public class ReviewPresenter {

    ReviewInteractor interactor;

    ReviewPresenter(ReviewInteractor interactor) {
        this.interactor = interactor;
    }


    public void addReview(User user, String rating, String content, List<String> list, Order order) {
        if(!TextUtils.isEmpty(content)){
            new ReviewProduct(user, interactor, rating, content, list).execute(order);
        }
        else {
            interactor.showFieldError();
        }
    }

    public static class ReviewProduct extends AsyncTask<Order, Void, Boolean> {

        User user;
        String content, rating;
        ReviewInteractor interactor;
        List<String> list;

        ReviewProduct(User user, ReviewInteractor interactor, String rating, String content, List<String> list) {
            this.interactor = interactor;
            this.user = user;
            this.content = content;
            this.rating = rating;
            this.list = list;
        }

        @Override
        protected Boolean doInBackground(Order... orders) {
            long i = AppController.getInstance().getDataBaseHelper().addReview(user.getUid(), user.getName(), orders[0].getPid(), rating, content, list);
            return i > 0;
        }

        @Override
        protected void onPostExecute(Boolean b) {
            interactor.onResult();
        }
    }
}
