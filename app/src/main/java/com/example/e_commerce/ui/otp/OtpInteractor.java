package com.example.e_commerce.ui.otp;

import com.example.e_commerce.ui.BaseInteractor;

public interface OtpInteractor extends BaseInteractor {

    @Override
    void showLoader();

    @Override
    void hideLoader();

    @Override
    void noInternet();

    void showInvalidOtp();

    void launchHome();
}
