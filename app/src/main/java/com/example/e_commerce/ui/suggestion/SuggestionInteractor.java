package com.example.e_commerce.ui.suggestion;

import com.example.e_commerce.ui.BaseInteractor;

public interface SuggestionInteractor extends BaseInteractor {
    @Override
    void showLoader();

    @Override
    void hideLoader();

    @Override
    void noInternet();

    void launchNextScreen();
}
