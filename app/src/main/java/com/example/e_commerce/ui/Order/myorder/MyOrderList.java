package com.example.e_commerce.ui.Order.myorder;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.e_commerce.R;
import com.example.e_commerce.adapter.MyOrderListAdapter;
import com.example.e_commerce.common.PreferenceManager;
import com.example.e_commerce.common.Utils;
import com.example.e_commerce.common.imageUtils.ImageFetcher;
import com.example.e_commerce.model.entity.Order;
import com.example.e_commerce.model.entity.Review;
import com.example.e_commerce.model.entity.User;
import com.example.e_commerce.ui.Order.orderDetail.OrderDetailFragment;
import com.example.e_commerce.ui.Order.review.ReviewFragment;
import com.example.e_commerce.ui.product.ProductActivity;
import com.example.e_commerce.ui.product.ProductHomeListener;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyOrderList extends Fragment implements MyOrderInteractor {


    private ProgressBar progressBar;
    private RecyclerView orderListRecycler;
    private TextView txtEmptyState;
    User user;
    ImageFetcher imageFetcher;
    ProductHomeListener mListener;
    private MyOrderPresenter presenter;
    private List<Order> orderList = new ArrayList<>();
    private MyOrderListAdapter adapter;
    public MyOrderList() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_order_list, container, false);
        progressBar = view.findViewById(R.id.progressBar);
        orderListRecycler = view.findViewById(R.id.orderList);
        user = PreferenceManager.getUserInfo(getContext());
        imageFetcher = Utils.getImageFetcher(getActivity() ,false);
        orderListRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new MyOrderListAdapter(getContext(), imageFetcher, orderList, new MyOrderListAdapter.Listener() {
            @Override
            public void onClick(View view, Order order) {
                if(view.getId() == R.id.actionReview){
                    mListener.navigate(ReviewFragment.class.getSimpleName() ,order);
                }
                else {
                    mListener.navigate(OrderDetailFragment.class.getSimpleName() ,order);
                }
            }
        });
        orderListRecycler.setAdapter(adapter);
        txtEmptyState = view.findViewById(R.id.txtEmptyState);
        presenter = new MyOrderPresenter(this);
        orderList.clear();
        adapter.notifyDataSetChanged();
        presenter.getList(user.getUid());
        return view;
    }

    @Override
    public void onResult(List<Order> list) {
        if(list != null && list.size()>0){
            orderList.clear();
            orderList.addAll(list);
            adapter.notifyDataSetChanged();
            txtEmptyState.setVisibility(View.INVISIBLE);
        }
        else {
            txtEmptyState.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(getActivity() != null){
            getActivity().setTitle(getString(R.string.myOrder));
        }
    }

    @Override
    public void noInternet() {

    }

    @Override
    public void showLoader() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoader() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ProductHomeListener) {
            mListener = (ProductHomeListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
