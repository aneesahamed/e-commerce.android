package com.example.e_commerce.ui.product.address;

import com.example.e_commerce.model.entity.Address;
import com.example.e_commerce.ui.BaseInteractor;

import java.util.List;

public interface AddressChangeInteractor extends BaseInteractor {
    @Override
    void showLoader();

    @Override
    void hideLoader();

    @Override
    void noInternet();

    void onSaveFinished();

    void showFieldError(int error);

    void showAddressList(List<Address> list);
}
