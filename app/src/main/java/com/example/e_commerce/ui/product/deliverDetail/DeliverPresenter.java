package com.example.e_commerce.ui.product.deliverDetail;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.example.e_commerce.common.AppConstant;
import com.example.e_commerce.common.AppController;
import com.example.e_commerce.common.PreferenceManager;
import com.example.e_commerce.common.imageUtils.AsyncTask;
import com.example.e_commerce.model.entity.Address;
import com.example.e_commerce.model.entity.AddressContract;
import com.example.e_commerce.model.entity.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import androidx.loader.content.CursorLoader;

import static com.example.e_commerce.model.DBConstant.ADDRESS_ID;
import static com.example.e_commerce.model.DBConstant.ADDRESS_TABLE;
import static com.example.e_commerce.model.DBConstant.ADDRESS_TYPE;
import static com.example.e_commerce.model.DBConstant.ALT_MOBILE_NO;
import static com.example.e_commerce.model.DBConstant.AREA;
import static com.example.e_commerce.model.DBConstant.CITY;
import static com.example.e_commerce.model.DBConstant.DOOR_NO;
import static com.example.e_commerce.model.DBConstant.FIRST_NAME;
import static com.example.e_commerce.model.DBConstant.LAST_NAME;
import static com.example.e_commerce.model.DBConstant.PINCODE;
import static com.example.e_commerce.model.DBConstant.STATE;
import static com.example.e_commerce.model.DBConstant.UID;

public class DeliverPresenter {

    private DeliverInteractor interactor;

    DeliverPresenter(DeliverInteractor interactor) {
        this.interactor = interactor;
    }

    public void getPreviousAddress(Context context, String id) {
        interactor.showLoader();
        new GetAddress(context, interactor).execute(id);
    }

    public static class GetAddress extends AsyncTask<String, Void, Address> {
        DeliverInteractor interactor;
        Context context;

        public GetAddress(Context context, DeliverInteractor interactor) {
            this.interactor = interactor;
            this.context = context;
        }

        @Override
        protected Address doInBackground(String... id) {
            String selection = UID + " =? AND " + ADDRESS_ID + " =?";
            User user = PreferenceManager.getUserInfo(context);
            Uri uri = AddressContract.AddressEntry.getAddressUri(0);
            Cursor cursor = context.getContentResolver().query(uri, null, selection, new String[]{user.getUid(), id[0]}, null);
            Address address = null;
            if (cursor != null && cursor.moveToFirst()) {
                address = new Address();
                address.setFirstName(cursor.getString(cursor.getColumnIndex(FIRST_NAME)));
                address.setLastName(cursor.getString(cursor.getColumnIndex(LAST_NAME)));
                address.setAddressId(cursor.getString(cursor.getColumnIndex(ADDRESS_ID)));
                address.setDoorNo(cursor.getString(cursor.getColumnIndex(DOOR_NO)));
                address.setArea(cursor.getString(cursor.getColumnIndex(AREA)));
                address.setCity(cursor.getString(cursor.getColumnIndex(CITY)));
                address.setState(cursor.getString(cursor.getColumnIndex(STATE)));
                address.setPinCode(cursor.getString(cursor.getColumnIndex(PINCODE)));
                address.setAltNo(cursor.getString(cursor.getColumnIndex(ALT_MOBILE_NO)));
                address.setAddressType(cursor.getString(cursor.getColumnIndex(ADDRESS_TYPE)));
                long seconds = AppController.getInstance().getDeliveryDuration(address.getPinCode(), "180001");
                address.setDeliveryInfo(seconds);
                cursor.close();
            }
            return address;
        }

        @Override
        protected void onPostExecute(Address address) {
            if (interactor != null) {
                interactor.hideLoader();
                interactor.updateAddress(address);
            }
        }

    }


}
