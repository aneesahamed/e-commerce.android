package com.example.e_commerce.ui.product.address;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.example.e_commerce.R;
import com.example.e_commerce.adapter.AddressListAdapter;
import com.example.e_commerce.common.PreferenceManager;
import com.example.e_commerce.model.entity.Address;
import com.example.e_commerce.model.entity.User;
import com.example.e_commerce.ui.common.ClickListener;
import com.example.e_commerce.ui.product.ProductHomeListener;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChooseAddressFragment extends Fragment implements AddressChangeInteractor {


    ProgressBar progressBar;
    RecyclerView addressList;
    FrameLayout actionAdd;
    FrameLayout actionContinue;
    List<Address> addresses = new ArrayList<>();
    AddressListAdapter addressListAdapter;
    AddressChangePresenter presenter;
    ProductHomeListener mListener;
    User user;

    public ChooseAddressFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_choose_address, container, false);
        progressBar = view.findViewById(R.id.progressBar);
        addressList = view.findViewById(R.id.addressList);
        actionAdd = view.findViewById(R.id.actionAdd);
        actionContinue = view.findViewById(R.id.actionContinue);
        presenter = new AddressChangePresenter(this);
        addressListAdapter = new AddressListAdapter(getActivity(), addresses, new ClickListener() {
            @Override
            public void onItemClick(Object object) {
                Address address = (Address) object;
                if (address != null) {
                    Bundle args = new Bundle();
                    args.putParcelable("address" ,address);
                    args.putBoolean("isForAdd", false);
                    mListener.navigate(AddressChangeFragment.class.getSimpleName(), args);
                }
            }
        });
        addressList.setLayoutManager(new LinearLayoutManager(getActivity()));
        addressList.setAdapter(addressListAdapter);
        user = PreferenceManager.getUserInfo(getActivity());
        actionAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle args = new Bundle();
                args.putBoolean("isForAdd", true);
                mListener.navigate(AddressChangeFragment.class.getSimpleName(), args);
            }
        });

        actionContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user.setAddressId(addressListAdapter.getSelectedId());
                PreferenceManager.setUserInfo(user ,getContext());
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        return view;

    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.getAddress(getActivity(), user.getUid());
    }

    @Override
    public void hideLoader() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showLoader() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void showFieldError(int error) {

    }

    @Override
    public void onSaveFinished() {

    }

    @Override
    public void noInternet() {

    }

    @Override
    public void showAddressList(List<Address> list) {
        if (list.size() > 0) {
            for (Address address : list) {
                if (user.getAddressId().equals(address.getAddressId())) {
                    address.setSelected(true);
                }
            }
            addresses.clear();
            addresses.addAll(list);
            addressListAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().setTitle(getString(R.string.selectAddress));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ProductHomeListener) {
            mListener = (ProductHomeListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        mListener = null;
    }
}
