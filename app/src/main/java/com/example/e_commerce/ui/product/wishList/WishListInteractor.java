package com.example.e_commerce.ui.product.wishList;

import com.example.e_commerce.model.entity.Product;
import com.example.e_commerce.ui.BaseInteractor;

import java.util.List;

public interface WishListInteractor extends BaseInteractor {
    @Override
    void noInternet();

    @Override
    void hideLoader();

    @Override
    void showLoader();

    void onResult(List<Product> list);
}
