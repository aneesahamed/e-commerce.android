package com.example.e_commerce.ui.otp;

import android.content.Context;
import android.text.TextUtils;

import com.example.e_commerce.R;
import com.example.e_commerce.common.AppConstant;
import com.example.e_commerce.common.Utils;

public class OtpPresenter {
    private OtpInteractor interactor;
    OtpPresenter(OtpInteractor interactor){
        this.interactor = interactor;
    }

    public void triggerOtp(Context context ,String secrectCode){
        Utils.registerInformationNotificationChannel(context);
        String info = String.format(context.getString(R.string.yourOtpCodeIs),secrectCode);
        Utils.sendNotification(context ,context.getString(R.string.yourOtpCode) ,info ,true, AppConstant.PRIMARY_CHANNEL ,0);
    }
    public void validateCode(String code ,String secretCode){
        if(isValid(code)){
            if(code.equals(secretCode)){
                interactor.showLoader();
                interactor.launchHome();
            }
            else {
                interactor.showInvalidOtp();
            }
        }
        else {
            interactor.showInvalidOtp();
        }
    }
    private boolean isValid(String code) {
        boolean isValid = true;
        if (interactor != null) {
            if (TextUtils.isEmpty(code)) {
                isValid = false;
            } else if (code.length() != 6) {
                isValid = false;
            }

            return isValid;
        }
        return false;
    }

}
