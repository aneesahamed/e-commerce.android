package com.example.e_commerce.ui.login;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.e_commerce.R;
import com.example.e_commerce.common.Utils;
import com.example.e_commerce.model.entity.User;
import com.example.e_commerce.ui.BaseActivity;
import com.example.e_commerce.ui.common.CustomEditText;
import com.example.e_commerce.ui.otp.OtpActivity;
import com.example.e_commerce.ui.signup.SignUpActivity;
import com.example.e_commerce.ui.signup.SignUpHelper;
import com.google.android.material.button.MaterialButton;

public class LoginActivity extends BaseActivity implements LoginInteractor, SignUpHelper.Callback {

    MaterialButton btnLogin;
    CustomEditText edMobileNo;
    LoginPresenter loginPresenter;
    SignUpHelper signUpHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        TextView txtSignUp = findViewById(R.id.txtSignUp);
        btnLogin = findViewById(R.id.btnLogin);
        edMobileNo = findViewById(R.id.edMobileNumber);
        loginPresenter = new LoginPresenter(this);
        Spannable text = new SpannableString(getString(R.string.dontHaveAnAccount));
        text.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorPrimary)), 24, 30, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        txtSignUp.setText(text);

        txtSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.hideKeyBoard(LoginActivity.this);
                loginPresenter.checkUser(LoginActivity.this, edMobileNo.getText().toString().trim());
            }
        });
        if (savedInstanceState != null) {
            edMobileNo.setText(savedInstanceState.getString("mobileNo"));
        }

        signUpHelper = (SignUpHelper) getSupportFragmentManager().findFragmentByTag(SignUpHelper.class.getSimpleName());
        if (signUpHelper == null) {
            signUpHelper = new SignUpHelper();
            loginPresenter.setSignUpHelper(signUpHelper);
            getSupportFragmentManager().beginTransaction().add(signUpHelper, SignUpHelper.class.getSimpleName()).commit();

        } else {
            loginPresenter.setSignUpHelper(signUpHelper);
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("mobileNo", edMobileNo.getText().toString());
    }

    @Override
    public void noInternet() {

    }

    @Override
    public void showMobileFieldError(boolean isEmpty) {

    }

    @Override
    public void showLoader() {
        showProgressDialog();
    }

    @Override
    public void hideLoader() {
        hideProgressDialog();
    }

    @Override
    public void result(User user) {
        if (user != null) {
            Intent intent = new Intent(LoginActivity.this, OtpActivity.class);
            intent.putExtra("isFromLogin", true);
            intent.putExtra("user", user);
            startActivity(intent);
        } else {
            Toast.makeText(LoginActivity.this, getString(R.string.invalidNumber), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void launchNextScreen() {

    }

    @Override
    public void showNameFieldError() {

    }

    @Override
    public void showEmailFieldError(boolean isEmpty) {

    }
}
