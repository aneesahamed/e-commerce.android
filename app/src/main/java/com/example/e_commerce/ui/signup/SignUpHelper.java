package com.example.e_commerce.ui.signup;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;

import com.example.e_commerce.common.AppController;
import com.example.e_commerce.model.entity.User;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class SignUpHelper extends Fragment {

    public boolean isTaskRunning = false;
    private Callback mListener;

    public interface TaskListener {
        void onFinished(User user);
    }

    public interface Callback extends SignUpInteractor {
        @Override
        void showLoader();

        @Override
        void hideLoader();

        void result(User user);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (isTaskRunning) {
            if (mListener != null) {
                mListener.showLoader();
            }
        }
    }

    public void checkUser(String mobileNo) {
        isTaskRunning = true;
        mListener.showLoader();
        new CheckUserExists(new TaskListener() {
            @Override
            public void onFinished(User result) {
                isTaskRunning = false;
                if (mListener != null) {
                    mListener.hideLoader();
                    mListener.result(result);
                }

            }
        }).execute(mobileNo);
    }

    public static class CheckUserExists extends AsyncTask<String, Void, User> {

        private TaskListener listener;

        public CheckUserExists(TaskListener listener) {
            this.listener = listener;
        }

        @Override
        protected User doInBackground(String... args) {
            return AppController.getInstance().getDataBaseHelper().isUserAlreadyExist(args[0]);
        }

        @Override
        protected void onPostExecute(User user) {
            listener.onFinished(user);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Callback) {
            mListener = (Callback) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener.hideLoader();
        mListener = null;
    }
}
