package com.example.e_commerce.ui.product.productList;


import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;

import com.example.e_commerce.common.AppController;
import com.example.e_commerce.model.entity.Product;
import com.example.e_commerce.ui.BaseInteractor;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class ProductHeadlessFragment extends Fragment {

    public interface FragListener extends BaseInteractor {
        @Override
        void showLoader();

        @Override
        void hideLoader();

        @Override
        void noInternet();

        void onResult(boolean isLoadMore, List<Product> list);
    }

    public FragListener listener;
    private List<Product> products;
    private boolean isTaskRunning;
    public List<Product> mlist = new ArrayList<>();
    public boolean mIsLoadMore = false;

    public ProductHeadlessFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    public void getData(boolean isFromSearch, String keyWord,String sort, int pagination, final boolean isLoadMore) {
        isTaskRunning = true;
        this.mIsLoadMore = isLoadMore;
        new GetProducts(isFromSearch,sort, pagination, new GetProducts.Listener() {
            @Override
            public void onResult(List<Product> list) {
                isTaskRunning = false;
                if (list.size() > 0) {
                    if (!isLoadMore) {
                        mlist.clear();
                        mlist.addAll(list);
                    } else {
                        mlist.addAll(list);
                    }

                }
                if (listener != null) {
                    listener.hideLoader();
                    listener.onResult(isLoadMore, list);
                }
            }
        }).execute(keyWord);
    }

    public void getData(Product product, String sort, int pagination, final boolean isLoadMore) {
        isTaskRunning = true;
        this.mIsLoadMore = isLoadMore;
        new GetProductBySpec(sort, pagination, new GetProductBySpec.Listener() {
            @Override
            public void onResult(List<Product> list) {
                isTaskRunning = false;
                if (list.size() > 0) {
                    if (!isLoadMore) {
                        mlist.clear();
                        mlist.addAll(list);
                    } else {
                        mlist.addAll(list);
                    }

                }
                if (listener != null) {
                    listener.hideLoader();
                    listener.onResult(isLoadMore, list);
                }
            }
        }).execute(product);
    }

    public List<Product> getData() {
        if (!isTaskRunning) {
            if (mlist != null && mlist.size() > 0) {
                return mlist;
            }
        }
        return new ArrayList<Product>();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (isTaskRunning) {
            listener.showLoader();
        } else {
            listener.hideLoader();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragListener) {
            listener = (FragListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    public static class GetProducts extends AsyncTask<String, Void, List<Product>> {
        public interface Listener {
            void onResult(List<Product> list);
        }

        private int pagination;
        private Listener listener;
        private boolean isFromSearch;
        private String sort;
        public GetProducts(boolean isFromSearch, String sort ,int pagination, Listener listener) {
            this.listener = listener;
            this.isFromSearch = isFromSearch;
            this.pagination = pagination;
            this.sort = sort;
        }

        @Override
        protected List<Product> doInBackground(String... key) {
            return isFromSearch ? AppController.getInstance().getDataBaseHelper().getProductList(key[0], pagination, sort) : AppController.getInstance().getDataBaseHelper().getProductListByCategory(key[0], pagination ,sort);
        }

        @Override
        protected void onPostExecute(List<Product> list) {
            if (listener != null) {
                listener.onResult(list);

            }
        }
    }

    public static class GetProductBySpec extends AsyncTask<Product, Void, List<Product>> {
        public interface Listener {
            void onResult(List<Product> list);
        }

        private int pagination;
        private Listener listener;
        private String sort;
        public GetProductBySpec(String sort ,int pagination, Listener listener) {
            this.listener = listener;
            this.pagination = pagination;
            this.sort = sort;
        }

        @Override
        protected List<Product> doInBackground(Product... product) {
            return  AppController.getInstance().getDataBaseHelper().getProductsByFilter(product[0], pagination, sort);
        }

        @Override
        protected void onPostExecute(List<Product> list) {
            if (listener != null) {
                listener.onResult(list);

            }
        }
    }
}
