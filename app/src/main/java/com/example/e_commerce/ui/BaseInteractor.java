package com.example.e_commerce.ui;

public interface BaseInteractor {
    void showLoader();
    void hideLoader();
    void noInternet();
}
