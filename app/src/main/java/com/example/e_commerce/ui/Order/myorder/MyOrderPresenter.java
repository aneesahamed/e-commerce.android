package com.example.e_commerce.ui.Order.myorder;

import com.example.e_commerce.common.AppController;
import com.example.e_commerce.common.imageUtils.AsyncTask;
import com.example.e_commerce.model.entity.Order;

import java.util.List;

public class MyOrderPresenter {
    MyOrderInteractor interactor;

    MyOrderPresenter(MyOrderInteractor interactor) {
        this.interactor = interactor;
    }

    public void getList(String uid) {
        new GetList(interactor).execute(uid);
    }

    public static class GetList extends AsyncTask<String, Void, List<Order>> {
        private MyOrderInteractor interactor;

        GetList(MyOrderInteractor interactor) {
            this.interactor = interactor;
        }

        @Override
        protected List<Order> doInBackground(String... uid) {
            return AppController.getInstance().getDataBaseHelper().getMyOrderList(uid[0]);
        }

        @Override
        protected void onPostExecute(List<Order> list) {
            if(interactor != null){
                interactor.hideLoader();
                interactor.onResult(list);
            }
        }
    }
}
