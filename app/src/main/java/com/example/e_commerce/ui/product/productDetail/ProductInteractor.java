package com.example.e_commerce.ui.product.productDetail;

import com.example.e_commerce.model.entity.Product;

import java.util.List;

public interface ProductInteractor {
    void updateUI(Product product);

    void showError();

    void updateRelatedProducts(List<Product> list, boolean isLoadMore);
}
