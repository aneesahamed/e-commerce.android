package com.example.e_commerce.ui.product.address;

import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;

import com.example.e_commerce.common.AppController;
import com.example.e_commerce.common.PreferenceManager;
import com.example.e_commerce.common.imageUtils.AsyncTask;
import com.example.e_commerce.model.entity.Address;
import com.example.e_commerce.model.entity.AddressContract;
import com.example.e_commerce.model.entity.User;

import java.util.List;
import java.util.UUID;

import static com.example.e_commerce.model.DBConstant.ADDRESS_ID;
import static com.example.e_commerce.model.DBConstant.ADDRESS_TYPE;
import static com.example.e_commerce.model.DBConstant.ALT_MOBILE_NO;
import static com.example.e_commerce.model.DBConstant.AREA;
import static com.example.e_commerce.model.DBConstant.CID;
import static com.example.e_commerce.model.DBConstant.CITY;
import static com.example.e_commerce.model.DBConstant.DOOR_NO;
import static com.example.e_commerce.model.DBConstant.FIRST_NAME;
import static com.example.e_commerce.model.DBConstant.LAST_NAME;
import static com.example.e_commerce.model.DBConstant.PINCODE;
import static com.example.e_commerce.model.DBConstant.STATE;
import static com.example.e_commerce.model.DBConstant.UID;

public class AddressChangePresenter {
    AddressChangeInteractor interactor;

    AddressChangePresenter(AddressChangeInteractor interactor) {
        this.interactor = interactor;
    }

    public void saveAddress(Context context, boolean isForSave ,String addressId , String fName, String lName, String doorNo, String area, String city, String state, String alt ,String pinCode ,String addressType) {
        if (isValid(fName, lName, doorNo, area, city, state, alt ,pinCode)) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(FIRST_NAME, fName);
            contentValues.put(LAST_NAME, lName);
            contentValues.put(DOOR_NO, doorNo);
            contentValues.put(AREA,area);
            contentValues.put(CITY, city);
            contentValues.put(STATE, state);
            contentValues.put(ALT_MOBILE_NO, alt);
            contentValues.put(PINCODE ,pinCode);
            contentValues.put(ADDRESS_TYPE, addressType);
            if(!isForSave){
                contentValues.put(ADDRESS_ID, addressId);
            }
            new SaveAdress(context ,addressId ,isForSave ,interactor).execute(contentValues);
        }
    }

    public void getAddress(Context context ,String id){
        interactor.showLoader();
        new GetAdresses(context,interactor).execute(id);
    }
    private boolean isValid(String fName, String lName, String doorNo, String area, String city, String state, String alt ,String pinCode) {
        boolean isValid = true;
        if (TextUtils.isEmpty(fName)) {
            isValid = false;
            interactor.showFieldError(0);
        }
        else if (TextUtils.isEmpty(lName)) {
            isValid = false;
            interactor.showFieldError(1);
        }
        else if (TextUtils.isEmpty(doorNo)) {
            isValid = false;
            interactor.showFieldError(2);
        }
        else if (TextUtils.isEmpty(area)) {
            isValid = false;
            interactor.showFieldError(3);
        }
        else if (TextUtils.isEmpty(city)) {
            isValid = false;
            interactor.showFieldError(4);
        }else if (TextUtils.isEmpty(state)) {
            isValid = false;
            interactor.showFieldError(5);
        }
        else if (TextUtils.isEmpty(alt)) {
            isValid = false;
            interactor.showFieldError(6);
        }
        else if (TextUtils.isEmpty(pinCode)) {
            isValid = false;
            interactor.showFieldError(7);
        }
        return isValid;
    }

    public static class SaveAdress extends AsyncTask<ContentValues, Void, Boolean> {
        AddressChangeInteractor interactor;
        Context context;
        boolean isForSave;
        String mAddressId;
        SaveAdress(Context context, String mAddressId ,boolean isForSave ,AddressChangeInteractor interactor) {
            this.interactor = interactor;
            this.context = context;
            this.isForSave = isForSave;
            this.mAddressId = mAddressId;
        }

        @Override
        protected Boolean doInBackground(ContentValues... contentValues) {
            User user = PreferenceManager.getUserInfo(context);
            contentValues[0].put(UID, user.getUid());
           if(isForSave){
               String addressId = UUID.randomUUID().toString();
               contentValues[0].put(ADDRESS_ID, addressId);
               Uri uri = context.getContentResolver().insert(AddressContract.AddressEntry.CONTENT_URI, contentValues[0]);
               if (uri != null) {
                   user.setAddressId(addressId);
                   PreferenceManager.setUserInfo(user, context);
                   return true;
               }
           }
           else {
               String selection = UID+" =? AND "+ADDRESS_ID+" =? ";
               int i = context.getContentResolver().update(AddressContract.AddressEntry.getAddressUri(0),contentValues[0],selection ,new String[]{user.getUid() ,mAddressId});
               if(i>0){
                   return true;
               }
           }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean mBoolean) {
            if (mBoolean) {
                if (interactor != null) {
                    interactor.onSaveFinished();
                }
            }
        }
    }

    public static class GetAdresses extends AsyncTask<String, Void, List<Address>> {
        AddressChangeInteractor interactor;
        Context context;

        GetAdresses(Context context, AddressChangeInteractor interactor) {
            this.interactor = interactor;
            this.context = context;
        }

        @Override
        protected List<Address> doInBackground(String... id) {
            return AppController.getInstance().getDataBaseHelper().getAddresses(id[0]);
        }

        @Override
        protected void onPostExecute(List<Address> list) {
            if (interactor != null) {
                interactor.hideLoader();
                interactor.showAddressList(list);
            }
        }
    }
}
