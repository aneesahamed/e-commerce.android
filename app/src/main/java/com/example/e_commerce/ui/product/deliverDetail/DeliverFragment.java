package com.example.e_commerce.ui.product.deliverDetail;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.e_commerce.R;
import com.example.e_commerce.common.PreferenceManager;
import com.example.e_commerce.common.Utils;
import com.example.e_commerce.common.imageUtils.ImageFetcher;
import com.example.e_commerce.model.entity.Address;
import com.example.e_commerce.model.entity.Price;
import com.example.e_commerce.model.entity.Product;
import com.example.e_commerce.model.entity.User;
import com.example.e_commerce.ui.product.ProductHomeListener;
import com.example.e_commerce.ui.product.address.AddressChangeFragment;
import com.example.e_commerce.ui.product.address.ChooseAddressFragment;
import com.example.e_commerce.ui.product.payment.PaymentFragment;
import com.google.android.material.card.MaterialCardView;

import java.util.Arrays;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;


public class DeliverFragment extends Fragment implements DeliverInteractor {


    private ImageView imgProduct;
    private TextView productName, productColor, txtRating, txtPrice, txtOrgPrice, txtOff, txtWarrenty, txtQty, txtDelivery, txtName, txtActionChange;
    private TextView txtAddress, txtState_pinCode, txtMobNo, txtPriceItem, txtPriceValue, txtDeliveryValue, txtOfferValue, txtPayValue;
    private MaterialCardView actionQty;
    private FrameLayout actionChange, actionContinue;
    private Spinner qtySpinner;
    private ImageFetcher imageFetcher;
    private String[] qtyList;
    private DeliverPresenter presenter;
    public ProductHomeListener mListener;
    private boolean isTaskRunning;
    private Address address;
    private Product product;
    private User user;
    private Price price = new Price();

    public DeliverFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_deliver, container, false);
        imageFetcher = Utils.getImageFetcher(getActivity(), true);
        imgProduct = view.findViewById(R.id.imgProduct);
        qtyList = getResources().getStringArray(R.array.qty);
        productName = view.findViewById(R.id.productName);
        productColor = view.findViewById(R.id.productColor);
        txtRating = view.findViewById(R.id.txtRating);
        txtPrice = view.findViewById(R.id.txtProductPrice);
        txtOrgPrice = view.findViewById(R.id.txtProductPriceOrg);
        txtOff = view.findViewById(R.id.txtOff);
        txtWarrenty = view.findViewById(R.id.txtWarrenty);
        qtySpinner = view.findViewById(R.id.qtySpinner);
        txtQty = view.findViewById(R.id.txtQty);
        txtDelivery = view.findViewById(R.id.txtDelivery);
        actionChange = view.findViewById(R.id.actionChange);
        txtName = view.findViewById(R.id.txtName);
        txtAddress = view.findViewById(R.id.txtAddress);
        txtState_pinCode = view.findViewById(R.id.txtState_pinCode);
        txtMobNo = view.findViewById(R.id.txtMobNo);
        txtPriceItem = view.findViewById(R.id.txtPriceItem);
        txtPriceValue = view.findViewById(R.id.txtPriceValue);
        txtDeliveryValue = view.findViewById(R.id.txtDeliveryValue);
        txtOfferValue = view.findViewById(R.id.txtOfferValue);
        txtPayValue = view.findViewById(R.id.txtPayValue);
        actionContinue = view.findViewById(R.id.actionContinue);
        txtActionChange = view.findViewById(R.id.txtActionChange);
        actionQty = view.findViewById(R.id.actionQty);
        user = PreferenceManager.getUserInfo(getActivity());
        if(presenter == null){
            presenter = new DeliverPresenter(this);
        }
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.qty, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        qtySpinner.setAdapter(adapter);
        actionQty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                qtySpinner.performClick();
            }
        });
        actionContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(user.getAddressId())) {
                    Bundle bundle = new Bundle();
                    product.setDeliveryInfo(address.getDeliveryInfo());
                    bundle.putParcelable("product" ,product);
                    bundle.putBoolean("isProductList" ,false);
                    mListener.navigate(PaymentFragment.class.getSimpleName(), bundle);
                } else {
                    Toast.makeText(getContext() ,getString(R.string.please_add_address),Toast.LENGTH_SHORT).show();
                }
            }
        });

        actionChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                if (address == null) {
                    bundle.putBoolean("isForAdd", true);
                    mListener.navigate(AddressChangeFragment.class.getSimpleName(), bundle);
                } else {
                    mListener.navigate(ChooseAddressFragment.class.getSimpleName(), null);
                }

            }
        });

        if (savedInstanceState == null) {
            if (getArguments().containsKey("product")) {
                this.product = getArguments().getParcelable("product");
                populateData(product);
            }
        } else {
            if (this.product != null) {
                getActivity().setTitle(getString(R.string.delivery));
                populateData(product);
            }
        }
        return view;
    }

    private void populateData(final Product product) {
        imageFetcher.loadImage(product.getImgList().get(0), imgProduct);
        productName.setText(product.getName());
        if (!TextUtils.isEmpty(product.getColor())) {
            productColor.setText(String.format(getString(R.string.prodColor), product.getColor()));
        }
        txtRating.setVisibility(product.getRating() > 0 ? View.VISIBLE : View.GONE);
        txtRating.setText(String.valueOf(product.getRating()));
        txtPrice.setText(String.format(getString(R.string.currency), String.valueOf(Utils.getDiscountValue(product.getAmount(), product.getDisCount()))));
        txtOrgPrice.setText(String.valueOf(product.getAmount()));
        txtOrgPrice.setPaintFlags(txtOrgPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        txtOff.setText(String.format(getString(R.string.offText), String.valueOf(product.getDisCount())));
        txtWarrenty.setText(String.format(getString(R.string.warrenty), product.getWarrenty()));
        qtySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                product.setQty(Integer.valueOf(qtyList[position]));
                txtQty.setText(String.format(getString(R.string.qty), qtyList[position]));
                price.setPrice(String.valueOf(Utils.getDiscountValue(product.getAmount(), product.getDisCount())));
                price.setDelivery("0");
                price.setItemCount(product.getQty());
                price.setPay(String.valueOf(Utils.getDiscountValue(product.getAmount(), product.getDisCount())));
                populatePriceDetails(price);
                mListener.setProduct(product);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        qtySpinner.setSelection(Arrays.asList(qtyList).indexOf(String.valueOf(product.getQty())));
        isTaskRunning = true;
        presenter.getPreviousAddress(getActivity(), PreferenceManager.getUserInfo(getActivity()).getAddressId());
        mListener.setProduct(product);

    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

    }

    private void populatePriceDetails(Price price) {
        txtPriceItem.setText(String.format(getString(R.string.price_items), String.valueOf(price.getItemCount())));
        String mPrice = String.valueOf(Integer.valueOf(price.getPrice()) * price.getItemCount());
        price.setPrice(mPrice);
        txtPriceValue.setText(String.format(getString(R.string.currency), mPrice));
        txtDeliveryValue.setText(price.getDelivery().equals("0") ? "Free" : String.format(getString(R.string.currency), price.getDelivery()));
        int total = price.getDelivery().equals("0") ? Integer.valueOf(mPrice) : Integer.valueOf(mPrice) - Integer.valueOf(price.getDelivery());
        price.setPay(String.valueOf(total));
        txtPayValue.setText(String.format(getString(R.string.currency), String.valueOf(total)));
    }

    private void populateAddress(Address address) {
        this.address = address;
        if (address != null) {
            txtName.setVisibility(View.VISIBLE);
            txtAddress.setVisibility(View.VISIBLE);
            txtActionChange.setText(getString(R.string.change));
            txtName.setText(String.format(getString(R.string.concatinate), address.getFirstName(), address.getLastName()));
            String fullAddress = String.format(getString(R.string.formAddress), address.getDoorNo(), address.getArea(), address.getCity(), address.getState(), address.getState(), address.getPinCode(), address.getAltNo());
            txtAddress.setText(fullAddress);
            txtDelivery.setVisibility(View.VISIBLE);
            txtDelivery.setText(Utils.getDurationDay(getActivity() ,address.getDeliveryInfo()));
        } else {
            txtActionChange.setText(getString(R.string.add));
            txtName.setVisibility(View.GONE);
            txtAddress.setVisibility(View.GONE);
            txtDelivery.setVisibility(View.GONE);
        }

    }

    @Override
    public void showLoader() {
        mListener.showLoader();
    }

    @Override
    public void hideLoader() {
        mListener.hideLoader();
    }

    @Override
    public void updateAddress(Address address) {
        isTaskRunning = false;
        populateAddress(address);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ProductHomeListener) {
            mListener = (ProductHomeListener) context;
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (mListener != null) {
            if (isTaskRunning) {
                mListener.showLoader();
            }
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener.hideLoader();
        mListener = null;
    }

    @Override
    public void noInternet() {

    }
}
