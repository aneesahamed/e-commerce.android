package com.example.e_commerce.ui.login;

import android.content.Context;
import android.text.TextUtils;

import com.example.e_commerce.common.Utils;
import com.example.e_commerce.ui.signup.SignUpHelper;

public class LoginPresenter {
    LoginInteractor interactor;
    private SignUpHelper signUpHelper;
    LoginPresenter(LoginInteractor interactor){
        this.interactor = interactor;
    }

    public void setSignUpHelper(SignUpHelper signUpHelper){
        this.signUpHelper = signUpHelper;
    }
    public void checkUser(Context context ,String mobileNo){
        if(isValid(mobileNo)){
            if(Utils.isConnectedToInternet(context)){
                if(signUpHelper != null){
                    signUpHelper.checkUser("+91"+mobileNo);
                }
            }
            else {
                interactor.noInternet();
            }
        }
    }
    private boolean isValid(String  mobile){
        boolean isValid = true;
        if(TextUtils.isEmpty(mobile)){
            isValid = false;
            interactor.showMobileFieldError(true);
        }
        else if(mobile.length()!=10){
            isValid = false;
            interactor.showMobileFieldError(false);
        }
        return isValid;
    }

}
