package com.example.e_commerce.ui.Order.orderDetail;


import android.graphics.Paint;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.e_commerce.R;
import com.example.e_commerce.common.AppController;
import com.example.e_commerce.common.PreferenceManager;
import com.example.e_commerce.common.Utils;
import com.example.e_commerce.common.imageUtils.ImageFetcher;
import com.example.e_commerce.model.entity.Address;
import com.example.e_commerce.model.entity.Order;
import com.example.e_commerce.model.entity.Price;
import com.example.e_commerce.model.entity.Product;
import com.example.e_commerce.model.entity.User;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrderDetailFragment extends Fragment implements  OrderDetailInteractor {


    private TextView productName, productColor, txtPrice, txtOrgPrice, txtQty ,txtName,txtOrderId ,txtOrderedInfo,txtPackedInfo,txtDeliveredInfo;
    private TextView txtAddress, txtState_pinCode, txtMobNo, txtPriceItem, txtPriceValue, txtDeliveryValue, txtOfferValue, txtPayValue;
    ImageView imgProduct;
    private FrameLayout actionCancel,actionChange;
    TextView  actionReview;
    private ImageFetcher imageFetcher;
    private Order order;
    private User user;
    private View packedDot,deliveredDot ,packedPath,deliveredPath,orderedPath;
    private Address address;
    private OrderDetailPresenter presenter;
    public OrderDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_order_detail, container, false);
        productName = view.findViewById(R.id.productName);
        productColor = view.findViewById(R.id.productColor);
        imgProduct = view.findViewById(R.id.imgProduct);
        packedDot = view.findViewById(R.id.packedDot);
        txtOrderId = view.findViewById(R.id.txtOrderId);
        packedPath = view.findViewById(R.id.packedPath);
        orderedPath = view.findViewById(R.id.orderPath);
        deliveredDot = view.findViewById(R.id.deliveredDot);
        deliveredPath = view.findViewById(R.id.deliveredPath);
        txtPrice = view.findViewById(R.id.txtProductPrice);
        txtOrgPrice = view.findViewById(R.id.txtProductPriceOrg);
        txtQty = view.findViewById(R.id.txtQty);
        txtName = view.findViewById(R.id.txtName);
        txtAddress = view.findViewById(R.id.txtAddress);
        actionCancel = view.findViewById(R.id.actionCancel);
        actionChange = view.findViewById(R.id.actionChange);
        actionChange.setVisibility(View.INVISIBLE);
        actionReview =  view.findViewById(R.id.actionReview);
        txtState_pinCode = view.findViewById(R.id.txtState_pinCode);
        txtMobNo = view.findViewById(R.id.txtMobNo);
        txtPriceItem = view.findViewById(R.id.txtPriceItem);
        txtPriceValue = view.findViewById(R.id.txtPriceValue);
        txtDeliveryValue = view.findViewById(R.id.txtDeliveryValue);
        txtOfferValue = view.findViewById(R.id.txtOfferValue);
        txtPayValue = view.findViewById(R.id.txtPayValue);
        txtOrderedInfo = view.findViewById(R.id.txtOrderedInfo);
        txtPackedInfo = view.findViewById(R.id.txtPackedInfo);
        txtDeliveredInfo = view.findViewById(R.id.txtDeliveryInfo);
        imageFetcher = Utils.getImageFetcher(getActivity() ,true);
        actionReview.setVisibility(View.GONE);
        user = PreferenceManager.getUserInfo(getActivity());
        presenter = new OrderDetailPresenter(this);
        if(savedInstanceState == null){
            order = getArguments().getParcelable("order");
            populateProductInfo(order.getProduct());
            presenter.getAddress(getActivity() ,order.getAddressId());
        }
        else {
            populateProductInfo(order.getProduct());
            populateAddress(address);
        }

        actionCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int i = AppController.getInstance().getDataBaseHelper().cancelOrder(user.getUid() ,order.getOrderId());
                if(i>0){
                    setRetainInstance(false);
                    getActivity().getSupportFragmentManager().popBackStack();
                }
            }
        });
        return view;
    }

    private void populateProductInfo(Product product){
        imageFetcher.loadImage(product.getImgList().get(0), imgProduct);
        productName.setText(product.getName());
        if (!TextUtils.isEmpty(product.getColor())) {
            productColor.setText(String.format(getString(R.string.prodColor), product.getColor()));
        }
        txtPrice.setText(String.format(getString(R.string.currency), String.valueOf(Utils.getDiscountValue(product.getAmount(), product.getDisCount()))));
        txtOrgPrice.setText(String.valueOf(product.getAmount()));
        txtOrgPrice.setPaintFlags(txtOrgPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        txtQty.setText(String.format(getString(R.string.qty), String.valueOf(product.getQty())));
        txtOrderId.setText(String.format(getString(R.string.orderId),order.getOrderId().substring(0,8)));
        Price price = new Price();
        price.setPrice(String.valueOf(Utils.getDiscountValue(product.getAmount(), product.getDisCount())));
        price.setDelivery("0");
        price.setItemCount(product.getQty());
        price.setPay(String.valueOf(Utils.getDiscountValue(product.getAmount(), product.getDisCount())));
        populatePriceDetails(price ,order.getCouponDisCount());
        populateTimeLine(order);

    }

    private void populatePriceDetails(Price price ,int couponDisCount) {
        txtPriceItem.setText(String.format(getString(R.string.price_items), String.valueOf(price.getItemCount())));
        String mPrice = String.valueOf(Integer.valueOf(price.getPrice()) * price.getItemCount());
        String priceFromCoupon = String.valueOf(Utils.getDiscountValue(Integer.valueOf(mPrice),couponDisCount));
        price.setPrice(priceFromCoupon);
        txtPriceValue.setText(String.format(getString(R.string.currency), priceFromCoupon));
        txtDeliveryValue.setText(price.getDelivery().equals("0") ? "Free" : String.format(getString(R.string.currency), price.getDelivery()));
        int total = price.getDelivery().equals("0") ? Integer.valueOf(mPrice) : Integer.valueOf(mPrice) - Integer.valueOf(price.getDelivery());
        price.setPay(String.valueOf(total));
        txtPayValue.setText(String.format(getString(R.string.currency), String.valueOf(total)));
    }

    private void populateAddress(Address address) {
        if (address != null) {
            txtName.setVisibility(View.VISIBLE);
            txtAddress.setVisibility(View.VISIBLE);
            txtName.setText(String.format(getString(R.string.concatinate), address.getFirstName(), address.getLastName()));
            String fullAddress = String.format(getString(R.string.formAddress), address.getDoorNo(), address.getArea(), address.getCity(), address.getState(), address.getState(), address.getPinCode(), address.getAltNo());
            txtAddress.setText(fullAddress);
        } else {
            txtName.setVisibility(View.GONE);
            txtAddress.setVisibility(View.GONE);
        }

    }

    private void populateTimeLine(Order order){
        txtOrderedInfo.setText(String.format(getString(R.string.ordered), Utils.getDateFromTimeMillis(order.getOrderedTime() ,Utils.FORMAT_DATE_D_MMM_YYYY)));
        orderedPath.setBackground(ContextCompat.getDrawable(getActivity() ,R.color.baseTextColor));
        packedDot.setBackgroundResource(R.drawable.bg_time_line_dot_grey);
        packedPath.setBackground(ContextCompat.getDrawable(getActivity() ,R.color.baseTextColor));
        deliveredPath.setBackground(ContextCompat.getDrawable(getActivity() ,R.color.baseTextColor));
        deliveredDot.setBackgroundResource(R.drawable.bg_time_line_dot_grey);
        if(!TextUtils.isEmpty(order.getPackedStatus())){
            txtPackedInfo.setText(String.format(getString(R.string.packed), Utils.getDateFromTimeMillis(order.getPackedTime() ,Utils.FORMAT_DATE_D_MMM_YYYY)));
            orderedPath.setBackground(ContextCompat.getDrawable(getActivity() ,R.color.colorGreen));
            packedDot.setBackgroundResource(R.drawable.bg_time_line_dot);
        }
        if(!TextUtils.isEmpty(order.getDeliveryStatus())){
            actionChange.setVisibility(View.GONE);
            txtDeliveredInfo.setText(String.format(getString(R.string.delivered), Utils.getDateFromTimeMillis(order.getDeliveryTime() ,Utils.FORMAT_DATE_D_MMM_YYYY)));
            packedDot.setBackgroundResource(R.drawable.bg_time_line_dot);
            deliveredDot.setBackgroundResource(R.drawable.bg_time_line_dot);
            deliveredPath.setBackground(ContextCompat.getDrawable(getActivity() ,R.color.colorGreen));
        }
    }
    @Override
    public void showLoader() {

    }

    @Override
    public void hideLoader() {

    }

    @Override
    public void noInternet() {

    }

    @Override
    public void showAddress(Address address) {
        this.address = address;
        populateAddress(address);
    }
}
