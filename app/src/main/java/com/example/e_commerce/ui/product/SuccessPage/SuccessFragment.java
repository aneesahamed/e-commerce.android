package com.example.e_commerce.ui.product.SuccessPage;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.e_commerce.R;
import com.example.e_commerce.ui.Order.OrderActivity;

import androidx.fragment.app.Fragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class SuccessFragment extends Fragment {


    TextView gotoMyorder;

    public SuccessFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_success, container, false);
        gotoMyorder = view.findViewById(R.id.gotoMyOrder);

        gotoMyorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity() , OrderActivity.class);
                startActivity(intent);
            }
        });
        return view;
    }

}
