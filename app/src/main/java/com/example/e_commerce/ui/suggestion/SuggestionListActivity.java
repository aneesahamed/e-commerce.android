package com.example.e_commerce.ui.suggestion;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.MenuItemCompat;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.AsyncTaskLoader;
import androidx.loader.content.Loader;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.example.e_commerce.R;
import com.example.e_commerce.adapter.SuggestionListAdapter;
import com.example.e_commerce.common.AppConstant;
import com.example.e_commerce.common.AppController;
import com.example.e_commerce.common.Utils;
import com.example.e_commerce.common.imageUtils.ImageCache;
import com.example.e_commerce.common.imageUtils.ImageFetcher;
import com.example.e_commerce.model.entity.Category;
import com.example.e_commerce.ui.BaseActivity;
import com.example.e_commerce.ui.home.HomeActivity;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import static com.example.e_commerce.common.AppConstant.IMAGE_CACHE_DIR;

public class SuggestionListActivity extends BaseActivity implements LoaderManager.LoaderCallbacks<List<Category>>, SuggestionInteractor {

    SuggestionListAdapter adapter;
    ArrayList<Category> categoryList = new ArrayList<>();
    ArrayList<Category> tempList = new ArrayList<>();
    ArrayList<Category> searchedList = new ArrayList<>();
    RecyclerView suggestionList;
    ProgressBar progressBar;
    ImageFetcher imageFetcher;
    CustomLoader customLoader;
    SearchView searchView;
    String searchKeyWord = AppConstant.EMPTY;
    MenuItem searchItem;
    private boolean isFromSuggestion;
    boolean isSearchViewCollapsed = true;
    SuggestionPresenter presenter;
    final Object lock = new Object();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suggestion_list);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (savedInstanceState == null) {
            isFromSuggestion = getIntent().getBooleanExtra("isFromSuggestion", false);
        } else {
            isFromSuggestion = savedInstanceState.getBoolean("isFromSuggestion");
        }
        suggestionList = findViewById(R.id.suggestionList);
        progressBar = findViewById(R.id.progressBar);
        presenter = new SuggestionPresenter(this);
        imageFetcher = Utils.getImageFetcher(this, true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        adapter = new SuggestionListAdapter(this, isFromSuggestion, categoryList, imageFetcher);
        suggestionList.setLayoutManager(linearLayoutManager);
        suggestionList.setAdapter(adapter);


        if (savedInstanceState != null) {
            isFromSuggestion = savedInstanceState.getBoolean("isFromSuggestion");
            isSearchViewCollapsed = savedInstanceState.getBoolean("isSearchCollapsed");
            searchKeyWord = savedInstanceState.getString("searchedKeyword");
            if (!savedInstanceState.getBoolean("isSearchCollapsed")) {
                invalidateOptionsMenu();
                searchedList.clear();
                searchedList.addAll(savedInstanceState.<Category>getParcelableArrayList("searchedList"));
                adapter.setList(searchedList);
            } else {
                getSupportLoaderManager().initLoader(0, null, this);
            }
        } else {
            getSupportLoaderManager().initLoader(0, null, this);
        }
        toolbar.setNavigationIcon(isFromSuggestion?R.drawable.ic_close:R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isFromSuggestion){
                    presenter.registerUserInterest(categoryList, SuggestionListActivity.this);
                }
                else {
                    finish();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_items, menu);
        return true;
    }

    private void queryKeywords(String keyword) {

        synchronized (lock) {
            searchedList.clear();
            for (Category category : tempList) {
                if (category.getName().toLowerCase().startsWith(keyword.toLowerCase())) {
                    searchedList.add(category);
                }
            }
            adapter.setList(searchedList);
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.action_cart).setVisible(false);
        menu.findItem(R.id.action_wish_list).setVisible(false);
        menu.findItem(R.id.action_search).setVisible(false);
        menu.findItem(R.id.action_searchClass).setVisible(true);

        final MenuItem done = menu.findItem(R.id.action_done);
        done.setVisible(isFromSuggestion);
        searchItem = menu.findItem(R.id.action_searchClass);
        searchView = (SearchView) searchItem.getActionView();
        if (!isSearchViewCollapsed) {
            done.setVisible(false);
            searchItem.expandActionView();
            searchView.setQuery(searchKeyWord, false);
        }
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                searchItem.collapseActionView();
                isSearchViewCollapsed = true;
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                searchKeyWord = s;
                if (!TextUtils.isEmpty(s)) {
                    queryKeywords(s);
                } else {
                    getSupportLoaderManager().initLoader(0, null, SuggestionListActivity.this);
                }
                return false;
            }
        });
        searchItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                done.setVisible(false);
                isSearchViewCollapsed = false;
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                if (adapter != null) {
                    searchedList.clear();
                    isSearchViewCollapsed = true;
                    getSupportLoaderManager().initLoader(0, null, SuggestionListActivity.this);
                }
                done.setVisible(true);
                return true;
            }
        });
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_done) {
            presenter.registerUserInterest(categoryList, this);

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList("searchedList", searchedList);
        outState.putBoolean("isSearchCollapsed", isSearchViewCollapsed);
        outState.putString("searchedKeyword", searchKeyWord);
    }

    @NonNull
    @Override
    public Loader<List<Category>> onCreateLoader(int id, @Nullable Bundle args) {
        customLoader = new CustomLoader(this, getIntent().getStringExtra("gcId"), progressBar);
        return customLoader;
    }

    @Override
    public void onLoadFinished(@NonNull Loader<List<Category>> loader, List<Category> list) {
        categoryList.clear();
        categoryList.addAll(list);
        adapter.notifyDataSetChanged();
        tempList.clear();
        tempList.addAll(list);
    }

    @Override
    public void onLoaderReset(@NonNull Loader<List<Category>> loader) {

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void showLoader() {
        showProgressDialog();
    }

    @Override
    public void hideLoader() {
        hideProgressDialog();
    }

    @Override
    public void noInternet() {

    }

    @Override
    public void launchNextScreen() {
        imageFetcher.clearCache();
        imageFetcher.closeCache();
        Intent intent = new Intent(this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public static class CustomLoader extends AsyncTaskLoader<List<Category>> {
        private List<Category> list;
        private boolean isTaskRunning;
        private Context context;
        private String gcId;
        private ProgressBar progressBar;

        CustomLoader(Context context, String gcId, ProgressBar progressBar) {
            super(context);
            this.context = context;
            this.progressBar = progressBar;
            this.gcId = gcId;
        }

        public void setProgressBar(ProgressBar progressBar) {
            this.progressBar = progressBar;
        }

        @Override
        protected void onStartLoading() {
            if (list != null) {
                progressBar.setVisibility(View.INVISIBLE);
                deliverResult(list);
            } else {
                progressBar.setVisibility(View.VISIBLE);
                forceLoad();
            }
        }

        @Nullable
        @Override
        public List<Category> loadInBackground() {

            return AppController.getInstance().getDataBaseHelper().getCategories(gcId);
        }

        @Override
        public void deliverResult(@Nullable List<Category> data) {
            progressBar.setVisibility(View.INVISIBLE);
            isTaskRunning = false;
            list = data;
            super.deliverResult(data);
        }
    }
}
