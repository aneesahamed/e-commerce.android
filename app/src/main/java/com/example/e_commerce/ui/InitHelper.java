package com.example.e_commerce.ui;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;

import com.example.e_commerce.MainActivity;
import com.example.e_commerce.common.AppController;

import java.lang.ref.WeakReference;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class InitHelper extends Fragment {

    private FragmentListener listener;
    private boolean isTaskRunning;

    public interface FragmentListener {
        void onLoadComplete(boolean mBoolean);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    public void init(MainActivity activity) {
        isTaskRunning = true;
        new InitDatabase(this).execute();

    }

    public void completeInitialization(boolean result){
        if (listener != null) {
            listener.onLoadComplete(result);
        }
    }
    public static class InitDatabase extends AsyncTask<Void, Void, Boolean> {

        private final WeakReference<InitHelper> initHelperWeakReference;
        InitDatabase(InitHelper initHelper) {
            initHelperWeakReference = new WeakReference<>(initHelper);
        }

        @Override
        protected void onPreExecute() {
            //activityRef.get().showProgressDialog();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            return AppController.getInstance().getDataBaseHelper().dataInitialization();
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            initHelperWeakReference.get().completeInitialization(aBoolean);
        }
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getActivity() != null) {
            if (isTaskRunning) {
                ((MainActivity) getActivity()).showProgressDialog();
            } else {
                ((MainActivity) getActivity()).hideProgressDialog();
            }
        }

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof FragmentListener) {
            listener = (FragmentListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }
}
