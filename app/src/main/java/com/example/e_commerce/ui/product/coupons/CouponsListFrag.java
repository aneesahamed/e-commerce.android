package com.example.e_commerce.ui.product.coupons;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.e_commerce.R;
import com.example.e_commerce.adapter.CouponListAdapter;
import com.example.e_commerce.common.PreferenceManager;
import com.example.e_commerce.common.Utils;
import com.example.e_commerce.common.imageUtils.ImageFetcher;
import com.example.e_commerce.model.entity.Coupon;
import com.example.e_commerce.model.entity.User;
import com.example.e_commerce.ui.common.ClickListener;
import com.example.e_commerce.ui.product.ProductActivity;
import com.example.e_commerce.ui.product.ProductHomeListener;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * A simple {@link Fragment} subclass.
 */
public class CouponsListFrag extends Fragment implements CouponInteractor {


    private ProgressBar progressBar;
    private RecyclerView couponsListRecycler;
    private List<Coupon> couponsList = new ArrayList<>();
    private CouponPresenter presenter;
    private CouponListAdapter adapter;
    private ImageFetcher imageFetcher;
    private boolean isFromNav;
    private ProductHomeListener mListener;
    TextView txtEmptyState;
    User user;
    public CouponsListFrag() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_coupons_list, container, false);
        progressBar = view.findViewById(R.id.progressBar);
        txtEmptyState = view.findViewById(R.id.txtEmptyState);
        couponsListRecycler = view.findViewById(R.id.couponList);
        imageFetcher = Utils.getImageFetcher(getActivity() ,true);
        presenter = new CouponPresenter(this);
        couponsListRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        user = PreferenceManager.getUserInfo(getContext());
        if(getArguments() != null){
            isFromNav = getArguments().containsKey("isFromNav");
        }
        adapter = new CouponListAdapter(getActivity(), isFromNav ,imageFetcher, couponsList, new ClickListener() {
            @Override
            public void onItemClick(Object object) {
                Coupon coupon = (Coupon)object;
                if(coupon != null){
                    mListener.updateCoupons(coupon);
                }
            }
        });

        couponsListRecycler.setAdapter(adapter);
        if(savedInstanceState == null){
            presenter.getCouponList(getContext() ,user.getUid());
        }else {
            txtEmptyState.setVisibility(couponsList.size()>0?View.GONE:View.VISIBLE);
        }
        return view;
    }


    @Override
    public void onResult(List<Coupon> list) {
        if(list != null && list.size()>0){
            couponsList.clear();
            couponsList.addAll(list);
            adapter.notifyDataSetChanged();
            txtEmptyState.setVisibility(View.GONE);
        }
        else {
            txtEmptyState.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(getActivity() != null){
            getActivity().setTitle(getString(R.string.coupons));
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ProductHomeListener) {
            mListener = (ProductHomeListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void showLoader() {
        if(progressBar != null){
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideLoader() {
        if(progressBar != null){
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void noInternet() {

    }
}
