package com.example.e_commerce.ui.login;

import com.example.e_commerce.ui.signup.SignUpInteractor;

public interface LoginInteractor {
    void launchNextScreen();
    void showMobileFieldError(boolean isEmpty);
    void noInternet();
}
