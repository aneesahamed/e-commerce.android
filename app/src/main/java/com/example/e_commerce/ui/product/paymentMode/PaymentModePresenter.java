package com.example.e_commerce.ui.product.paymentMode;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;

import com.example.e_commerce.common.AppConstant;
import com.example.e_commerce.common.AppController;
import com.example.e_commerce.common.PreferenceManager;
import com.example.e_commerce.common.imageUtils.AsyncTask;
import com.example.e_commerce.model.entity.Address;
import com.example.e_commerce.model.entity.AddressContract;
import com.example.e_commerce.model.entity.Price;
import com.example.e_commerce.model.entity.Product;
import com.example.e_commerce.model.entity.User;

import java.util.List;

import static com.example.e_commerce.model.DBConstant.ADDRESS_ID;
import static com.example.e_commerce.model.DBConstant.PINCODE;
import static com.example.e_commerce.model.DBConstant.UID;

public class PaymentModePresenter {
    PaymentModeInteractor interactor;

    PaymentModePresenter(PaymentModeInteractor interactor) {
        this.interactor = interactor;
    }

    public void placeOrder(Context context ,User user, String cardNo, String month, String year, String cvv, Bundle args) {
        if (isValid(cardNo, month, year, cvv)) {
            interactor.showLoader();
            new PlaceOrder(context ,user ,interactor).execute(args);
        }
    }

    private boolean isValid(String cardNo, String month, String year, String cvv) {
        boolean isValid = true;
        if (TextUtils.isEmpty(cardNo)) {
            isValid = false;
            interactor.showFieldError(0);
        } else if (TextUtils.isEmpty(month)) {
            isValid = false;
            interactor.showFieldError(1);
        } else if (TextUtils.isEmpty(year)) {
            isValid = false;
            interactor.showFieldError(2);
        } else if (TextUtils.isEmpty(cvv)) {
            isValid = false;
            interactor.showFieldError(3);
        }
        return isValid;
    }

    public static class PlaceOrder extends AsyncTask<Bundle, Void, Void> {

        User user;
        PaymentModeInteractor interactor;
        Context context;
        PlaceOrder(Context context ,User user ,PaymentModeInteractor interactor) {
            this.interactor = interactor;
            this.user = user;
            this.context = context;
        }

        @Override
        protected Void doInBackground(Bundle... bundles) {
            Bundle bundle = bundles[0];
            String selection = UID + " =? AND " + ADDRESS_ID + " =?";
            Uri uri = AddressContract.AddressEntry.getAddressUri(0);
            Cursor cursor = context.getContentResolver().query(uri, null, selection, new String[]{user.getUid(), user.getAddressId()}, null);
            String pinCode = AppConstant.EMPTY;
            if (cursor != null && cursor.moveToFirst()) {
                pinCode = cursor.getString(cursor.getColumnIndex(PINCODE));
            }
            long duration = AppController.getInstance().getDeliveryDuration(pinCode,"180001");
            if(bundle.getBoolean("isProductList",false)){
                Price price = bundle.getParcelable("price");
                List<Product> list = bundle.getParcelableArrayList("productList");
                for(Product product : list){
                    product.setDeliveryInfo(duration);
                }
                AppController.getInstance().getDataBaseHelper().placeOrder(user ,list,price);
                AppController.getInstance().getDataBaseHelper().redeemCoupon(user.getUid() ,price.getCouponId());
            }
            else {
                Product product = bundle.getParcelable("product");
                product.setDeliveryInfo(duration);
                AppController.getInstance().getDataBaseHelper().placeOrder(user ,product ,product.getPrice());
                AppController.getInstance().getDataBaseHelper().redeemCoupon(user.getUid() ,product.getPrice().getCouponId());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
           if(interactor != null){
               interactor.hideLoader();
               interactor.launchNextScreen();
           }
        }
    }
}
