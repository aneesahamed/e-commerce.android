package com.example.e_commerce.ui.signup;

import com.example.e_commerce.ui.BaseInteractor;

public interface SignUpInteractor extends BaseInteractor {
    @Override
    void showLoader();

    @Override
    void hideLoader();

    void showMobileFieldError(boolean isEmpty);
    void showNameFieldError();
    void showEmailFieldError(boolean isEmpty);
    void launchNextScreen();
}
