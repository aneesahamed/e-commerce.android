package com.example.e_commerce.ui.product;

import com.example.e_commerce.model.entity.Coupon;
import com.example.e_commerce.model.entity.Product;

public interface ProductHomeListener {
    void showLoader();
    void hideLoader();
    void noInternet();
    void setProduct(Product product);
    void navigate(String tag ,Object object);
    void openDialogVariant();
    void updateCoupons(Coupon coupon);
}
