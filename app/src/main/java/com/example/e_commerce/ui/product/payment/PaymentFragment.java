package com.example.e_commerce.ui.product.payment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.e_commerce.R;
import com.example.e_commerce.common.Utils;
import com.example.e_commerce.model.entity.Coupon;
import com.example.e_commerce.model.entity.Price;
import com.example.e_commerce.model.entity.Product;
import com.example.e_commerce.ui.product.ProductHomeListener;
import com.example.e_commerce.ui.product.coupons.CouponsListFrag;
import com.example.e_commerce.ui.product.paymentMode.PaymentModeFrag;

import org.w3c.dom.Text;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class PaymentFragment extends Fragment {

    TextView txtPriceItem, txtPriceValue, txtDeliveryValue, txtOfferValue, txtPayValue, txtOfferTitle, txtCouponName;
    RelativeLayout rlCoupons, rlCouponsName;
    FrameLayout actionContinue;
    ImageView imgCancel;
    Product product;
    RadioGroup radioGroup;
    RadioButton rdCod, rdEmi, rdNet, rdDebit;
    Price price = new Price();
    ProductHomeListener mListener;
    boolean isBulkList;
    String payMode;
    ArrayList<Product> productList = new ArrayList<>();
    Coupon coupon;
    private boolean isProductList;

    public PaymentFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_payment, container, false);
        txtPriceItem = view.findViewById(R.id.txtPriceItem);
        txtPriceValue = view.findViewById(R.id.txtPriceValue);
        actionContinue = view.findViewById(R.id.actionContinue);
        txtDeliveryValue = view.findViewById(R.id.txtDeliveryValue);
        txtOfferValue = view.findViewById(R.id.txtOfferValue);
        txtPayValue = view.findViewById(R.id.txtPayValue);
        txtOfferTitle = view.findViewById(R.id.txtOfferTitle);
        rlCoupons = view.findViewById(R.id.rlCoupons);
        rlCouponsName = view.findViewById(R.id.rlCouponsName);
        imgCancel = view.findViewById(R.id.imgCancel);
        txtCouponName = view.findViewById(R.id.txtCouponName);
        radioGroup = view.findViewById(R.id.radioGroups);
        rdCod = view.findViewById(R.id.rdCOD);
        rdDebit = view.findViewById(R.id.rdCredit);
        rdEmi = view.findViewById(R.id.rdEMI);
        rdNet = view.findViewById(R.id.rdNetBank);
        rlCoupons.setVisibility(View.VISIBLE);
        rlCouponsName.setVisibility(View.GONE);
        txtOfferTitle.setVisibility(View.GONE);
        txtOfferValue.setVisibility(View.GONE);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                setPayMode(checkedId);
            }
        });
        rlCoupons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.navigate(CouponsListFrag.class.getSimpleName(), null);
            }
        });

        imgCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rlCoupons.setVisibility(View.VISIBLE);
                rlCouponsName.setVisibility(View.GONE);
                txtOfferTitle.setVisibility(View.GONE);
                txtOfferValue.setVisibility(View.GONE);
                if (!isProductList) {
                    populatePriceDetails(price);
                } else {
                    populatePriceList(price);
                }
                coupon = null;
            }
        });

        actionContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(payMode.equals("C")){
                    Bundle args = new Bundle();
                    price.setPayMode(payMode);
                    args.putBoolean("isProductList", isProductList);
                    if (coupon != null) {
                        price.setCouponId(coupon.getCouponId());
                    }
                    if (isProductList) {
                        args.putParcelableArrayList("productList", productList);
                        args.putParcelable("price", price);
                    } else {
                        product.setPrice(price);
                        args.putParcelable("product", product);
                    }

                    mListener.navigate(PaymentModeFrag.class.getSimpleName(), args);
                }
            }
        });

        if (savedInstanceState == null) {
            rdCod.setChecked(true);
            isProductList = getArguments().getBoolean("isProductList");
            if (getArguments().containsKey("product")) {
                product = getArguments().getParcelable("product");
                populateData(product);
            } else if (getArguments().containsKey("productList")) {
                productList = getArguments().getParcelableArrayList("productList");
                price = getArguments().getParcelable("price");
                populatePriceList(price);
            }
        } else {
            if (this.product != null) {
                populateData(this.product);
            } else {
                populatePriceList(price);
            }
            if (this.coupon != null) {
                applyCoupons(coupon);
            }
            payMode = savedInstanceState.getString("pMode");
            setPayMode(payMode);
            getActivity().setTitle(getString(R.string.payment));
        }
        return view;
    }

    private void setPayMode(String payMode) {
        switch (payMode) {
            case "C":
                rdCod.setChecked(true);
                break;
            case "D":
                rdDebit.setChecked(true);
                break;
            case "N":
                rdNet.setChecked(true);
                break;
            case "E":
                rdEmi.setChecked(true);
                break;
        }
    }

    private void setPayMode(int id) {
        switch (id) {
            case R.id.rdCOD:
                rdCod.setChecked(true);
                payMode = "C";
                break;
            case R.id.rdCredit:
                rdDebit.setChecked(true);
                payMode = "D";
                break;
            case R.id.rdNetBank:
                rdNet.setChecked(true);
                payMode = "N";
                break;
            case R.id.rdEMI:
                rdEmi.setChecked(true);
                payMode = "E";
                break;
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("pMode", payMode);
    }

    public void applyCoupons(Coupon coupon) {
        this.coupon = coupon;
        rlCoupons.setVisibility(View.GONE);
        rlCouponsName.setVisibility(View.VISIBLE);
        txtCouponName.setText(coupon.getCouponTitle());
        txtOfferTitle.setVisibility(View.VISIBLE);
        txtOfferValue.setVisibility(View.VISIBLE);
        txtOfferValue.setText(String.format(getString(R.string.currency), String.valueOf(Utils.getSavingValue(isProductList ? price.getTotal() : product.getAmount(), coupon.getDiscount()))));
        int appliedCoupon = Utils.getDiscountValue(isProductList ? price.getTotal() : Integer.valueOf(price.getPay()), coupon.getDiscount());
        price.setPay(String.valueOf(appliedCoupon));
        txtPayValue.setText(String.format(getString(R.string.currency), String.valueOf(appliedCoupon)));
    }

    private void populateData(Product product) {
        price.setItemCount(product.getQty());
        price.setPrice(String.valueOf(Utils.getDiscountValue(product.getAmount(), product.getDisCount())));
        price.setDelivery("0");
        price.setItemCount(product.getQty());
        price.setPay(String.valueOf(Utils.getDiscountValue(product.getAmount(), product.getDisCount())));
        populatePriceDetails(price);
    }

    private void populatePriceDetails(Price price) {
        txtPriceItem.setText(String.format(getString(R.string.price_items), String.valueOf(price.getItemCount())));
        String mPrice = String.valueOf(Integer.valueOf(price.getPrice()) * price.getItemCount());
        price.setPrice(mPrice);
        txtPriceValue.setText(String.format(getString(R.string.currency), mPrice));
        txtDeliveryValue.setText(price.getDelivery().equals("0") ? "Free" : String.format(getString(R.string.currency), price.getDelivery()));
        int total = price.getDelivery().equals("0") ? Integer.valueOf(mPrice) : Integer.valueOf(mPrice) - Integer.valueOf(price.getDelivery());
        price.setPay(String.valueOf(total));
        txtPayValue.setText(String.format(getString(R.string.currency), String.valueOf(total)));
    }

    private void populatePriceList(Price price) {
        txtPriceItem.setText(String.format(getString(R.string.price_items), String.valueOf(price.getItemCount())));
        txtPriceValue.setText(String.format(getString(R.string.currency), String.valueOf(price.getTotal())));
        txtDeliveryValue.setText(price.getDelivery().equals("0") ? "Free" : String.format(getString(R.string.currency), price.getDelivery()));
        int total = price.getDelivery().equals("0") ? price.getTotal() : price.getTotal() - Integer.valueOf(price.getDelivery());
        price.setPay(String.valueOf(total));
        txtPayValue.setText(String.format(getString(R.string.currency), String.valueOf(total)));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ProductHomeListener) {
            mListener = (ProductHomeListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

}
