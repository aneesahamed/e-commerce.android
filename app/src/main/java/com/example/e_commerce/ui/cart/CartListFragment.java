package com.example.e_commerce.ui.cart;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.e_commerce.R;
import com.example.e_commerce.adapter.CartListAdapter;
import com.example.e_commerce.common.AppController;
import com.example.e_commerce.common.PreferenceManager;
import com.example.e_commerce.common.Utils;
import com.example.e_commerce.common.imageUtils.ImageFetcher;
import com.example.e_commerce.model.entity.Price;
import com.example.e_commerce.model.entity.Product;
import com.example.e_commerce.model.entity.User;
import com.example.e_commerce.ui.product.ProductHomeListener;
import com.example.e_commerce.ui.product.payment.PaymentFragment;
import com.example.e_commerce.ui.product.productDetail.ProductDetailFragment;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * A simple {@link Fragment} subclass.
 */
public class CartListFragment extends Fragment implements CartInteractor {


    private ProgressBar progressBar;
    private RecyclerView cartListRecycler;
    private RelativeLayout rlPrice;
    private ArrayList<Product> cartList = new ArrayList<>();
    private CartPresenter presenter;
    private CartListAdapter adapter;
    private ImageFetcher imageFetcher;
    private ProductHomeListener mListener;
    private Price price;
    private FrameLayout actionCheckout;
    TextView txtEmptyState, txtPriceItem, txtPriceValue, txtDeliveryValue, txtOfferValue, txtPayValue;
    User user;

    public CartListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_cart_list, container, false);
        progressBar = view.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        rlPrice = view.findViewById(R.id.rlPrice);
        txtEmptyState = view.findViewById(R.id.txtEmptyState);
        txtPriceItem = view.findViewById(R.id.txtPriceItem);
        txtPriceValue = view.findViewById(R.id.txtPriceValue);
        actionCheckout = view.findViewById(R.id.actionCheckout);
        txtDeliveryValue = view.findViewById(R.id.txtDeliveryValue);
        txtOfferValue = view.findViewById(R.id.txtOfferValue);
        txtPayValue = view.findViewById(R.id.txtPayValue);
        cartListRecycler = view.findViewById(R.id.cartListRecycler);
        imageFetcher = Utils.getImageFetcher(getActivity(), true);
        presenter = new CartPresenter(this);
        cartListRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new CartListAdapter(getContext(), imageFetcher, cartList, new CartListAdapter.CartListener() {
            @Override
            public void onClick(View view, int position, Object object) {
                Product product = (Product) object;
                if (view.getId() == R.id.actionRemove) {
                    if (product != null) {
                        int i = AppController.getInstance().getDataBaseHelper().deleteCartItem(user.getUid(), product.getpId());
                        if (i > 0) {
                            cartList.remove(position);
                            adapter.notifyItemRemoved(position);
                            adapter.notifyItemRangeChanged(position, cartList.size() - position);
                            txtEmptyState.setVisibility(cartList.size() > 0 ? View.GONE : View.VISIBLE);
                            rlPrice.setVisibility(cartList.size() > 0 ? View.VISIBLE : View.GONE);
                            actionCheckout.setVisibility(cartList.size() > 0 ? View.VISIBLE : View.GONE);
                            calculateProductPrice();
                        }
                    }
                } else if (view.getId() == R.id.actionBuy) {
                    if (product != null) {
                        mListener.navigate(ProductDetailFragment.class.getSimpleName(), product);
                    }
                }
                else if(view.getId()==R.id.actionQty) {
                    calculateProductPrice();
                }
            }
        });
        cartListRecycler.setAdapter(adapter);
        user = PreferenceManager.getUserInfo(getContext());
        if (savedInstanceState == null) {
            presenter.getCartList(user.getUid());
        } else {
            calculateProductPrice();
            txtEmptyState.setVisibility(cartList.size() > 0 ? View.GONE : View.VISIBLE);
            rlPrice.setVisibility(cartList.size() > 0 ? View.VISIBLE : View.GONE);
        }

        actionCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle args = new Bundle();
                args.putParcelableArrayList("productList" ,cartList);
                args.putParcelable("price" ,price);
                args.putBoolean("isProductList" ,true);
                mListener.navigate(PaymentFragment.class.getSimpleName() ,args);
            }
        });
        return view;
    }

    @Override
    public void onResult(List<Product> list) {
        if (list != null && list.size() > 0) {
            txtEmptyState.setVisibility(View.INVISIBLE);
            cartList.clear();
            cartList.addAll(list);
            adapter.notifyDataSetChanged();
            calculateProductPrice();
        } else {
            txtEmptyState.setVisibility(View.VISIBLE);
            rlPrice.setVisibility(View.GONE);
            actionCheckout.setVisibility(View.GONE);
        }
    }

    private void calculateProductPrice(){
        price = new Price();
        price.setDelivery("0");
        for(Product product : cartList){
            int amount = Utils.getDiscountValue(product.getAmount() ,product.getDisCount())*product.getQty();
            price.setTotal(price.getTotal()+amount);
        }
        price.setItemCount(cartList.size());
        populatePriceDetails(price);
    }

    private void populatePriceDetails(Price price) {
        txtPriceItem.setText(String.format(getString(R.string.price_items), String.valueOf(price.getItemCount())));
        txtPriceValue.setText(String.format(getString(R.string.currency), String.valueOf(price.getTotal())));
        txtDeliveryValue.setText(price.getDelivery().equals("0") ? "Free" : String.format(getString(R.string.currency), price.getDelivery()));
        int total = price.getDelivery().equals("0") ? price.getTotal() : price.getTotal() - Integer.valueOf(price.getDelivery());
        price.setPay(String.valueOf(total));
        txtPayValue.setText(String.format(getString(R.string.currency), String.valueOf(total)));
    }

    @Override
    public void showLoader() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoader() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void noInternet() {

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getActivity() != null) {
            getActivity().setTitle(getString(R.string.myCart));
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ProductHomeListener) {
            mListener = (ProductHomeListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }
}
