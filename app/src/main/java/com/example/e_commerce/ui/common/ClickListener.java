package com.example.e_commerce.ui.common;

public interface ClickListener {
    void onItemClick(Object object);
}
