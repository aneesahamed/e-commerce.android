package com.example.e_commerce.ui.productVariant;

import android.os.AsyncTask;

import com.example.e_commerce.common.AppController;
import com.example.e_commerce.model.entity.Product;

public class VariantPresenter {
    private VariantInteractor interactor;
    VariantPresenter(VariantInteractor interactor){
        this.interactor = interactor;
    }

    public void getProductById(String pid){
        new GetProductById(interactor).execute(pid);
    }

    public void getProductBySpec(Product product){
        new GetProductBySpec(interactor).execute(product);
    }

    public void onDestroy(){
        interactor = null;
    }

    public static class GetProductById extends AsyncTask<String, Void, Product> {
        private VariantInteractor callback;
        GetProductById(VariantInteractor callBack) {
            this.callback = callBack;
        }

        @Override
        protected Product doInBackground(String... strings) {
            return AppController.getInstance().getDataBaseHelper().getProductInfo(strings[0]);
        }

        @Override
        protected void onPostExecute(Product product) {
            if (callback != null) {
                callback.updateUI(product);
            }
        }
    }

    public static class GetProductBySpec extends AsyncTask<Product, Void, Product> {
        private VariantInteractor callback;

        GetProductBySpec(VariantInteractor callBack) {
            this.callback = callBack;
        }

        @Override
        protected Product doInBackground(Product... products) {
           return AppController.getInstance().getDataBaseHelper().getProductBySpecification(products[0]);
        }

        @Override
        protected void onPostExecute(Product product) {
            if (callback != null) {
                callback.updateUI(product);
            }
        }
    }
}
