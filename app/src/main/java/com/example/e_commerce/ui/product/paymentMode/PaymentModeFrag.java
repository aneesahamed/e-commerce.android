package com.example.e_commerce.ui.product.paymentMode;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.example.e_commerce.R;
import com.example.e_commerce.common.PreferenceManager;
import com.example.e_commerce.model.entity.User;
import com.example.e_commerce.ui.product.ProductHomeListener;
import com.example.e_commerce.ui.product.SuccessPage.SuccessFragment;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class PaymentModeFrag extends Fragment implements PaymentModeInteractor {


    private EditText edCardNo, edMM, edYY, edCvv;
    private FrameLayout actionContinue;
    private PaymentModePresenter presenter;
    private ProductHomeListener mListener;
    User user;

    public PaymentModeFrag() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_payment_mode, container, false);
        edCardNo = view.findViewById(R.id.edCardNo);
        edMM = view.findViewById(R.id.edMM);
        edYY = view.findViewById(R.id.edYY);
        edCvv = view.findViewById(R.id.edCvv);
        user = PreferenceManager.getUserInfo(getActivity());
        actionContinue = view.findViewById(R.id.actionContinue);
        presenter = new PaymentModePresenter(this);
        actionContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.placeOrder(getActivity() ,user,
                        edCardNo.getText().toString().trim(),
                        edMM.getText().toString().trim(),
                        edYY.getText().toString().trim(),
                        edCvv.getText().toString().trim(),
                        getArguments());
            }
        });

        if(savedInstanceState != null){
            edCardNo.setText(savedInstanceState.getString("cardNo"));
            edMM.setText(savedInstanceState.getString("mm"));
            edYY.setText(savedInstanceState.getString("yy"));
            edCvv.setText(savedInstanceState.getString("cvv"));
        }
        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("cardNo" ,edCardNo.getText().toString());
        outState.putString("mm",edMM.getText().toString());
        outState.putString("yy",edYY.getText().toString());
        outState.putString("cvv",edCvv.getText().toString());
    }

    @Override
    public void launchNextScreen() {
        mListener.navigate(SuccessFragment.class.getSimpleName() ,null);
    }

    @Override
    public void showLoader() {

    }

    @Override
    public void hideLoader() {

    }

    @Override
    public void noInternet() {

    }

    @Override
    public void showFieldError(int code) {
        switch (code) {
            case 0:
                edCardNo.setError(getString(R.string.required));
                break;
            case 1:
                edMM.setError(getString(R.string.required));
                break;
            case 2:
                edYY.setError(getString(R.string.required));
                break;
            case 3:
                edCvv.setError(getString(R.string.required));
                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ProductHomeListener) {
            mListener = (ProductHomeListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

}
