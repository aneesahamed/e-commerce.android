package com.example.e_commerce.ui.Order.orderDetail;

import com.example.e_commerce.model.entity.Address;
import com.example.e_commerce.model.entity.Price;
import com.example.e_commerce.ui.BaseInteractor;

public interface OrderDetailInteractor extends BaseInteractor {
    @Override
    void hideLoader();

    @Override
    void showLoader();

    @Override
    void noInternet();

    void showAddress(Address address);

}
