package com.example.e_commerce;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import com.example.e_commerce.common.AppController;
import com.example.e_commerce.common.PreferenceManager;
import com.example.e_commerce.model.DataBaseHelper;
import com.example.e_commerce.ui.BaseActivity;
import com.example.e_commerce.ui.InitHelper;
import com.example.e_commerce.ui.home.HomeActivity;
import com.example.e_commerce.ui.login.LoginActivity;

public class MainActivity extends BaseActivity implements InitHelper.FragmentListener {

    private boolean isDataLoading;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(savedInstanceState != null){
            isDataLoading = savedInstanceState.getBoolean("isDataLoading");
        }

        if (!AppController.getInstance().getDataBaseHelper().isDBLoaded()) {
            InitHelper initHelper = (InitHelper) getSupportFragmentManager().findFragmentByTag(InitHelper.class.getSimpleName());
            if(initHelper == null){
                initHelper = new InitHelper();
                getSupportFragmentManager().beginTransaction().add(initHelper ,InitHelper.class.getSimpleName()).commit();
                showProgressDialog();
                isDataLoading = true;
                initHelper.init(this);
            }
        } else {
            if(!isDataLoading){
                launchNextScreen();
            }
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("isDataLoading" ,isDataLoading);
    }

    /* public class initDatabase extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            showProgressDialog();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            return AppController.getInstance().getDataBaseHelper().dataInitialization();
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            hideProgressDialog();
            launchNextScreen();
        }
    }*/

    @Override
    public void onLoadComplete(boolean mBoolean) {
        isDataLoading = false;
        hideProgressDialog();
        launchNextScreen();
    }

    public void launchNextScreen() {
        Intent intent = new Intent(this, PreferenceManager.getUserInfo(this) == null ? LoginActivity.class : HomeActivity.class);
        startActivity(intent);
    }

}
