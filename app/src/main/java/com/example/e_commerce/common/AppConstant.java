package com.example.e_commerce.common;

public class AppConstant {

    public static final String EMPTY = "";
    public static final String API_KEY = "AIzaSyCgq4ZgBFP5-WkiWZJMhQefcaV4mQYLM4Q";
    public static final String API_END_POINTS = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=%s&destinations=%s&key="+API_KEY;
    public static final String USER_PREF = "app.user";
    public static final String PRIMARY_CHANNEL = "notification_channel_general";
    public static final String IMAGE_CACHE_DIR = "thumbs";
    public static final long PER_DAY_MILLISECONDS = 86400000;

    //preference key name
    public static final String PREF_UID = "uid";
    public static final String PREF_NAME = "name";
    public static final String PREF_EMAIL = "email";
    public static final String PREF_MOBILE_NO = "mobileNo";
    public static final String PREF_PROFILE = "profile";
    public static final String PREF_ADDRESS_ID = "addressId";


    //json key name
    public static final String GENERAL_CATEGORY= "generalCategory";
    public static final String NAME = "name";
    public static final String IMAGE_URL = "imageUrl";
    public static final String CATEGORY = "category";
    public static final String SUB_CATEGORY = "subCategory";
    public static final String PRODUCTS = "products";
    public static final String VARIANTS = "variants";
    public static final String TITLE = "title";
    public static final String AMOUNT = "amount";
    public static final String DISCOUNT_PERCENTAGE = "discountPercentage";
    public static final String TYPE = "type";
    public static final String DESC = "desc";
    public static final String PRODUCT_BASE_INFO = "productBaseInfo";
    public static final String PRODUCT_DESC = "productDescription";
    public static final String RATING = "rating";
    public static final String WARRENTY = "warrenty";
    public static final String SPECIFICATION = "specification";
    public static final String MODEL_NUMBER ="Model Number";
    public static final String MODEL_NAME ="Model Name";
    public static final String COLOR = "Color";
    public static final String P_TYPE = "Type";
    public static final String SIM_TYPE = "Sim Type";
    public static final String WEIGHT = "Weight";
    public static final String PRODUCT_BRAND = "productBrand";
    public static final String STOCK_COUNT = "stockCount";
    public static final String SIZE ="size";
    public static final String P_COLOR = "color";
    public static final String COLOR_NAME = "colorNAme";
    public static final String CODE = "code";
    public static final String AVAILABLE_RAM = "availableRAM";
    public static final String AVAILABLE_HERTZ = "availableHertz";
    public static final String AVAILABLE_MEM = "availableMem";
    public static final String AVAILABLE_CORE = "availableCore";
    public static final String IMAGE_URLS = "imageUrls";
    public static final String CATEGORY_IMAGE = "categoryImage";
    public static final String SPEC_VARIANT = "specVariant";
    public static final String OFFER_PRODUCT = "offerProduct";
    public static final String TRENDS = "trends";


}
