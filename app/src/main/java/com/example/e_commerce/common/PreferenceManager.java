package com.example.e_commerce.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;

import com.example.e_commerce.model.entity.User;

public class PreferenceManager {


    private static SharedPreferences getSharedPreference(Context context) {
        return context.getSharedPreferences(AppConstant.USER_PREF, Context.MODE_PRIVATE);
    }

    public static String getPreferenceString(Context context, String name, String defValue) {
        return getSharedPreference(context).getString(name, defValue);
    }

    public static void setPreferenceString(Context context, String name, String value) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(name, value);
        editor.apply();
    }

    public static int getPreferenceInt(Context context, String name, int defValue) {
        return getSharedPreference(context).getInt(name, defValue);
    }

    public static void setPreferenceInt(Context context, String name, int value) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putInt(name, value);
        editor.apply();
    }

    public static User getUserInfo(Context context) {
        SharedPreferences sharedPreferences = getSharedPreference(context);
        if (sharedPreferences.getString(AppConstant.PREF_NAME, null) != null) {
            User user = new User();
            user.setUid(sharedPreferences.getString(AppConstant.PREF_UID, null));
            user.setName(sharedPreferences.getString(AppConstant.PREF_NAME, null));
            user.setEmail(sharedPreferences.getString(AppConstant.PREF_EMAIL, null));
            user.setMobileNo(sharedPreferences.getString(AppConstant.PREF_MOBILE_NO, null));
            user.setProfileUri(sharedPreferences.getString(AppConstant.PREF_PROFILE, null));
            user.setAddressId(sharedPreferences.getString(AppConstant.PREF_ADDRESS_ID, "431a62b6-d9df-4e71-874d-13885200cadb"));
            return user;
        } else {
            return null;
        }
    }

    public static void setUserInfo(User user, Context context) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(AppConstant.PREF_UID, user.getUid());
        editor.putString(AppConstant.PREF_NAME, user.getName());
        editor.putString(AppConstant.PREF_EMAIL, user.getEmail());
        editor.putString(AppConstant.PREF_MOBILE_NO, user.getMobileNo());
        editor.putString(AppConstant.PREF_ADDRESS_ID, user.getAddressId());
        if (!TextUtils.isEmpty(user.getProfileUri())) {
            editor.putString(AppConstant.PREF_PROFILE, user.getProfileUri());
        }
        editor.apply();
    }

    public static void clearUserInfo(Context context) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.clear();
        editor.apply();
    }
}
