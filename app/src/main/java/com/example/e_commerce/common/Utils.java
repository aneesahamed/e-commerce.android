package com.example.e_commerce.common;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.format.DateUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.example.e_commerce.R;
import com.example.e_commerce.common.imageUtils.ImageCache;
import com.example.e_commerce.common.imageUtils.ImageFetcher;
import com.example.e_commerce.ui.BaseActivity;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.core.app.NotificationCompat;

import static android.content.Context.NOTIFICATION_SERVICE;
import static androidx.core.content.ContextCompat.checkSelfPermission;
import static com.example.e_commerce.common.AppConstant.IMAGE_CACHE_DIR;

public class Utils {

    public static final String FORMAT_DATE_D_MMM_YYYY = "dd, MMM yyyy";
    public static final String FORMAT_DATE_DD_MM_YYYY = "dd-mm=yyyy";

    public static String getStringJSON(Context context) {
        String json;
        try {
            InputStream input = context.getAssets().open("dbSource.json");
            int size = input.available();
            byte[] buffer = new byte[size];
            input.read(buffer);
            input.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return json;
    }

    public static void requestFocus(Context context, EditText editText) {
        editText.clearFocus();
        editText.setFocusableInTouchMode(true);
        editText.requestFocus();
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
        }
        editText.setSelection(editText.getText().length());
    }

    public static boolean validateEmail(final String hex) {
        Pattern pattern;
        Matcher matcher;
        String EMAIL_PATTERN =
                "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(hex);
        return matcher.matches();
    }

    public static boolean isConnectedToInternet(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    public static String getOtp() {
        return String.format(new Locale("en"), "%06d", new Random().nextInt(1000000));
    }

    public static void registerInformationNotificationChannel(Context context) {
        if (Build.VERSION.SDK_INT >= 26) {
            NotificationManager mngr = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
            if (mngr.getNotificationChannel(AppConstant.PRIMARY_CHANNEL) != null) {
                return;
            }
            NotificationChannel channel = new NotificationChannel(
                    AppConstant.PRIMARY_CHANNEL,
                    context.getString(R.string.default_channel),
                    NotificationManager.IMPORTANCE_HIGH);
            // Configure the notification channel.
            channel.enableLights(false);
            channel.enableVibration(false);
            channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            channel.setSound(notification, getNotificationAudioAttributes());
            mngr.createNotificationChannel(channel);
        }
    }

    @TargetApi(21)
    private static AudioAttributes getNotificationAudioAttributes() {
        return new AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_NOTIFICATION).build();
    }


    public static NotificationManager getNotificationManager(Context context) {
        return (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
    }

    public static void sendNotification(Context context, String title, String message, boolean playSound, String channel, int notificationId) {
        //Show notification to the user
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channel)
                .setSmallIcon(Utils.getNotificationIcon())
                .setContentText(message)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message));
        if (!title.isEmpty()) {
            mBuilder.setContentTitle(title);
        }
        mBuilder.setAutoCancel(true);  // for removing the notification post click
        mBuilder.setColor(context.getResources().getColor(R.color.colorAccent));
        //Play notification sound
        if (playSound) {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            mBuilder.setSound(notification);
        }
        NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        // Because clicking the notification opens a new ("special") activity, there's
        // no need to create an artificial back stack.
        nm.notify(notificationId, mBuilder.build());
    }

    public static int getNotificationIcon() {
        boolean whiteIcon = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
        return whiteIcon ? R.mipmap.ic_launcher : R.mipmap.ic_launcher;
    }

    public static void hideKeyBoard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view != null) {
            imm.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }

    }

    public static String revertMobileNo(String mobNo) {
        return mobNo.substring(3);
    }

    public static ImageFetcher getImageFetcher(Activity context, boolean withPlaceHolder) {
        final DisplayMetrics displayMetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        final int height = displayMetrics.heightPixels;
        final int width = displayMetrics.widthPixels;
        final int longest = (height > width ? height : width) / 2;
        ImageCache.ImageCacheParams cacheParams = new ImageCache.ImageCacheParams(context, IMAGE_CACHE_DIR);
        cacheParams.setMemCacheSizePercent(0.20f);
        ImageFetcher imageFetcher = new ImageFetcher(context, longest);
        if (withPlaceHolder) {
            imageFetcher.setLoadingImage(R.drawable.placeholder);
        }
        imageFetcher.addImageCache(((BaseActivity) context).getSupportFragmentManager(), cacheParams);
        return imageFetcher;
    }

    public static int getDiscountValue(int original, int discount) {
        double i = original - (original * ((double) discount / 100));
        return (int) Math.round(i);
    }

    public static int getSavingValue(int original, int discount) {
        double i = (original * ((double) discount / 100));
        return (int) Math.round(i);
    }

    private static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmap(String pathName, int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(pathName, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(pathName, options);
    }

    public static boolean isStoragePermissionGiven(Context context) {

        return (checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    public static boolean isCameraPermissionGiven(Context context) {


        if (checkSelfPermission(context, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    public static File createTempFile(Context context) {

        String rootPath = getDirectory(context);
        File rootDir = new File(rootPath);
        if (!rootDir.exists()) {
            rootDir.mkdirs();
        }
        File tempFile = new File(rootPath, getFileUniqueName());
        return tempFile;
    }

    public static String getFileUniqueName() {

        Random random = new Random();
        int rNo = random.nextInt(999999) + 100000;
        return "IMG_" + Calendar.getInstance().getTimeInMillis() + String.valueOf(rNo) + ".jpg";
    }

    public static String getDirectory(Context context) {
        return isExternalStorageWritable() ? Environment.getExternalStorageDirectory() + "/e-commerce" : context.getFilesDir() + "/e-commerce";
    }

    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }


    public static class FilePath {
        public static String getPath(final Context context, final Uri uri) {
            // DocumentProvider
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && DocumentsContract.isDocumentUri(context, uri)) {
                // ExternalStorageProvider
                if (isExternalStorageDocument(uri)) {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];
                    if ("primary".equalsIgnoreCase(type)) {
                        return Environment.getExternalStorageDirectory() + "/" + split[1];
                    }
                }
                // DownloadsProvider
                else {
                    if (isDownloadsDocument(uri)) {
                        final String id = DocumentsContract.getDocumentId(uri);
                        final Uri contentUri = ContentUris.withAppendedId(
                                Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
                        return getDataColumn(context, contentUri, null, null);
                    }
                    // MediaProvider
                    else if (isMediaDocument(uri)) {
                        final String docId = DocumentsContract.getDocumentId(uri);
                        final String[] split = docId.split(":");
                        final String type = split[0];
                        Uri contentUri = null;
                        if ("image".equals(type)) {
                            contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                        } else if ("video".equals(type)) {
                            contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                        } else if ("audio".equals(type)) {
                            contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                        }

                        final String selection = "_id=?";
                        final String[] selectionArgs = new String[]{
                                split[1]
                        };

                        return getDataColumn(context, contentUri, selection, selectionArgs);
                    }
                }
            }
            // MediaStore (and general)
            else if ("content".equalsIgnoreCase(uri.getScheme())) {
                // Return the remote address
                if (isGooglePhotosUri(uri))
                    return uri.getLastPathSegment();

                return getDataColumn(context, uri, null, null);
            }
            // File
            else if ("file".equalsIgnoreCase(uri.getScheme())) {
                return uri.getPath();
            }

            return null;
        }

        public static String getDataColumn(Context context, Uri uri, String selection,
                                           String[] selectionArgs) {

            Cursor cursor = null;
            final String column = MediaStore.Images.Media.DATA;
            final String[] projection = {
                    column
            };

            try {
                cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                        null);
                if (cursor != null && cursor.moveToFirst()) {
                    final int index = cursor.getColumnIndexOrThrow(column);
                    return cursor.getString(index);
                }
            } finally {
                if (cursor != null)
                    cursor.close();
            }
            return null;
        }


        public static boolean isExternalStorageDocument(Uri uri) {
            return "com.android.externalstorage.documents".equals(uri.getAuthority());
        }


        public static boolean isDownloadsDocument(Uri uri) {
            return "com.android.providers.downloads.documents".equals(uri.getAuthority());
        }


        public static boolean isMediaDocument(Uri uri) {
            return "com.android.providers.media.documents".equals(uri.getAuthority());
        }

        public static boolean isGooglePhotosUri(Uri uri) {
            return "com.google.android.apps.photos.content".equals(uri.getAuthority());
        }
    }

    public static boolean hasIndex(int index, List<String> list) {
        if (index < list.size())
            return true;
        return false;
    }

    public static String getDayDiff(Context context, long time) {
        long daysDiff = Utils.getNumOfDay(time);
        String duration = AppConstant.EMPTY;
        if (daysDiff >= 0) {
            if (daysDiff >= 30) {
                int month = (int) daysDiff / 30;
                if (month == 1) {
                    duration = String.format(context.getString(R.string.deliveryIn), String.valueOf(month) + " month");
                } else {
                    duration = String.format(context.getString(R.string.deliveryIn), String.valueOf(month) + " months");
                }
            } else if (daysDiff == 0) {
                duration = context.getString(R.string.deliveryBy);
            } else if (daysDiff == 1) {
                duration = String.format(context.getString(R.string.deliveryIn), String.valueOf(daysDiff) + " day");
            } else {
                duration = String.format(context.getString(R.string.deliveryIn), String.valueOf(daysDiff) + " days");
            }
        }
        return duration;
    }

    public static long dayDiff(long time) {
        return TimeUnit.MILLISECONDS.toDays(time);
    }

    public static long getNumOfDay(long millis) {
        long msDiff = millis - Calendar.getInstance().getTimeInMillis();
        return TimeUnit.MILLISECONDS.toDays(msDiff);
    }

    public static String getDateFromTimeMillis(String milliseconds, String format) {
        long time = Long.parseLong(milliseconds);
        Date d = new java.util.Date(time);
        return new SimpleDateFormat(format, new Locale("en")).format(d);
    }

    public static long getNumDays(long sec) {
        return TimeUnit.SECONDS.toDays(sec);
    }

    public static String getDurationDay(Context context, long sec) {
        long day = getNumDays(sec);
        if (day >= 0 && day <= 1) {
            return String.format(context.getString(R.string.deliveryIn), "1 day");
        } else if (day > 1 && day < 7) {
            return String.format(context.getString(R.string.deliveryIn), String.valueOf(day)) + " days";
        } else {
            return String.format(context.getString(R.string.deliveryIn), "1") + " week";
        }

    }
}
