package com.example.e_commerce.common;

import android.app.Application;

import com.example.e_commerce.model.DataBaseHelper;
import com.example.e_commerce.model.DataProvider;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class AppController extends Application {
    public static AppController mInstance;
    private DataBaseHelper dataBaseHelper;
    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        dataBaseHelper = DataProvider.dataBaseHelper;
    }

    public static synchronized AppController getInstance(){
        return mInstance;
    }

    public DataBaseHelper getDataBaseHelper(){
        if(dataBaseHelper == null){
            dataBaseHelper = new DataBaseHelper(this);
        }
        return dataBaseHelper;
    }

    public void setDataBaseHelper(DataBaseHelper dataBaseHelper) {
        this.dataBaseHelper = dataBaseHelper;
    }

    public long getDeliveryDuration(String pincode, String dest) {
        try {

            URL mUrl = new URL(String.format(AppConstant.API_END_POINTS, pincode, dest));
            HttpURLConnection httpConnection = (HttpURLConnection) mUrl.openConnection();
            httpConnection.setRequestMethod("GET");
            httpConnection.setRequestProperty("Content-length", "0");
            httpConnection.setUseCaches(false);
            httpConnection.setAllowUserInteraction(false);
            httpConnection.setConnectTimeout(100000);
            httpConnection.setReadTimeout(100000);

            httpConnection.connect();

            int responseCode = httpConnection.getResponseCode();

            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
                return parseJson(sb.toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return 0;
    }

    private long parseJson(String jsonString) {

        long duration = 0;
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            JSONArray jsonArray = jsonObject.getJSONArray("rows");
            if (jsonArray.length() > 0) {
                JSONObject jsonObject1 = jsonArray.getJSONObject(0);
                JSONArray elementArray = jsonObject1.getJSONArray("elements");
                for (int i = 0; i < elementArray.length(); i++) {
                    JSONObject obj = elementArray.getJSONObject(i);
                    if (obj.getString("status").equals("OK")) {
                        JSONObject durationObj = obj.getJSONObject("duration");
                        duration += durationObj.getInt("value");
                    }
                }

            }

        } catch (JSONException ex) {

            ex.printStackTrace();
        }

        return duration;
    }
}
